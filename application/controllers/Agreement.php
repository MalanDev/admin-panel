<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

class Agreement extends CI_Controller {

	protected $api_url;

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		date_default_timezone_set("Asia/Colombo");

		$this->api_url = apiurl;
	}

	function createAgreement()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-all-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('agreement/create_agreement', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function getBookingNumbers()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-booking-numbers',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}


	function getAgreementNo()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-agreement-number',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}


	function getVehicleRegistrationNumberByVehicleType()
	{
		$vehicle_type_id = $_POST['vehicle_type_id'];
		$from_date = $_POST['from_date'];
		$to_date = $_POST['to_date'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'booking/check-booking-availability',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicle_type_id, 'from_date' => $from_date, 'to_date' => $to_date),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function saveAgreement()
	{
		$agreementId = $_POST['agreement_id'];
		$bookingNo = $_POST['bookingNo'];
		$agreement_no = $_POST['agreement_no'];
		$ddate = date('Y-m-d h:i:s');
		$fName = $_POST['fName'];
		$lName = $_POST['lName'];
		$birthday = $_POST['birthday'];
		$age = $_POST['age'];
		$nationality = $_POST['nationality'];
		$passport = $_POST['passport'];
		$expiryDate = $_POST['expiryDate'];
		$country_selector_code = $_POST['country_selector_code'];
		$mobile_country_code = $_POST['mobile_country_code'];
		$mobile2_country_code = $_POST['mobile2_country_code'];
		$mobile1 = $_POST['mobile1'];
		$mobile2 = $_POST['mobile2'];
		$email = $_POST['email'];
		$address = $_POST['address'];
		$permenentAdd = $_POST['permenentAdd'];
		$flightNo = $_POST['flightNo'];
		$departure = $_POST['departure'];
		$depDate = $_POST['depDate'];
		$depTime = $_POST['depTime'];
		$vehicletype = $_POST['vehicletype'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$regNo = $_POST['regNo'];
		$licenseNo = $_POST['licenseNo'];
		$milageStart = $_POST['milageStart'];
		$milageEnd = $_POST['milageEnd'];

		if (empty($agreementId)) {
			// New Agreement

			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $this->api_url.'agreement/create-agreement',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => array('booking_id' => $bookingNo,'agreement_no' => $agreement_no,'agreement_date' => $ddate,'first_name' => $fName,'last_name' => $lName,'date_of_birth' => $birthday,'age' => $age,'nationality' => $nationality,'passport_no' => $passport,'visa_expiry_date' => $expiryDate,'country_code' => $country_selector_code,'mobile_1_country_code' => $mobile_country_code,'mobile_1' => $mobile1,'mobile_2_country_code' => $mobile2_country_code,'mobile_2' => $mobile2,'email' => $email,'address_in_srilanka' => $address,'permanent_address' => $permenentAdd,'flight_number' => $flightNo,'departure_loation' => $departure,'departure_date' => $depDate,'departure_time' => $depTime,'vehicle_type_id' => $vehicletype,'from_date' => $fromDate,'to_date' => $toDate,'vehicle_id' => $regNo,'license_no' => $licenseNo,'milage_starts' => $milageStart,'milage_ends' => $milageEnd),
				CURLOPT_HTTPHEADER => array(
					'Authorization: Bearer '.$this->session->userdata('token')
				),
			));

			$response = json_decode(curl_exec($curl));
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

			curl_close($curl);

			if ($httpcode == 200) {

				$this->session->set_flashdata('success', 'Agreement successfully added');

			} else {
				$error = '<ul style="margin-bottom:0;">';
				foreach($response->error as $err) {
					$error.= '<li>'.$err.'</li>';
				}
				$error.= '</ul>';

				$this->session->set_flashdata('error', $error);

				redirect('create-agreement');
			}

		} else {
			//Update Agreement
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $this->api_url.'agreement/update-agreement',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => '',
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 0,
				CURLOPT_SSL_VERIFYPEER => false,
				CURLOPT_FOLLOWLOCATION => true,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => 'POST',
				CURLOPT_POSTFIELDS => array('agreement_id' => $agreementId,'booking_id' => $bookingNo,'agreement_date' => $ddate,'first_name' => $fName,'last_name' => $lName,'date_of_birth' => $birthday,'age' => $age,'nationality' => $nationality,'passport_no' => $passport,'visa_expiry_date' => $expiryDate,'country_code' => $country_selector_code,'mobile_1_country_code' => $mobile_country_code,'mobile_1' => $mobile1,'mobile_2_country_code' => $mobile2_country_code,'mobile_2' => $mobile2,'email' => $email,'address_in_srilanka' => $address,'permanent_address' => $permenentAdd,'flight_number' => $flightNo,'departure_loation' => $departure,'departure_date' => $depDate,'departure_time' => $depTime,'vehicle_type_id' => $vehicletype,'from_date' => $fromDate,'to_date' => $toDate,'vehicle_id' => $regNo,'license_no' => $licenseNo,'milage_starts' => $milageStart,'milage_ends' => $milageEnd),
				CURLOPT_HTTPHEADER => array(
					'Authorization: Bearer '.$this->session->userdata('token')
				),
			));

			$response = json_decode(curl_exec($curl));
			$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

			curl_close($curl);

			if ($httpcode == 200) {
				$this->session->set_flashdata('success', 'Agreement successfully updated!');

			} else {
				$error = '<ul style="margin-bottom:0;">';
				foreach($response->error as $err) {
					$error.= '<li>'.$err.'</li>';
				}
				$error.= '</ul>';

				$this->session->set_flashdata('error', $error);

				redirect('create-agreement');
			}
		}
	}

	function getAgreementById($id)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-agreement-by-agreement-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('agreement_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;

		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function deleteAgreement($id)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/delete-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('agreement_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Agreement successfully deleted!');

			redirect('create-agreement');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('create-agreement');
		}	
	}

	function printAgreement($id)
	{		
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-agreement-by-agreement-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('agreement_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$receipt = $this->load->view('agreement/print_agreement', $response, true);

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4',
				'margin_top' => 6,
				'margin_right' => 10,
				'margin_left' => 10,
				'margin_bottom' => 6
			]);
			$mpdf->WriteHTML($receipt);
			$mpdf->Output();

		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function endAgreement()
	{
		$id = $_POST['agreementid'];
		$end_date = $_POST['end_date'];
		$milage_end = $_POST['milage_end'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/end-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('agreement_id' => $id, 'agreement_end_at' => $end_date, 'milage_ends' => $milage_end),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Agreement successfully ended!');

			redirect('create-agreement');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('create-agreement');
		}
	}

	function viewAgreement()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-all-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('agreement/view_agreement', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function getAgreementNoByBookingId($id)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-agreement-numbers-by-booking-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('booking_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;die();

		} else {
			$response = json_decode($response);

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('view-booking');
		}
	}

	function searchAgreement()
	{
		$bookingNo = $_POST['bookingNo'];
		$agreement_No = $_POST['agreement_No'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$pickup_date = $_POST['pickup_date'];
		$return_date = $_POST['return_date'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/search-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('booking_id' => $bookingNo,'agreement_id' => $agreement_No,'from_date' => $fromDate,'to_date' => $toDate, 'is_pickup_date' => $pickup_date,'is_return_date' => $return_date),
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('agreement/ajax_agreement_table', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('view-agreement');
		}
	}


	function getAllAgreements()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/get-all-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('agreement/ajax_agreement_table', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('view-agreement');
		}
	}


	function updateAgreement()
	{
		$booking_no_edit = $_POST['booking_no_edit'];
		$vehicletype_edit = $_POST['vehicletype_edit'];
		$created_at_edit = $_POST['created_at_edit'];
		$fName_edit = $_POST['fName_edit'];
		$lName_edit = $_POST['lName_edit'];
		$birthday_edit = $_POST['birthday_edit'];
		$age_edit = $_POST['age_edit'];
		$nationality_edit = $_POST['nationality_edit'];
		$passport_edit = $_POST['passport_edit'];
		$expiryDate_edit = $_POST['expiryDate_edit'];
		$country_selector_code = $_POST['country_selector_code'];
		$mobile_country_code = $_POST['mobile_country_code'];
		$mobile2_country_code = $_POST['mobile2_country_code'];
		$mobile1 = $_POST['mobile1'];
		$mobile2 = $_POST['mobile2'];
		$email_edit = $_POST['email_edit'];
		$address_edit = $_POST['address_edit'];
		$permenentAdd_edit = $_POST['permenentAdd_edit'];
		$flightNo_edit = $_POST['flightNo_edit'];
		$departure_edit = $_POST['departure_edit'];
		$depDate_edit = $_POST['depDate_edit'];
		$depTime_edit = $_POST['depTime_edit'];
		$fromDate_edit = $_POST['fromDate_edit'];
		$toDate_edit = $_POST['toDate_edit'];
		$regNo_edit = $_POST['regNo_edit'];
		$licenseNo_edit = $_POST['licenseNo_edit'];
		$milageStart_edit = $_POST['milageStart_edit'];
		$milageEnd_edit = $_POST['milageEnd_edit'];
		$agreement_id_edit = $_POST['agreement_id_edit'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/update-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('agreement_id' => $agreement_id_edit,'booking_id' => $booking_no_edit,'agreement_date' => $created_at_edit,'first_name' => $fName_edit,'last_name' => $lName_edit,'date_of_birth' => $birthday_edit,'age' => $age_edit,'nationality' => $nationality_edit,'passport_no' => $passport_edit,'visa_expiry_date' => $expiryDate_edit,'country_code' => $country_selector_code,'mobile_1_country_code' => $mobile_country_code,'mobile_1' => $mobile1,'mobile_2_country_code' => $mobile2_country_code,'mobile_2' => $mobile2,'email' => $email_edit,'address_in_srilanka' => $address_edit,'permanent_address' => $permenentAdd_edit,'flight_number' => $flightNo_edit,'departure_loation' => $departure_edit,'departure_date' => $depDate_edit,'departure_time' => $depTime_edit,'vehicle_type_id' => '1','from_date' => $fromDate_edit,'to_date' => $toDate_edit,'vehicle_id' => $regNo_edit,'license_no' => $licenseNo_edit,'milage_starts' => $milageStart_edit,'milage_ends' => $milageEnd_edit),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Agreement successfully updated');

		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

}