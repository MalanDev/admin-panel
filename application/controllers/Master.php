<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	protected $api_url;

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		date_default_timezone_set("Asia/Colombo");

		$this->api_url = apiurl;
	}


/*-------------------------------------------------
------------- Vehicle Type ------------------------
--------------------------------------------------*/
function vehicleTypes()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-type/get-all-vehicle-types',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);


	if ($httpcode == 200) {
		$this->load->view('master/vehicle_types', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}		
}

function getAllVehicleTypes()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-type/get-all-vehicle-types',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);


	if ($httpcode == 200) {
		echo $response;
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}		
}

function saveVehicleType()
{
	$vehicle_type_id = $_POST['vehicletype_id'];
	$vehicle_type = $_POST['vehicletype'];
	$vehiclefee = $_POST['vehiclefee'];
	$noofseats = $_POST['noofseats'];

	if (empty($vehicle_type_id)) {
		// New Vehicle Type
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle-type/save-vehicle-type',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('type' => $vehicle_type,'vehicle_fee' => $vehiclefee,'no_of_seats' => $noofseats),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Vehicle Type successfully added!');

			redirect('vehicle-types');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-types');
		}
	} else {
		// Update Vehicle Type
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle-type/update-vehicle-type',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicle_type_id,'type' => $vehicle_type,'vehicle_fee' => $vehiclefee,'no_of_seats' => $noofseats),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Vehicle Type successfully updated!');

			redirect('vehicle-types');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-types');
		}
	}		
}

function vehicleTypeStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-type/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_type_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function getVehicleTypeById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-type/get-vehicle-type-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_type_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}		
}

function deleteVehicleType($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-type/remove-vehicle-type',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_type_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Vehicle Type successfully deleted!');

		redirect('vehicle-types');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('vehicle-types');
	}		
}


/*-------------------------------------------------
------------- Pickup Location ---------------------
--------------------------------------------------*/
function pickupLocations()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'pickup-location/get-all-pickup-location',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/pickup_locations', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}	
}

function pickupLocationStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'pickup-location/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('pickup_location_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function savePickupLocation()
{
	$vehicletype = $_POST['vehicletype'];
	$pickupLocation = $_POST['pickupLocation'];
	$locationFee = $_POST['locationFee'];
	$pickupLocation_id = $_POST['pickupLocation_id'];

	if (empty($pickupLocation_id)) {
		// New Pickup Location
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'pickup-location/save-pickup-location',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'pickup_location' => $pickupLocation,'fee' => $locationFee),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Pickup Location successfully added!');

			redirect('pickup-locations');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('pickup-locations');
		}
	} else {
		// Update Pickup Location
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'pickup-location/update-pickup-location',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('pickup_location_id' => $pickupLocation_id,'vehicle_type_id' => $vehicletype,'pickup_location' => $pickupLocation,'fee' => $locationFee),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Pickup Location successfully updated!');

			redirect('pickup-locations');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('pickup-locations');
		}
	}		
}

function getPickupLocationById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'pickup-location/get-pickup-location-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('pickup_location_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}

function deletePickupLocation($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'pickup-location/remove-pickup-location',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('pickup_location_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Pickup Location successfully deleted!');

		redirect('pickup-locations');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('pickup-locations');
	}	
}


/*-------------------------------------------------
------------- Return Location ---------------------
--------------------------------------------------*/
function returnLocations()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'return-location/get-all-return-location',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/return_locations', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}	
}

function returnLocationStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'return-location/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('return_location_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function saveReturnLocation()
{
	$vehicletype = $_POST['vehicletype'];
	$returnLocation = $_POST['returnLocation'];
	$locationFee = $_POST['locationFee'];
	$returnLocation_id = $_POST['returnLocation_id'];

	if (empty($returnLocation_id)) {
		// New Return Location
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'return-location/save-return-location',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'return_location' => $returnLocation,'fee' => $locationFee),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Return Location successfully added!');

			redirect('return-locations');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('return-locations');
		}
	} else {
		// Update Return Location
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'return-location/update-return-location',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('return_location_id' => $returnLocation_id,'vehicle_type_id' => $vehicletype,'return_location' => $returnLocation,'fee' => $locationFee),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Return Location successfully updated!');

			redirect('return-locations');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('return-locations');
		}
	}		
}

function getReturnLocationById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'return-location/get-return-location-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('return_location_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}

function deleteReturnLocation($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'return-location/remove-return-location',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('return_location_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Return Location successfully deleted!');

		redirect('return-locations');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('return-locations');
	}	
}


/*-------------------------------------------------
------------- Service Charges ---------------------
--------------------------------------------------*/
function serviceCharges()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'service-chargers/get-all-service-chargers',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/service_charges', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}	
}

function saveServiceCharge()
{
	$vehicletype = $_POST['vehicletype'];
	$drivingLicenseFee = $_POST['drivingLicenseFee'];
	$Dduration = $_POST['Dduration'];
	$insuranceFee = $_POST['insuranceFee'];
	$Iduration = $_POST['Iduration'];
	$servicecharge_id = $_POST['servicecharge_id'];

	if (empty($servicecharge_id)) {
		// New Service Charge
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'service-chargers/save-service-chargers',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'driving_license_fee' => $drivingLicenseFee,'driving_license_days' => $Dduration,'insurance_fee' => $insuranceFee,'insurance_days' => $Iduration),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Service Charge successfully added!');

			redirect('service-charges');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('service-charges');
		}
	} else {
		// Update Service Charge
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'service-chargers/update-service-chargers',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('service_charge_id' => $servicecharge_id,'vehicle_type_id' => $vehicletype,'driving_license_fee' => $drivingLicenseFee,'driving_license_days' => $Dduration, 'insurance_fee' => $insuranceFee, 'insurance_days' => $Iduration),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Service Charge successfully updated!');

			redirect('service-charges');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('service-charges');
		}
	}		
}

function serviceChargeStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'service-chargers/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('service_charge_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function getServiceChargeById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'service-chargers/get-service-charge-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('service_charge_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}


function deleteServiceCharge($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'service-chargers/remove-service-chargers',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('service_charge_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Service Charge successfully deleted!');

		redirect('service-charges');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('service-charges');
	}	
}


/*-------------------------------------------------
------------- Extra Services ---------------------
--------------------------------------------------*/
function extraServices()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'extra-service/get-all-extra-service',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/extra_services', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}
}

function extraServiceStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'extra-service/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('extra_service_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function saveExtraService()
{
	$vehicletype = $_POST['vehicletype'];
	$exterService = $_POST['exterService'];
	$serviceFee = $_POST['serviceFee'];
	$extraService_id = $_POST['extraService_id'];

	if (empty($extraService_id)) {
		// New Extra Service
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'extra-service/save-extra-service',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'service' => $exterService,'fee' => $serviceFee),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Extra Service successfully added!');

			redirect('extra-services');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('extra-services');
		}
	} else {
		// Update Extra Service
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'extra-service/update-extra-service',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('extra_service_id' => $extraService_id,'vehicle_type_id' => $vehicletype,'service' => $exterService,'fee' => $serviceFee),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Extra Service successfully updated!');

			redirect('extra-services');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('extra-services');
		}
	}		
}

function getExtraServiceById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'extra-service/get-extra-service-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('extra_service_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}

function deleteExtraService($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'extra-service/remove-extra-service',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('extra_service_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Extra Service successfully deleted!');

		redirect('extra-services');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('extra-services');
	}	
}

function coupons()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'coupons/get-all-coupons',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/coupons', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}		
}

function saveCoupon()
{
	$vehicletype = $_POST['vehicletype'];
	$coupon_id = $_POST['coupon_id'];
	$couponName = $_POST['couponName'];
	$couponCode = $_POST['couponCode'];
	$fromDate = $_POST['fromDate'];
	$toDate = $_POST['toDate'];
	$discount_select = $_POST['discount_select'];
	$discount = $_POST['discount'];
	$minAmount = $_POST['minAmount'];

	if (empty($coupon_id)) {
		// New Coupon
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'coupons/save-coupons',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'coupon_name' => $couponName,'coupon_code' => $couponCode,'from_date' => $fromDate,'to_date' => $toDate,'discount_type' => $discount_select,'discount' => $discount,'minimum_amount' => $minAmount),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Coupon successfully added!');

			redirect('coupons');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('coupons');
		}
	} else {
		// Update Coupon
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'coupons/update-coupons',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('coupon_id' => $coupon_id, 'vehicle_type_id' => $vehicletype,'coupon_name' => $couponName,'coupon_code' => $couponCode,'from_date' => $fromDate,'to_date' => $toDate,'discount_type' => $discount_select,'discount' => $discount,'minimum_amount' => $minAmount),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Coupon successfully updated!');

			redirect('coupons');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('coupons');
		}
	}	
}


function couponStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'coupons/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('coupon_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}


function deleteCoupon($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'coupons/remove-coupons',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('coupon_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Coupon successfully deleted!');

		redirect('coupons');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('coupons');
	}	
}


function getCouponById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'coupons/get-coupon-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('coupon_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}

/*-------------------------------------------------
-------- Vehicle Owner Registration ----------------
--------------------------------------------------*/
function vehicleOwnerRegistration()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-owner/get-all-vehicle-owner',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/vehicle_owner_registraion', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}
}

function getAllOwners()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-owner/get-all-vehicle-owner',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);


	if ($httpcode == 200) {
		echo $response;
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}		
}


function getAllOwnersForReport()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-owner/get-all-vehicle-owner',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);


	if ($httpcode == 200) {
		echo $response;
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}		
}


function getVehicleOwnerById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-owner/get-vehicle-owner-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_owner_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}

function saveVehicleOwner()
{
	$vehicle_owner_id = $_POST['vehicle_owner_id'];
	$firstName = $_POST['firstName'];
	$lastName = $_POST['lastName'];
	$nic = $_POST['nic'];
	$drivingLNo = $_POST['drivingLNo'];
	$address = $_POST['address'];
	$address2 = $_POST['address2'];
	$city = $_POST['city'];
	$email = $_POST['email'];
	$country_code = $_POST['country_code'];
	$mobile1 = $_POST['mobile1'];
	$mobile2 = $_POST['mobile2'];
	$mobile_country_code = $_POST['mobile_country_code'];
	$mobile_country_code2 = $_POST['mobile_country_code2'];
	$accountNo = $_POST['accountNo'];
	$accountName = $_POST['accountName'];
	$bank = $_POST['bank'];
	$branch = $_POST['branch'];

	if (empty($vehicle_owner_id)) {
		// New Vehicle Owner
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle-owner/save-vehicle-owner',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('first_name' => $firstName,'last_name' => $lastName,'nic' => $nic,'driving_license_no' => $drivingLNo,'address_line_1' => $address,'address_line_2' => $address2,'city' => $city,'mobile_1' => $mobile1, 'mobile_1_country_code' => $mobile_country_code, 'mobile_2' => $mobile2, 'mobile_2_country_code' => $mobile_country_code2, 'email' => $email,'country_code' => $country_code,'account_no' => $accountNo,'account_name' => $accountName,'bank' => $bank,'branch' => $branch),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Vehicle Owner successfully added!');

			redirect('vehicle-owner-registration');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-registration');
		}
	} else {
		// Update Vehicle Owner
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle-owner/update-vehicle-owner',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_owner_id' => $vehicle_owner_id,'first_name' => $firstName,'last_name' => $lastName,'nic' => $nic,'driving_license_no' => $drivingLNo,'address_line_1' => $address,'address_line_2' => $address2,'city' => $city,'mobile_1' => $mobile1, 'mobile_1_country_code' => $mobile_country_code, 'mobile_2' => $mobile2, 'mobile_2_country_code' => $mobile_country_code2,'email' => $email,'country_code' => $country_code,'account_no' => $accountNo,'account_name' => $accountName,'bank' => $bank,'branch' => $branch),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Vehicle Owner successfully updated!');

			redirect('vehicle-owner-registration');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-registration');
		}
	}		
}

function getAllBanks()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'bank/get-all-banks',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);


	if ($httpcode == 200) {
		echo $response;
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}		
}
function vehicleOwnerStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-owner/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_owner_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function deleteVehicleOwner($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle-owner/remove-vehicle-owner',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_owner_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Vehicle Owner successfully deleted!');

		redirect('vehicle-owner-registration');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('vehicle-owner-registration');
	}	
}


/*-------------------------------------------------
-------- Vehicle Registration ----------------
--------------------------------------------------*/

function vehicleRegistration()
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle/get-all-vehicle',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('is_active_check' => '0'),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->load->view('master/vehicle_registration', $response);
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('/');
	}	
}

function saveVehicleRegistration()
{
	$registerNo_img = $license_img = $smoke_img = $insurance_img = '';
	if ($_FILES['registerNo_image']['name'] != ''){
		$registerNo_img = new CURLFile($_FILES['registerNo_image']['tmp_name'], $_FILES['registerNo_image']['type'], $_FILES['registerNo_image']['name']);
	}

	if ($_FILES['license_image']['name'] != ''){
		$license_img = new CURLFile($_FILES['license_image']['tmp_name'], $_FILES['license_image']['type'], $_FILES['license_image']['name']);
	}

	if ($_FILES['smoke_image']['name'] != ''){
		$smoke_img = new CURLFile($_FILES['smoke_image']['tmp_name'], $_FILES['smoke_image']['type'], $_FILES['smoke_image']['name']);
	}
	
	if ($_FILES['insurance_image']['name'] != ''){
		$insurance_img = new CURLFile($_FILES['insurance_image']['tmp_name'], $_FILES['insurance_image']['type'], $_FILES['insurance_image']['name']);
	}	

	$vehicletype = $_POST['vehicletype'];
	$owner = $_POST['owner'];
	$registerNo = $_POST['registerNo'];
	$licenseNo = $_POST['licenseNo'];
	$license_expiry = $_POST['license_expiry'];
	$testNo = $_POST['testNo'];
	$smoke_expiry = $_POST['smoke_expiry'];
	$insuranceNo = $_POST['insuranceNo'];
	$insurance_expiry = $_POST['insurance_expiry'];
	$per_day = $_POST['per_day'];
	$vehicle_id = $_POST['vehicle_id'];

	if (empty($vehicle_id)) {
		// New Vehicle Registration
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle/save-vehicle',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'vehicle_owner_id' => $owner,'registration_no' => $registerNo,'vehicle_image'=> $registerNo_img,'revenue_license_no' => $licenseNo,'revenue_license_exp_date' => $license_expiry,'revenue_license_image'=> $license_img,'smoke_test_no' => $testNo,'smoke_test_exp_date' => $smoke_expiry,'smoke_test_image'=> $smoke_img,'insurance_no' => $insuranceNo,'insurance_exp_date' => $insurance_expiry,'insurance_image'=> $insurance_img,'per_day_rate' => $per_day),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Vehicle successfully added!');

			redirect('vehicle-registration');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-registration');
		}
	} else {
		// Update Vehicle Registration
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle/update-vehicle',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_id'=> $vehicle_id, 'vehicle_type_id' => $vehicletype,'vehicle_owner_id' => $owner,'registration_no' => $registerNo,'vehicle_image'=> $registerNo_img,'revenue_license_no' => $licenseNo,'revenue_license_exp_date' => $license_expiry,'revenue_license_image'=> $license_img,'smoke_test_no' => $testNo,'smoke_test_exp_date' => $smoke_expiry,'smoke_test_image'=> $smoke_img,'insurance_no' => $insuranceNo,'insurance_exp_date' => $insurance_expiry,'insurance_image'=> $insurance_img,'per_day_rate' => $per_day),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));
		
		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->session->set_flashdata('success', 'Vehicle successfully updated!');

			redirect('vehicle-registration');
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-registration');
		}
	}		
}

function vehicleRegistrationStatus($id, $status)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle/set-is-active',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_id' => $id,'is_active' => $status),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);

	curl_close($curl);
}

function getVehicleRegistrationById($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle/get-vehicle-by-id',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = curl_exec($curl);
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {

		echo $response;

	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);
	}	
}


function deleteVehicleRegistration($id)
{
	$curl = curl_init();

	curl_setopt_array($curl, array(
		CURLOPT_URL => $this->api_url.'vehicle/remove-vehicle',
		CURLOPT_RETURNTRANSFER => true,
		CURLOPT_ENCODING => '',
		CURLOPT_MAXREDIRS => 10,
		CURLOPT_TIMEOUT => 0,
		CURLOPT_SSL_VERIFYPEER => false,
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
		CURLOPT_CUSTOMREQUEST => 'POST',
		CURLOPT_POSTFIELDS => array('vehicle_id' => $id),
		CURLOPT_HTTPHEADER => array(
			'Authorization: Bearer '.$this->session->userdata('token')
		),
	));

	$response = json_decode(curl_exec($curl));
	$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

	curl_close($curl);

	if ($httpcode == 200) {
		$this->session->set_flashdata('success', 'Vehicle successfully deleted!');

		redirect('vehicle-registration');
	} else {
		$error = '<ul style="margin-bottom:0;">';
		foreach($response->error as $err) {
			$error.= '<li>'.$err.'</li>';
		}
		$error.= '</ul>';

		$this->session->set_flashdata('error', $error);

		redirect('vehicle-registration');
	}	
}

}