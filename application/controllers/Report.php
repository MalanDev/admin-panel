<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Report extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		date_default_timezone_set("Asia/Colombo");

		$this->api_url = apiurl;
	}

	function vehicleOwnerDetails()
	{
		
		$this->load->view('report/vehicle_owner_details');
		
	}

	function searchOwners()
	{
		$owner = $_POST['owner'];
		$vehicletype = $_POST['vehicletype'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'vehicle-owner/get-vehicle-owner-details',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type' => $vehicletype,'owner_name' => $owner),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('report/ajax_vehicle_owner_details', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function vehicleDetails()
	{
		$this->load->view('report/vehicle_details');
	}

	function checkAvailableVehicles()
	{
		$this->load->view('report/check_available_vehicles');
	}

	function searchAvailableVehicles()
	{
		$vehicletype = $_POST['vehicletype'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'booking/check-booking-availability',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype, 'from_date' => $fromDate,'to_date' => $toDate),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('report/ajax_vechicleavailability', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function viewBookings()
	{
		$this->load->view('report/view_bookings');
	}

	function searchBooking()
	{
		$vehicletype = $_POST['vehicletype'];
		$bookingNo = $_POST['bookingNo'];
		$status = $_POST['status'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$booking_date = $_POST['booking_date'];
		$pickup_date = $_POST['pickup_date'];
		$return_date = $_POST['return_date'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'booking/search-booking',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_type_id' => $vehicletype,'booking_no' => $bookingNo,'booking_status_id' => $status,'from_date' => $fromDate,'to_date' => $toDate,'is_booking_date' => $booking_date,'is_pickup_date' => $pickup_date,'is_return_date' => $return_date),
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('report/ajax_booking_table', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function viewAgreements()
	{
		$this->load->view('report/view_agreements');
	}

	function searchAgreements()
	{
		$bookingNo = $_POST['bookingNo'];
		$agreement_No = $_POST['agreement_No'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$pickup_date = $_POST['pickup_date'];
		$return_date = $_POST['return_date'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'agreement/search-agreement',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('booking_id' => $bookingNo,'agreement_id' => $agreement_No,'from_date' => $fromDate,'to_date' => $toDate, 'is_pickup_date' => $pickup_date,'is_return_date' => $return_date),
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('report/ajax_agreements', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function customerInvoices()
	{
		$this->load->view('report/customer_invoices');
	}

	function customerInvoiceSearch()
	{
		$invoiceNo = $_POST['invoiceNo'];
		$booking_no = $_POST['booking_no'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$status = $_POST['status'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/customer-invoice-search',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoiceNo,'booking_id' => $booking_no,'invoice_status' => $status,'from_date' => $fromDate,'to_date' => $toDate),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('report/ajax_customer_invoice', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function vehicleOwnerInvoices()
	{
		$this->load->view('report/vehicle_owner_invoices');
	}

	function VehicleOwnerInvoiceSearch()
	{
		$invoiceNo = $_POST['invoiceNo'];
		$bookingNo = $_POST['bookingNo'];
		$agreementno = $_POST['agreementno'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$status = $_POST['status'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/search-vehicle-owner-invoice',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoiceNo,'booking_id' => $bookingNo,'agreement_id' => $agreementno,'invoice_status' => $status,'from_date' => $fromDate,'to_date' => $toDate),
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('report/ajax_vehicle_owner_invoice', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

}