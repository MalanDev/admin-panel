<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	protected $api_url;

	function __construct() {
		parent::__construct();

		$this->api_url = apiurl;
	}

	function index()
	{
		$this->load->view('login');
	}

	function login()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'auth/login',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => 'email='.$username.'&password='.$password,
			CURLOPT_HTTPHEADER => array(
				'Content-Type: application/x-www-form-urlencoded'
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$userdata = array(
				'user_id' => $response->user->id,
				'username'  => $response->user->first_name,
				'token' => $response->access_token
			);

			$this->session->set_userdata($userdata);

			redirect('/');
		} else {

			$this->session->set_flashdata('error', 'Invalid username or password!');

			redirect('login');
		}		
	}

	function logout()
	{
		$this->session->sess_destroy();

		redirect('login');
	}

}