<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	protected $api_url;

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}

		date_default_timezone_set("Asia/Colombo");

		$this->api_url = apiurl;
	}

	function index()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'dashboard/get-dashboard-details',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('index', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('/');
		}
	}

	function getvehicleOwnerInvoices()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/search-vehicle-owner-invoice',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => '-1','booking_id' => '-1','agreement_id' => '-1','invoice_status' => '-1','from_date' => '-1','to_date' => '-1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('ajax_dashboard_vehicle_owner_invoice', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

}