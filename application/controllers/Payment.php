<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require_once FCPATH . 'vendor/autoload.php';

class Payment extends CI_Controller {

	function __construct() {
		parent::__construct();
		if(!$this->session->userdata('username')){
			redirect('login');
		}
		date_default_timezone_set("Asia/Colombo");

		$this->api_url = apiurl;
	}


	function customerInvoice(){

		$this->load->view('payment/customer_invoice');
	}


	function vehicleOwnerInvoice(){

		$this->load->view('payment/vehicle_owner_invoice');
	}

	function getInvoiceNumbers()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/get-customer-invoice-numbers',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function getVehicleOwnerInvoiceNumbers()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/get-vehicle-owner-invoice-numbers',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function getBookingsByInvoiceId()
	{
		$invoiceId = $_POST['invoiceId'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/get-booking-numbers-by-vehicle-owner-invoice-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoiceId),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function getAgreementsByBookingId()
	{
		$bookingId = $_POST['bookingId'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/get-agreement-numbers-by-booking-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('booking_id' => $bookingId),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function getBookingNumbers()
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/get-all-end-bookings',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			echo $response;
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function getBookingNumberByInvoiceId($id)
	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/get-booking-number-by-invoice-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;die();

		} else {
			$response = json_decode($response);

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('customer-invoice');
		}
	}


	function customerInvoiceSearch()
	{
		$invoiceNo = $_POST['invoiceNo'];
		$booking_no = $_POST['booking_no'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$status = $_POST['status'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/customer-invoice-search',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoiceNo,'booking_id' => $booking_no,'invoice_status' => $status,'from_date' => $fromDate,'to_date' => $toDate),
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('payment/ajax_customer_invoice_table', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('customer-invoice');
		}
	}


	function VehicleOwnerInvoiceSearch()
	{
		$invoiceNo = $_POST['invoiceNo'];
		$bookingNo = $_POST['bookingNo'];
		$agreementno = $_POST['agreementno'];
		$fromDate = $_POST['fromDate'];
		$toDate = $_POST['toDate'];
		$status = $_POST['status'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/search-vehicle-owner-invoice',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoiceNo,'booking_id' => $bookingNo,'agreement_id' => $agreementno,'invoice_status' => $status,'from_date' => $fromDate,'to_date' => $toDate),
			// CURLOPT_POSTFIELDS => array('is_active_check' => '1'),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {
			$this->load->view('payment/ajax_vehicle_owner_invoice_table', $response);
		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}


	function payInvoice()
	{
		$invoice_id = $_POST['invoice_id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/set-customer-invoice-paid',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Invoice successfully paid!');

		} else {

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('customer-invoice');
		}
	}

	function refundInvoice()
	{
		$invoice_id = $_POST['invoice_id'];
		$refund_i = $_POST['refund_i'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/customer-invoice-refund',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id, 'refund_amount' => $refund_i),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Invoice successfully refunded!');

		} else {

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('customer-invoice');
		}
	}

	function payOwnerInvoice()
	{
		$invoice_id = $_POST['invoice_id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/set-vehicle-owner-invoice-paid',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Invoice successfully paid!');

		} else {

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

	function refundOwnerInvoice()
	{
		$invoice_id = $_POST['invoice_id'];
		$refund_i = $_POST['refund_i'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/vehicle-owner-invoice-refund',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id, 'refund_amount'  => $refund_i),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Invoice successfully refunded!');

		} else {

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

	function cancelInvoice()
	{
		$invoice_id = $_POST['invoice_id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/customer-invoice-cancel',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Invoice successfully cancelled!');

		} else {

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('customer-invoice');
		}
	}

	function cancelOwnerInvoice()
	{
		$invoice_id = $_POST['invoice_id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/vehicle-owner-invoice-cancel',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$this->session->set_flashdata('success', 'Invoice successfully cancelled!');

		} else {

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

	function getCustomerInvoiceById()
	{
		$invoice_id = $_POST['invoice_id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/get-customer-invoice-details-by-invoice-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;die();

		} else {
			$response = json_decode($response);

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('customer-invoice');
		}
	}

	function getVehicleOwnerInvoiceById()
	{
		$invoice_id = $_POST['invoice_id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/get-invoice-details-by-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $invoice_id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;die();

		} else {
			$response = json_decode($response);

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

	function printCustomerInvoice($id)
	{		
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments/get-customer-invoice-details-by-invoice-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$receipt = $this->load->view('payment/print_customer_invoice', $response, true);

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4',
				'margin_top' => 6,
				'margin_right' => 10,
				'margin_left' => 10,
				'margin_bottom' => 6
			]);
			$mpdf->WriteHTML($receipt);
			$mpdf->Output();

		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}

	function removeService()
	{
		$id = $_POST['id'];

		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/remove-extra-charges',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('extra_chrage_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;die();

		} else {

			$response = json_decode($response);

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

	function addService()
	{
		$service_name_v = $_POST['service_name_v'];
		$service_fee_v = $_POST['service_fee_v'];
		$invoice_id = $_POST['invoice_id'];
		
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/add-extra-charges',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('vehicle_owner_invoice_id' => $invoice_id,'charge_name' => $service_name_v,'charge_amount' => $service_fee_v),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			echo $response;die();

		} else {

			$response = json_decode($response);

			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);

			redirect('vehicle-owner-invoice');
		}
	}

	function printVehicleOwnerInvoice($id)
	{		
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->api_url.'payments-vehicle-owner/get-invoice-details-by-id',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => '',
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_POSTFIELDS => array('invoice_id' => $id),
			CURLOPT_HTTPHEADER => array(
				'Authorization: Bearer '.$this->session->userdata('token')
			),
		));

		$response = json_decode(curl_exec($curl));

		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);

		curl_close($curl);

		if ($httpcode == 200) {

			$receipt = $this->load->view('payment/print_vehicle_owner_invoice', $response, true);

			$mpdf = new \Mpdf\Mpdf([
				'format' => 'A4',
				'margin_top' => 6,
				'margin_right' => 10,
				'margin_left' => 10,
				'margin_bottom' => 6
			]);
			$mpdf->WriteHTML($receipt);
			$mpdf->Output();

		} else {
			$error = '<ul style="margin-bottom:0;">';
			foreach($response->error as $err) {
				$error.= '<li>'.$err.'</li>';
			}
			$error.= '</ul>';

			$this->session->set_flashdata('error', $error);
		}
	}
}