<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
          <tr>
            <th>Invoice No</th>
            <th>Booking No</th>
            <th>Customer</th>
            <th>Pickup Date</th>
            <th>Return Date</th>
            <th>Pickup Location</th>
            <th>Return Location</th>
            <th>Payment Status</th>
            <th>Payment</th>
            <th style="width:200px;">Action</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($data as $invoice) { ?>
            <tr>
              <td><?php echo $invoice->invoice_no; ?></td>
              <td><?php echo $invoice->booking_no; ?></td>
              <td><?php echo $invoice->full_name; ?></td>
              <td><?php echo $invoice->from_date; ?></td>
              <td><?php echo $invoice->to_date; ?></td>
              <td><?php echo $invoice->pickup_location; ?></td>
              <td><?php echo $invoice->return_location; ?></td>
              <td class="text-center">
                <?php if ($invoice->is_paid == 1) { ?>
                  <span class="badge badge-pill" style="width: 80px;background-color: #b6fff1;color: #1abc9c;">Paid</span>
                <?php } else { ?>
                  <span class="badge badge-pill" style="width: 80px;background-color: #dccff7;color: #805dca;">Pending</span>
                <?php } ?>
              </td>
              <td class="text-center">
                <?php if ($invoice->is_paid == 1 && $invoice->is_refund == 0) { ?>
                  <button class="btn btn-outline-danger btn-rounded" onclick="refundInvoice('<?php echo $invoice->id; ?>', '<?php echo $invoice->invoice_no; ?>', '<?php echo $invoice->booking_no; ?>', '<?php echo $invoice->full_name; ?>', '<?php echo $invoice->max_refund_amount; ?>');">Refund</button>
                <?php } else if ($invoice->is_paid == 0) { ?>
                  <button class="btn btn-outline-primary btn-rounded px-4" onclick="payInvoice(<?php echo $invoice->id; ?>);">Pay</button>
                <?php } ?>
              </td>
              <td class="text-center">
                <button class="btn btn-outline-danger btn-rounded" onclick="cancelInvoice(<?php echo $invoice->id; ?>);">Delete</button>
                <a href="<?php echo base_url(); ?>payment/printCustomerInvoice/<?php echo $invoice->id; ?>" class="btn btn-outline-secondary btn-rounded" target="_blank" id="print_v">Print</a>
                <button class="btn btn-outline-secondary btn-rounded" onclick="getCustomerInvoiceById(<?php echo $invoice->id; ?>);">View</button>
              </td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });
</script>