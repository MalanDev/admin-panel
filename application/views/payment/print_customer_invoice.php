<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style type="text/css">
    table {
      width: 100%;
    }

    td {
      width: 25%;
      height: 60px;
    }


    .total_td {
      height: 10px;
    }

    .subtitle {
      padding: 30px 0;
      font-size: 16px;
      border-bottom: 1px solid #ccc;
      padding-bottom: 0;
      margin-bottom: 10px;
      color: #092766;
    }
  </style>
</head>
<body>
  <table>
    <tr>
      <td style="width:60%;"><img src="<?php echo base_url(); ?>assets/assets/img/logo-3.png" style="width: 150px;"></td>
      <td style="width:40%;">
        <h2>Tuk Tuk Rent Negombo</h2>
        <p>282, Lewis Place, Negombo</p>
        <p>Local & Whatsapp : +94779903257</p>
        <p>Email : rentnegombo@gmail.com</p>
        <p>Web : www.tuktukrentalnegombo.com</p>
      </td>
    </tr>
  </table>

  <table style="border-bottom:1px solid #ccc;color: #092766;">
    <tr>
      <td style="height:20px;"><h3>Customer Invoice - <?php echo $data->invoice_no; ?></h3></td>
      <td style="text-align:right;height: 20px;"><h3>Booking No - <?php echo $data->booking_no; ?></h3></td>
    </tr>
  </table>

  <table>
    <tr>
      <td>
        <h4>Full Name</h4>
        <p><?php echo $data->full_name; ?></p>
      </td>
      <td>
        <h4>Mobile</h4>
        <p><?php echo $data->mobile_no; ?></p>
      </td>
      <td>
        <h4>Email</h4>
        <p><?php echo $data->email; ?></p>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        <h4>From</h4>
        <p><?php echo $data->pickup->pickup_date; ?></p>
      </td>
      <td>
        <h4>To</h4>
        <p><?php echo $data->return->return_date; ?></p>
      </td>
      <td></td>
      <td></td>      
    </tr>
  </table>


  <div class="subtitle" style="padding:10px 0;"><b>Selected Vehicle Fee</b></div>

  <table>
    <tr>
      <td style="width:40%;">
        <h4>Selected Vehicle Type</h4>
        <p><?php echo $data->vehicle->type; ?></p>
      </td>
      <td style="width:30%;">
        <h4>Price (€)</h4>
        <p><?php echo $data->vehicle->vehicle_type_fee; ?> * <?php echo $data->vehicle->no_of_days; ?> * <?php echo $data->vehicle->no_of_vehicle; ?></p>
      </td>
      <td style="width:30%;text-align:right;">
        <h4>Total (€)</h4>
        <p><?php echo $data->vehicle->total_vehicle_type_fee; ?></p>
      </td>
    </tr>
  </table>


  <div class="subtitle" style="padding:10px 0;"><b>Location Fee</b></div>

  <table>
    <tr>
      <td style="width:40%;height: 20px;">
        <h4>Pick-Up Fee (€)</h4>
      </td>
      <td style="width:30%;height: 20px;"></td>
      <td style="width:30%;height: 20px;text-align:right;">
       <p><?php echo $data->pickup->pickup_fee; ?></p>
     </td>
   </tr>
   <tr>
    <td style="width:40%;height: 20px;">
      <h4>Return Fee (€)</h4>
    </td>
    <td style="width:30%;height: 20px;"></td>
    <td style="width:30%;height: 20px;text-align:right;">
      <p><?php echo $data->return->return_fee; ?></p>
    </td>
  </tr>
</table>


<div class="subtitle" style="padding:10px 0;"><b>Service Fee</b></div>

<table>
  <tr>
    <td style="width:40%;height: 20px;">
      <h4>License</h4>
    </td>
    <td style="width:30%;height: 20px;">
      <h4>Price (€)</h4>
      <p><?php echo $data->service->license_fee; ?> * <?php echo $data->service->no_of_license; ?></p>
    </td>
    <td style="width:30%;height: 20px;text-align:right;">
      <h4>Total (€)</h4>
      <p><?php echo $data->service->total_license_fee; ?></p>
    </td>
  </tr>
  <tr>
    <td style="width:40%;height: 20px;">
      <h4>Insurance</h4>
    </td>
    <td style="width:30%;height: 20px;">
      <p><?php echo $data->service->insurance_fee; ?> * <?php echo $data->service->no_of_insurance; ?></p>
    </td>
    <td style="width:30%;height: 20px;text-align:right;">
      <p><?php echo $data->service->total_insurance_fee; ?></p>
    </td>
  </tr>
</table>

<?php if (count($data->extra_service) > 0) { ?>
  <div class="subtitle"><b>Extra Services Fee</b></div>

  <table>
    <tr>
      <td style="width:40%;height: 20px;">
      </td>
      <td style="width:30%;height: 20px;">
        <h4>Price (€)</h4>
      </td>
      <td style="width:30%;height: 20px;text-align:right;">
        <h4>Total (€)</h4>
      </td>
    </tr>
    <?php foreach ($data->extra_service as $extra_service) { 
      if ($extra_service->no_of_service != 0) {
        ?>
        <tr>
          <td style="width:40%;height: 20px;">
            <h4><?php echo $extra_service->service; ?></h4>
          </td>
          <td style="width:30%;height: 20px;">
            <p><?php echo $extra_service->fee; ?> * <?php echo $extra_service->no_of_service; ?></p>
          </td>
          <td style="width:30%;height: 20px;text-align:right;">
            <p><?php echo $extra_service->total_extra_service_fee; ?></p>
          </td>
        </tr>
      <?php }} ?>
    </table>
  <?php } ?>


  <div class="subtitle"><b>Total Fee</b></div>

  <table>
    <tr>
      <td class="total_td" style="width:40%;"></td>
      <td class="total_td" style="width:30%;"><h4>Sub Total (€)</h4></td>
      <td class="total_td" style="width:30%;text-align:right;"><p><?php echo $data->sub_total; ?></p></td>
    </tr>
    <tr>
      <td class="total_td" style="width:40%;"></td>
      <td class="total_td" style="width:30%;"><h4>Discount (€)</h4></td>
      <td class="total_td" style="width:30%;text-align:right;"><p><?php echo $data->discount; ?></p></td>
    </tr>
    <?php if ($data->is_refund == 1) { ?>
      <tr>
        <td class="total_td" style="width:40%;"></td>
        <td class="total_td" style="width:30%;"><h4>Refund Amount (€)</h4></td>
        <td class="total_td" style="width:30%;text-align:right;"><p><?php echo $data->refund_amount; ?></p></td>
      </tr>
    <?php } ?>
    <tr>
      <td class="total_td" style="width:40%;"></td>
      <td class="total_td" style="width:30%;"><h4>Grand Total (€)</h4></td>
      <td class="total_td" style="width:30%;text-align:right;"><h4><?php echo $data->total_amount; ?> (<?php echo ($data->is_paid == 1) ? 'Paid' : 'Non Paid';?>)</h4></td>
    </tr>

    <tr>
      <td>
        <br><br>
        <p>...........................................</p>
        <p style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature</p>
      </td>
      <td><br><br></td>
      <td>
        <br><br>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date('Y-m-d H:i'); ?></span>
        <p>...........................................</p>
        <p style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</p>
      </td>
    </tr>
  </table>

  <br>
  <hr>
  <p style="text-align:center;">Powered by crud.lk</p>

</body>
</html>