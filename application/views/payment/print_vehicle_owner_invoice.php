<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style type="text/css">
    table {
      width: 100%;
    }

    td {
      width: 25%;
      height: 60px;
    }


    .total_td {
      height: 10px;
    }

    .subtitle {
      padding: 30px 0;
      font-size: 18px;
      border-bottom: 1px solid #ccc;
      padding-bottom: 0;
      margin-bottom: 10px;
      color: #092766;
    }
  </style>
</head>
<body>

  <table>
    <tr>
      <td style="width:60%;"><img src="<?php echo base_url(); ?>assets/assets/img/logo-3.png" style="width: 150px;"></td>
      <td style="width:40%;">
        <h2>Tuk Tuk Rent Negombo</h2>
        <p>282, Lewis Place, Negombo</p>
        <p>Local & Whatsapp : +94779903257</p>
        <p>Email : rentnegombo@gmail.com</p>
        <p>Web : www.tuktukrentalnegombo.com</p>
      </td>
    </tr>
  </table>

  <br>
  <h2>Vehicle Owner Invoice - <?php echo $data->invoice_no; ?></h2>

  <div class="subtitle" style="padding:10px 0;"><b>Personal Details</b></div>

  <table>
    <tr>
      <td>
        <h4>Owner Name</h4>
        <p><?php echo $data->agreement->vehicle->vehicle_owner->first_name.' '.$data->agreement->vehicle->vehicle_owner->last_name; ?></p>
      </td>
      <td>
        <h4>Vehicle No</h4>
        <p><?php echo $data->agreement->vehicle->registration_no; ?></p>
      </td>    
      <td>
        <h4>From</h4>
        <p><?php echo $data->agreement->from_date; ?></p>
      </td>
      <td>
        <h4>To</h4>
        <p><?php echo $data->agreement->to_date; ?></p>
      </td>  
    </tr>
  </table>


  <div class="subtitle"><b>Rate Details</b></div>

  <table>
    <tr>
      <td style="height: 20px;width: 75%;">
        <h4></h4>
      </td>
      <td style="height: 20px;text-align:right;">
        <h4>Fee (LKR)</h4>
      </td>
    </tr>
    <tr>
      <td style="height: 20px;width: 75%;">
        <h4>Per Day Rate</h4>
      </td>
      <td style="height: 20px;text-align:right;">
        <p><?php echo $data->rate; ?></p>
      </td>
    </tr>
    <tr>
      <td style="height: 20px;width: 75%;">
        <h4>Total for (<?php echo $data->booking_days; ?>) Days</h4>
      </td>
      <td style="height: 20px;text-align:right;">
        <p><?php echo $data->total_rate; ?></p>
      </td>
    </tr>
  </table>

  <?php if (count($data->vehicle_owner_invoice_extra_chargers) > 0) { ?>
    <div class="subtitle"><b>Extra Charges Details</b></div>

    <table>
     <tr>
      <td style="height: 20px;width: 75%;">
        <h4></h4>
      </td>
      <td style="height: 20px;text-align:right;">
        <h4>Fee (LKR)</h4>
      </td>
    </tr>
    <?php foreach ($data->vehicle_owner_invoice_extra_chargers as $vehicle_owner_invoice_extra_charge) { 
      if ($vehicle_owner_invoice_extra_charge->is_remove != 1) {
        ?>
        <tr>
          <td style="height: 20px;width: 75%;">
            <h4><?php echo $vehicle_owner_invoice_extra_charge->charge_name; ?></h4>
          </td>
          <td style="height: 20px;text-align:right;">
            <p>- <?php echo $vehicle_owner_invoice_extra_charge->charge_amount; ?></p>
          </td>
        </tr>
      <?php }} ?>
    </table>
  <?php } ?>


  <div class="subtitle"><b>Total Fee</b></div>

  <table>
    <?php if ($data->is_refund == 1) { ?>
      <tr>
        <td class="total_td" style="width: 75%;"><h4 style="font-weight: normal;">Refund Amount (LKR)</h4></td>
        <td class="total_td" style="text-align:right;"><h4 style="font-weight: normal;"><?php echo $data->refund_amount; ?></h4></td>
      </tr>
    <?php } ?>
    <tr>
      <td class="total_td" style="width: 75%;"><h4>Grand Total (LKR)</h4></td>
      <td class="total_td" style="text-align:right;"><h4><?php echo $data->grand_total; ?> (<?php echo ($data->is_paid == 1) ? 'Paid' : 'Non Paid';?>)</h4></td>
    </tr>
    <tr>
      <td>
        <br>
        <p>...........................................</p>
        <p style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature</p>
      </td>
      <td>
        <br>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date('Y-m-d H:i'); ?></span>
        <p>...........................................</p>
        <p style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</p>
      </td>
    </tr>
  </table>


  <div class="subtitle" style="padding:10px 0;font-size: 14px;margin-bottom: 0px;"><b>Document Details</b></div>

  <table>
    <tr>
      <td>
        <h5>License Expiry Date</h5>
        <p><?php echo $data->agreement->vehicle->revenue_license_exp_date; ?></p>
      </td>
      <td>
        <h5>Smoke Test Expiry Date</h5>
        <p><?php echo $data->agreement->vehicle->smoke_test_exp_date; ?></p>
      </td>
      <td>
        <h5>Insurance Expiry Date</h5>
        <p><?php echo $data->agreement->vehicle->insurance_exp_date; ?></p>
      </td> 
    </tr>
  </table>


  <div class="subtitle" style="padding:10px 0;font-size: 14px;margin-bottom: 0px;"><b>Bank Details</b></div>

  <table>
    <tr>
      <td>
        <h5>Account Name</h5>
        <p><?php echo $data->agreement->vehicle->vehicle_owner->vehicle_owner_bank_details->account_name; ?></p>
      </td>
      <td>
        <h5>Account No</h5>
        <p><?php echo $data->agreement->vehicle->vehicle_owner->vehicle_owner_bank_details->account_no; ?></p>
      </td>
      <td>
        <h5>Bank</h5>
        <p><?php echo $data->agreement->vehicle->vehicle_owner->vehicle_owner_bank_details->bank->bank_name; ?></p>
      </td>
      <td>
        <h5>Branch</h5>
        <p><?php echo $data->agreement->vehicle->vehicle_owner->vehicle_owner_bank_details->branch; ?></p>
      </td>
    </tr>
  </table>

  <hr>
  <p style="text-align:center;">Powered by crud.lk</p>

</body>
</html>