<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

  <style type="text/css">
    .booking_col {
      display: none;
    }

    #invoiceNo-error, #booking_no-error{
      position: absolute;
      bottom: -28px;
      left: 20px;
    }
  </style>
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Customer Invoice</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Payment Data</li>
                <li>Customer Invoice</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="customer_invoice_form">
          <div class="row layout-spacing">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group col-md-3 d-flex pt-4">
                      <div class="custom-control custom-radio radio-primary">
                        <input type="radio" id="basicRadio5" name="basicRadio" class="custom-control-input" checked value="invoice">
                        <label class="custom-control-label" for="basicRadio5">Invoice No</label>
                      </div>
                      <div>&nbsp;&nbsp;&nbsp;&nbsp;</div>
                      <div class="custom-control custom-radio radio-primary">
                        <input type="radio" id="basicRadio2" name="basicRadio" class="custom-control-input" value="booking">
                        <label class="custom-control-label" for="basicRadio2">Booking No</label>
                      </div>
                      <!-- <input type="hidden" id="radio_btn_val"> -->
                    </div>
                    <div class="form-group col-md-3 invoice_col">
                      <label for="exampleFormControlInput1">Invoice No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="invoiceNo" name="invoiceNo">
                        <option value="" disabled selected hidden>Select Invoice No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3 booking_col">
                      <label for="exampleFormControlInput1">Booking No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="booking_no" name="booking_no" style="width:100%;">
                        <option value="" disabled selected hidden>Select Booking No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Status</label>
                      <select class="form-control-rounded form-control" id="status">
                        <option value="" disabled selected hidden>Select Status</option>
                        <option value="1">Paid</option>
                        <option value="0">Not Paid</option>
                      </select>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate">
                    </div>

                    <div class="form-group mt-4 pt-2 col-md-3 offset-md-3 text-right">
                      <input type="submit" name="time" class="btn btn-button-7 btn-rounded mt-2" value="Search">
                      <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-2" value="Cancel" id="reset">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Invoice No</th>
                        <th>Booking No</th>
                        <th>Customer</th>
                        <th>Pickup Date</th>
                        <th>Return Date</th>
                        <th>Pickup Location</th>
                        <th>Return Location</th>
                        <th>Payment Status</th>
                        <th>Payment</th>
                        <th style="width:200px;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--  END CONTENT PART  -->
  </div>
  <!-- END MAIN CONTAINER -->




  <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="customer_invoice_view">
    <div class="modal-dialog modal-xl" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Customer Invoice - <span id="invoice_no_label_v">100001</span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">
         <form> 
          <div class="row">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="row">
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Invoice No</label>
                  <input type="text" class="form-control-rounded form-control" id="invoice_no_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <input type="text" class="form-control-rounded form-control" id="booking_no_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Booking Date</label>
                  <input type="text" class="form-control-rounded form-control" id="booking_date_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">From</label>
                  <input type="text" class="form-control-rounded form-control" id="pickup_date_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">To</label>
                  <input type="text" class="form-control-rounded form-control" id="return_date_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Customer Name</label>
                  <input type="text" class="form-control-rounded form-control" id="full_name_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Mobile No</label>
                  <input type="text" class="form-control-rounded form-control" id="mobile_no_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Email</label>
                  <input type="text" class="form-control-rounded form-control" id="email_v" readonly>
                </div>
              </div>
            </div> 
          </div>

          <!-- Selected Vehicle Fee -->
          <div class="row mt-3" style="padding:0 20px;">
            <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Selected Vehicle Fee</div>
            <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
            <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
          </div>

          <div class="row mt-3" style="padding:0 25px;">
            <div class="col-md-6 px-0" id="selected_vehicle_2_v"></div>
            <div class="col-md-4 px-0" style="text-align: right;"><span id="vehicle_price_2_v"></span>&nbsp;*&nbsp;<span id="no_of_days_2_v"></span>&nbsp;*&nbsp;<span id="vehicle_count_2_v"></span></div>
            <div class="col-md-2 px-0" style="text-align: right;"><span id="total_vehicle_fee_2_v" style="padding-left: 20px;"></span></div>
          </div>

          <!-- Location Fee -->
          <div class="row mt-3" style="padding:0 20px;">
            <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Location Fee
            </div>
            <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
            <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
          </div>

          <div class="row mt-3" style="padding:0 25px;">
            <div class="col-md-6 px-0">Pick-Up Fee</div>
            <div class="col-md-4 px-0" style="text-align: right;"><span id="pick_fee_2_v" style="padding-left: 20px;"></span></div>
            <div class="col-md-2 px-0" style="text-align: right;"><span id="total_pick_fee_2_v" style="padding-left: 20px;"></span></div>
          </div>

          <div class="row mt-3" style="padding:0 25px;">
            <div class="col-md-6 px-0">Return Fee</div>
            <div class="col-md-4 px-0" style="text-align: right;"><span id="retur_fee_2_v" style="padding-left: 20px;"></span></div>
            <div class="col-md-2 px-0" style="text-align: right;"><span id="total_retur_fee_2_v" style="padding-left: 20px;"></span></div>
          </div>


          <!-- Service Fee -->
          <div class="row mt-3" style="padding:0 20px;">
            <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Service Fee</div>
            <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
            <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
          </div>

          <div class="row mt-3" style="padding:0 25px;">
            <div class="col-md-6 px-0">License</div>
            <div class="col-md-4 px-0" style="text-align: right;"><span id="licen_fee_2_v" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span id="no_of_licen_2_v"></span></div>
            <div class="col-md-2 px-0" style="text-align: right;"><span id="total_licen_fee_2_v" style="padding-left: 20px;"></span></div>
          </div>

          <div class="row mt-3" style="padding:0 25px;">
            <div class="col-md-6 px-0">Insurance</div>
            <div class="col-md-4 px-0" style="text-align: right;"><span id="insuran_fee_2_v" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span id="no_of_insuran_2_v"></span></div>
            <div class="col-md-2 px-0" style="text-align: right;"><span id="total_insuran_fee_2_v" style="padding-left: 20px;"></span></div>
          </div>


          <!-- Extra Service Fee -->
          <div class="row mt-3 extra_service_table_3_v_heading" style="padding:0 20px;">
            <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Extra Service Fee</div>
            <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
            <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
          </div>

          <div id="extra_service_table_3_v"></div>

          <hr>
          <!-- Coupon Code -->
          <div class="row mt-4" style="padding:0 25px;">
            <div class="col-md-2 offset-md-8 px-0 text-right pr-2"><b>Sub Total :</b></div>
            <div class="col-md-2 px-0" style="text-align: right;"><b id="sub_total_v"></b></div>
          </div>
          <div class="row mt-2" style="padding:0 25px;">
            <div class="col-md-2 offset-md-8 px-0 text-right pr-2">Discount :</div>
            <div class="col-md-2 px-0" id="discount_v" style="text-align: right;"></div>
          </div>
          <div class="row mt-2" style="padding:0 25px;">
            <div class="col-md-2 offset-md-8 px-0 text-right pr-2">Refund Amount :</div>
            <div class="col-md-2 px-0" id="refund_v" style="text-align: right;"></div>
          </div>
          <div class="row mt-3" style="padding:0 25px;">
            <div class="col-md-6 px-0 text-right" style="background-color:#092766;padding: 10px;"></div>
            <div class="col-md-4 px-0 text-right pr-2" style="background-color:#092766;padding: 10px;"><h5 class="text-bold text-white">Grand Total:</h5></div>
            <div class="col-md-2 px-0" style="background-color:#092766;padding: 10px;text-align: right;"><h5 class="text-bold text-white" id="total_amount_v"></h5></div>
          </div>


          <!-- User Firm -->
          <div class="row mt-5">
            <div class="form-group mt-3 col-md-4 offset-md-8 text-right">
              <span id="print_span"></span>
              <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
</div>


<div class="modal fade bd-example2-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_invoice">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Invoice No - <span id="invoiceno_i"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="refund_form">
          <div class="row p-2">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="row">
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Invoice No</label>
                  <input type="text" class="form-control-rounded form-control" id="invoice_no_i" name="invoice_no_i" readonly>
                  <input type="hidden" name="invoice_id_i" id="invoice_id_i">
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <input type="text" class="form-control-rounded form-control" id="booking_no_i" name="booking_no_i" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Customer</label>
                  <input type="text" class="form-control-rounded form-control" id="customer_i" name="customer_i" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Refund Amount</label>
                  <input type="number" class="form-control-rounded form-control" id="refund_i" name="refund_i" required>
                </div>
                <div class="form-group offset-md-8 col-md-4 text-right mt-4">
                 <input type="submit" name="time" class="btn btn-button-7 btn-rounded px-4" value="Refund">
                 <input type="reset" name="time" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
               </div>
             </div>
           </div>
         </div> 
       </form>
     </div>
   </div>
 </div>
</div>

<input type="hidden" id="search" value="0">

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });


  getInvoiceNumbers();

  function getInvoiceNumbers() {
   $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getInvoiceNumbers',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#invoiceNo').append($('<option>', { 
              value: item.id,
              text : item.invoice_no 
            }));             
          });
        }
        $(".cs-overlay").css('visibility', 'hidden');
        getBookingNumbers();
      }        
    }
  });
 }


 function getBookingNumbers() {
   $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getBookingNumbers',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#booking_no').append($('<option>', { 
              value: item.id,
              text : item.booking_no 
            }));             
          });
        }
        $(".cs-overlay").css('visibility', 'hidden');
      } 

      $('#customer_invoice_form').submit();       
    }
  });
 }

 // $('#radio_btn_val').val('invoice');
 $('input[type=radio][name=basicRadio]').change(function() {
  if (this.value == 'invoice') {
    $('.invoice_col').show();
    $('.booking_col').hide();
    // $('#radio_btn_val').val('invoice');
  }
  else if (this.value == 'booking') {
    $('.invoice_col').hide();
    $('.booking_col').show();
    // $('#radio_btn_val').val('booking');
  }
});

 $('#invoiceNo').change(function(){
  $(this).valid();
});

 $('#booking_no').change(function(){
  $(this).valid();
});


//  $(document).ready(function() {

//   $("#customer_invoice_form").validate({
//     rules: {
//       invoiceNo : {
//         required: function(element){
//           return $("#radio_btn_val").val()=="invoice";
//         },
//       },
//       booking_no : {
//         required: function(element){
//           return $("#radio_btn_val").val()=="booking";
//         },
//       },
//     },
//     messages : {
//       invoiceNo: {
//         required: "Please select Invoice Number",
//       },
//       booking_no: {
//         required: "Please select Booking No",
//       },
//     }
//   });
// });


$('#customer_invoice_form').submit(function(evt) {
  evt.preventDefault();

  // if( $('#customer_invoice_form').valid() ) {

    // let radio_val = $('input[type=radio][name=basicRadio]:checked').val();
    let invoiceNo = $('#invoiceNo').val();
    let booking_no = $('#booking_no').val();
    let status = $('#status').val();
    let fromDate = $('#fromDate').val();
    let toDate = $('#toDate').val();

    // if (radio_val == "invoice") {
    //   booking_no = -1;
    // } else {
    //   invoiceNo = -1;
    // }

    if (invoiceNo == null) {
      invoiceNo = -1;
    }

    if (booking_no == null) {
      booking_no = -1;
    }

    if (fromDate == "") {
      fromDate = -1;
    }

    if (toDate == "") {
      toDate = -1;
    }

    if (status == null) {
      status = -1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>payment/customerInvoiceSearch',
      data:{invoiceNo,booking_no,status,fromDate,toDate},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $('#search').val(1);
        $('#table_div').html(res);
        $(".cs-overlay").css('visibility', 'hidden');   
      }
    });

  // } 
});


function payInvoice(invoice_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/payInvoice',
    data:{invoice_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let search = $('#search').val();
     if (search == 1) {
       $.confirm({
        title: 'Success!',
        content: 'Invoice successfully paid!',
        type: 'green',
        draggable: false,
        buttons: {
          tryAgain: {
            text: 'Close',
            action: function(){
             $('#customer_invoice_form').submit();
           }
         },
       }
     }); 
     } else {
      location.reload();
    } 
  }
});
}


$('#refund_form').on('submit', function(e) {
  e.preventDefault();

  let invoice_id = $('#invoice_id_i').val();
  let refund_i = $('#refund_i').val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/refundInvoice',
    data:{invoice_id, refund_i},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     $('#modal_invoice').modal('hide');
     let search = $('#search').val();
     if (search == 1) {
       $.confirm({
        title: 'Success!',
        content: 'Invoice successfully refunded!',
        type: 'green',
        draggable: false,
        buttons: {
          tryAgain: {
            text: 'Close',
            action: function(){
             $('#customer_invoice_form').submit();
           }
         },
       }
     }); 
     } else {
      location.reload();
    } 
  }
});

});

function cancelInvoice(invoice_id) {
  $.confirm({
    title: 'Confirm!',
    content: 'Are you sure, do you want to delete this?',
    type: 'red',
    buttons: {
      confirm: function(){
       $.ajax({
        type:'post',
        url:'<?php echo base_url(); ?>payment/cancelInvoice',
        data:{invoice_id},
        beforeSend: function(){
          $(".cs-overlay").css('visibility', 'visible');
        },
        success:function (res) {
         let search = $('#search').val();
         if (search == 1) {
           $.confirm({
            title: 'Success!',
            content: 'Invoice successfully deleted!',
            type: 'green',
            draggable: false,
            buttons: {
              tryAgain: {
                text: 'Close',
                action: function(){
                 $('#customer_invoice_form').submit();
               }
             },
           }
         }); 
         } else {
          location.reload();
        } 
      }
    });
     },
     cancel : {

     }
   }
 });

}


function getCustomerInvoiceById(invoice_id) {

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getCustomerInvoiceById',
    data:{invoice_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        let print_url = '<?php echo base_url(); ?>payment/printCustomerInvoice/'+result.data.id;
        $('#invoice_no_v').val(result.data.invoice_no);
        $('#invoice_no_label_v').html(result.data.invoice_no);
        $('#booking_date_v').val(result.data.booking_date);
        $('#booking_no_v').val(result.data.booking_no);
        $('#pickup_date_v').val(result.data.pickup.pickup_date);
        $('#return_date_v').val(result.data.return.return_date);
        $('#full_name_v').val(result.data.full_name);
        $('#mobile_no_v').val(result.data.mobile_no);
        $('#email_v').val(result.data.email);
        $('#selected_vehicle_2_v').html(result.data.vehicle.type);
        $('#vehicle_price_2_v').html(result.data.vehicle.vehicle_type_fee);
        $('#no_of_days_2_v').html(result.data.vehicle.no_of_days);
        $('#vehicle_count_2_v').html(result.data.vehicle.no_of_vehicle);
        $('#total_vehicle_fee_2_v').html(result.data.vehicle.total_vehicle_type_fee);
        $('#pick_fee_2_v').html(result.data.pickup.pickup_fee);
        $('#retur_fee_2_v').html(result.data.return.return_fee);
        $('#total_pick_fee_2_v').html(result.data.pickup.pickup_fee);
        $('#total_retur_fee_2_v').html(result.data.return.return_fee);
        $('#licen_fee_2_v').html(result.data.service.license_fee);
        $('#no_of_licen_2_v').html(result.data.service.no_of_license);
        $('#total_licen_fee_2_v').html(result.data.service.total_license_fee);
        $('#insuran_fee_2_v').html(result.data.service.insurance_fee);
        $('#no_of_insuran_2_v').html(result.data.service.no_of_insurance);
        $('#total_insuran_fee_2_v').html(result.data.service.total_insurance_fee);
        $('#sub_total_v').html(result.data.sub_total);
        $('#discount_v').html(result.data.discount);    
        $('#refund_v').html(result.data.refund_amount);         
        $('#total_amount_v').html(result.data.total_amount);

        $('#print_span').empty();
        $('#print_span').append('<a href="'+print_url+'" class="btn btn-outline-secondary btn-rounded" target="_blank" id="print_v">Print</a>');

        $('#extra_service_table_3_v').empty();

        $.each(result.data.extra_service, function (i, item) {
          if (item.no_of_service != 0) {
            $('#extra_service_table_3_v').append('<div class="row mt-3" style="padding:0 25px;"> <div class="col-md-6 px-0">'+item.service+'</div> <div class="col-md-4 px-0">'+item.fee+' * '+item.no_of_service+'</div> <div class="col-md-2 px-0">'+item.total_extra_service_fee+'</div> </div>');
          }   
        });

        if ($('#extra_service_table_3_v').is(':empty')){
          $('.extra_service_table_3_v_heading').hide();
        } else {
          $('.extra_service_table_3_v_heading').show();
        }

        $('#customer_invoice_view').modal('show');
        $(".cs-overlay").css('visibility', 'hidden');
      }
    }
  });
}

$('#reset').click(function() {
  location.reload();
});

function refundInvoice(id, invoice_no, booking_no, name, max_refund_amount) {
  $('#invoice_id_i').val(id);
  $('#invoice_no_i').val(invoice_no);
  $('#invoiceno_i').html(invoice_no);
  $('#booking_no_i').val(booking_no);
  $('#customer_i').val(name);
  $('#refund_i').attr('min',0);
  $('#refund_i').attr('max',max_refund_amount);
  $('#modal_invoice').modal('show');
}

</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>