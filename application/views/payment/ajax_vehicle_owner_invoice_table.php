<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering"
      class="table table-striped table-bordered table-hover" style="width:100%">
      <thead>
        <tr>
          <th>Invoice No</th>
          <th>Booking No</th>
          <th>Owner</th>
          <th>Agreement No</th>
          <th>Pickup Date</th>
          <th>Return Date</th>
          <th>Vehicle No</th>
          <th>Milage Start</th>
          <th>Milage End</th>
          <th>Payment Status</th>
          <th>Payment</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $owner_invoice) { ?>
          <tr>
            <td><?php echo $owner_invoice->invoice_no; ?></td>
            <td><?php echo $owner_invoice->booking_no; ?></td>
            <td><?php echo $owner_invoice->owner_name; ?></td>
            <td><?php echo $owner_invoice->agreement_no; ?></td>
            <td><?php echo $owner_invoice->from_date; ?></td>
            <td><?php echo $owner_invoice->to_date; ?></td>
            <td><?php echo $owner_invoice->registration_no; ?></td>
            <td><?php echo $owner_invoice->milage_starts; ?></td>
            <td><?php echo $owner_invoice->milage_ends; ?></td>
            <td class="text-center">
              <?php if ($owner_invoice->is_paid == 1) { ?>
                <span class="badge badge-pill" style="width: 80px;background-color: #b6fff1;color: #1abc9c;">Paid</span>
              <?php } else { ?>
                <span class="badge badge-pill" style="width: 80px;background-color: #dccff7;color: #805dca;">Pending</span>
              <?php } ?>
            </td>
            <td class="text-center">
              <?php if ($owner_invoice->is_paid == 1 && $owner_invoice->is_refund == 0) { ?>
                <button class="btn btn-outline-danger btn-rounded" onclick="refundInvoice('<?php echo $owner_invoice->invoice_id; ?>', '<?php echo $owner_invoice->invoice_no; ?>', '<?php echo $owner_invoice->booking_no; ?>', '<?php echo $owner_invoice->owner_name; ?>', '<?php echo $owner_invoice->max_refund_amount; ?>');">Refund</button>
              <?php } else if ($owner_invoice->is_paid == 0) { ?>
                <button class="btn btn-outline-primary btn-rounded px-4" onclick="payInvoice(<?php echo $owner_invoice->invoice_id; ?>);">Pay</button>
              <?php } ?>
            </td>
            <td class="text-center">
             <button class="btn btn-outline-danger btn-rounded" onclick="cancelInvoice(<?php echo $owner_invoice->invoice_id; ?>);">Delete</button>
             <a href="<?php echo base_url(); ?>payment/printVehicleOwnerInvoice/<?php echo $owner_invoice->invoice_id; ?>" class="btn btn-outline-secondary btn-rounded" target="_blank" id="print_v">Print</a>
             <button class="btn btn-outline-secondary btn-rounded" onclick="getVehicleOwnerInvoiceById(<?php echo $owner_invoice->invoice_id; ?>);">View</button>
           </td>
         </tr>
       <?php } ?>
     </tbody>
   </table>
 </div>
</div>
</div>

<script type="text/javascript">
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });
</script>