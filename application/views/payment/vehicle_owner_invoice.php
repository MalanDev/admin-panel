<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Vehicle Owner Invoice</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Payment Data</li>
                <li>Vehicle Owner Invoice</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="vehicle_owner_invoice_form">
          <div class="row layout-spacing">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Invoice No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="invoiceNo" name="invoiceNo">
                        <option value="" disabled selected hidden>Select Invoice No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Status</label>
                      <select class="form-control-rounded form-control search_dropdown" id="status" name="status">
                        <option value="" disabled selected hidden>Select Status</option>
                        <option value="1">Paid</option>
                        <option value="0">Not Paid</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Booking No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="bookingNo" name="bookingNo">
                        <option value="" disabled selected hidden>Select Booking No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Agreement No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="agreementno" name="agreementno">
                        <option value="" disabled selected hidden>Select Agreement No</option>
                      </select>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate" name="fromDate">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate" name="toDate">
                    </div>

                    <div class="form-group mt-4 pt-2 col-md-3 offset-md-3 text-right">
                      <input type="submit" class="btn btn-button-7 btn-rounded mt-2 search_btn" value="Search">
                      <input type="reset" class="btn btn-button-6 btn-rounded mt-2" value="Cancel" id="reset">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Invoice No</th>
                      <th>Booking No</th>
                      <th>Owner</th>
                      <th>Agreement No</th>
                      <th>Pickup Date</th>
                      <th>Return Date</th>
                      <th>Vehicle No</th>
                      <th>Milage Start</th>
                      <th>Milage End</th>
                      <th>Payment Status</th>
                      <th>Payment</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->



<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="owner_invoice_modal">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Vehicle Owner Invoice - <span id="invoice_v2"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <form> 
        <div class="row">

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="statbox widget box box-shadow mt-4">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Personal Details</h4>
                  </div>                                                      
                </div>
              </div>

              <div class="row">
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Invoice No</label>
                  <input type="text" class="form-control-rounded form-control" id="invoice_v" readonly>

                  <input type="hidden" class="form-control-rounded form-control" id="invoice_id_v">
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Date</label>
                  <input type="text" class="form-control-rounded form-control" id="date_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <input type="text" class="form-control-rounded form-control" id="booking_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Agreement No</label>
                  <input type="text" class="form-control-rounded form-control" id="agreement_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">From</label>
                  <input type="text" class="form-control-rounded form-control" id="from_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">To</label>
                  <input type="text" class="form-control-rounded form-control" id="to_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Owner Name</label>
                  <input type="text" class="form-control-rounded form-control" id="owner_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Vehicle No</label>
                  <input type="text" class="form-control-rounded form-control" id="vehicle_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Revenue License Expiry Date</label>
                  <input type="text" class="form-control-rounded form-control" id="license_expiry_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Smoke Test Expiry Date</label>
                  <input type="text" class="form-control-rounded form-control" id="smoke_expiry_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Insurance Expiry Date</label>
                  <input type="text" class="form-control-rounded form-control" id="insurance_expiry_v" readonly>
                </div>
              </div>
            </div>

            <div class="statbox widget box box-shadow mt-4">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Bank Details</h4>
                  </div>                                                      
                </div>
              </div>

              <div class="row">
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Account Name</label>
                  <input type="text" class="form-control-rounded form-control" id="account_name_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Account No</label>
                  <input type="text" class="form-control-rounded form-control" id="account_no_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Bank</label>
                  <input type="text" class="form-control-rounded form-control" id="bank_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Branch</label>
                  <input type="text" class="form-control-rounded form-control" id="branch_v" readonly>
                </div>
              </div>
            </div>


            <div class="statbox widget box box-shadow mt-4">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Rate Details</h4>
                  </div>                                                      
                </div>
              </div>

              <div class="row">
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Per Day Rate (LKR)</label>
                  <input type="text" class="form-control-rounded form-control" id="per_day_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Total for (<span id="rate_days_v"></span>) Days (LKR)</label>
                  <input type="text" class="form-control-rounded form-control" id="total_rate_v" readonly>
                </div>
              </div>
            </div>


            <div class="statbox widget box box-shadow mt-4">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Extra Charges Details</h4>
                  </div>                                                      
                </div>
              </div>

              <div class="row">
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Service</label>
                  <input type="text" class="form-control-rounded form-control" id="service_name_v">
                </div>
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Fee (LKR)</label>
                  <input type="text" class="form-control-rounded form-control" id="service_fee_v">
                </div>
                <div class="form-group mt-4 col-md-2">
                  <a href='javascript:void' class="btn btn-button-7 btn-rounded" id="service_btn_v">Add</a>
                </div>
              </div>
            </div>
          </div> 
        </div>

        <!-- Selected Vehicle Fee -->
        <div class="row mt-3" style="padding:0 20px;">
          <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Services</div>
          <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;"></div>
          <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;padding-left: 0;text-align: right;">Total (LKR)</div>
        </div>

        <div id="service_div"></div>

        <hr>
        <!-- Coupon Code -->
        <div class="row mt-3" style="padding:0 25px;">
          <div class="col-md-6 px-0 text-right" style="background-color:#092766;padding: 10px;"></div>
          <div class="col-md-4 px-0 text-right pr-2" style="background-color:#092766;padding: 10px;">
            <p class="text-bold text-white">Refund Amount:</p>
            <h5 class="text-bold text-white">Grand Total:</h5>
          </div>
          <div class="col-md-2 px-0" style="background-color:#092766;padding: 10px;text-align: right;">
            <p class="text-bold text-white" id="refund_amount_v"></p>
            <h5 class="text-bold text-white" id="total_amount_v"></h5>
          </div>
        </div>


        <!-- User Firm -->
        <div class="row mt-5">
          <div class="form-group mt-3 col-md-4 offset-md-8 text-right">
            <span id="print_span"></span>
            <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
</div>




<div class="modal fade bd-example2-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_invoice">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Invoice No - <span id="invoiceno_i"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="refund_form">
          <div class="row p-2">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="row">
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Invoice No</label>
                  <input type="text" class="form-control-rounded form-control" id="invoice_no_i" name="invoice_no_i" readonly>
                  <input type="hidden" name="invoice_id_i" id="invoice_id_i">
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <input type="text" class="form-control-rounded form-control" id="booking_no_i" name="booking_no_i" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Owner</label>
                  <input type="text" class="form-control-rounded form-control" id="owner_i" name="owner_i" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Refund Amount</label>
                  <input type="number" class="form-control-rounded form-control" id="refund_i" name="refund_i">
                </div>
                <div class="form-group offset-md-8 col-md-4 text-right mt-4">
                 <input type="submit" name="time" class="btn btn-button-7 btn-rounded px-4" value="Refund">
                 <input type="reset" name="time" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
               </div>
             </div>
           </div>
         </div> 
       </form>
     </div>
   </div>
 </div>
</div>


<input type="hidden" id="search" value="0">

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });

  getInvoiceNumbers();

  function getInvoiceNumbers() {
   $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getVehicleOwnerInvoiceNumbers',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#invoiceNo').append($('<option>', { 
              value: item.id,
              text : item.invoice_no 
            }));             
          });
        }
        $(".cs-overlay").css('visibility', 'hidden');
      }  

      $('#vehicle_owner_invoice_form').submit();
    }
  });
 }

 $('#invoiceNo').on('change', function () {
  let invoiceId = $(this).val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getBookingsByInvoiceId',
    data:{invoiceId},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $('#bookingNo').empty();
        $('#bookingNo').append('<option value="" selected>Select Booking No</option>');
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#bookingNo').append($('<option>', { 
              value: item.id,
              text : item.booking_no 
            }));             
          });

          $('#bookingNo').trigger('change');
        }       

        $(".cs-overlay").css('visibility', 'hidden');
      }        
    }
  });
  
});


 $('#bookingNo').on('change', function () {
  let bookingId = $(this).val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getAgreementsByBookingId',
    data:{bookingId},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $('#agreementno').empty();
        $('#agreementno').append('<option value="" selected>Select Agreement No</option>');
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#agreementno').append($('<option>', { 
              value: item.id,
              text : item.agreement_no 
            }));             
          });
        }       

        $(".cs-overlay").css('visibility', 'hidden');
      }        
    }
  });
  
});

 $('#reset').click(function() {
  location.reload();
});


 $('#vehicle_owner_invoice_form').submit(function(evt) {
  evt.preventDefault();

  let invoiceNo = $('#invoiceNo').val();
  let bookingNo = $('#bookingNo').val();
  let agreementno = $('#agreementno').val();
  let status = $('#status').val();
  let fromDate = $('#fromDate').val();
  let toDate = $('#toDate').val();

  if (invoiceNo == null || invoiceNo == "") {
    invoiceNo = -1;
  }

  if (bookingNo == null || bookingNo == "") {
    bookingNo = -1;
  }

  if (agreementno == null || agreementno == "") {
    agreementno = -1;
  }

  if (fromDate == "") {
    fromDate = -1;
  }

  if (toDate == "") {
    toDate = -1;
  }

  if (status == null) {
    status = -1;
  }

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/VehicleOwnerInvoiceSearch',
    data:{invoiceNo,bookingNo,agreementno,status,fromDate,toDate},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $('#search').val(1);
      $('#table_div').html(res);
      $(".cs-overlay").css('visibility', 'hidden');   
    }
  });

});


 function payInvoice(invoice_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/payOwnerInvoice',
    data:{invoice_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let search = $('#search').val();
     if (search == 1) {
       $.confirm({
        title: 'Success!',
        content: 'Invoice successfully paid!',
        type: 'green',
        draggable: false,
        buttons: {
          tryAgain: {
            text: 'Close',
            action: function(){
             $('#vehicle_owner_invoice_form').submit();
           }
         },
       }
     }); 
     } else {
      location.reload();
    } 
  }
});
}


$('#refund_form').on('submit', function(e) {
  e.preventDefault();

  let invoice_id = $('#invoice_id_i').val();
  let refund_i = $('#refund_i').val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/refundOwnerInvoice',
    data:{invoice_id, refund_i},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $('#modal_invoice').modal('hide');
      let search = $('#search').val();
      if (search == 1) {
       $.confirm({
        title: 'Success!',
        content: 'Invoice successfully refunded!',
        type: 'green',
        draggable: false,
        buttons: {
          tryAgain: {
            text: 'Close',
            action: function(){
             $('#vehicle_owner_invoice_form').submit();
           }
         },
       }
     }); 
     } else {
      location.reload();
    } 
  }
});
});


function cancelInvoice(invoice_id) {
  $.confirm({
    title: 'Confirm!',
    content: 'Are you sure, do you want to delete this?',
    type: 'red',
    buttons: {
      confirm: function(){
        $.ajax({
          type:'post',
          url:'<?php echo base_url(); ?>payment/cancelOwnerInvoice',
          data:{invoice_id},
          beforeSend: function(){
            $(".cs-overlay").css('visibility', 'visible');
          },
          success:function (res) {
           let search = $('#search').val();
           if (search == 1) {
             $.confirm({
              title: 'Success!',
              content: 'Invoice successfully deleted!',
              type: 'green',
              draggable: false,
              buttons: {
                tryAgain: {
                  text: 'Close',
                  action: function(){
                   $('#vehicle_owner_invoice_form').submit();
                 }
               },
             }
           }); 
           } else {
            location.reload();
          } 
        }
      });
      },
      cancel : {

      }
    }
  });
}


function getVehicleOwnerInvoiceById(invoice_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getVehicleOwnerInvoiceById',
    data:{invoice_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);

        let print_url = '<?php echo base_url(); ?>payment/printVehicleOwnerInvoice/'+result.data.id;
        $('#invoice_v2').html(result.data.invoice_no);
        $('#invoice_v').val(result.data.invoice_no);
        $('#invoice_id_v').val(result.data.id);
        $('#date_v').val(result.data.created_at);
        $('#booking_v').val(result.data.booking.booking_no);
        $('#agreement_v').val(result.data.agreement.agreement_no);
        $('#from_v').val(result.data.agreement.from_date);
        $('#to_v').val(result.data.agreement.to_date);
        $('#owner_v').val(result.data.agreement.vehicle.vehicle_owner.first_name+' '+result.data.agreement.vehicle.vehicle_owner.last_name);
        $('#vehicle_v').val(result.data.agreement.vehicle.registration_no);
        $('#account_name_v').val(result.data.agreement.vehicle.vehicle_owner.vehicle_owner_bank_details.account_name);
        $('#account_no_v').val(result.data.agreement.vehicle.vehicle_owner.vehicle_owner_bank_details.account_no);
        $('#bank_v').val(result.data.agreement.vehicle.vehicle_owner.vehicle_owner_bank_details.bank.bank_name);
        $('#branch_v').val(result.data.agreement.vehicle.vehicle_owner.vehicle_owner_bank_details.branch);
        $('#per_day_v').val(result.data.rate);
        $('#rate_days_v').html(result.data.booking_days);
        $('#total_rate_v').val(result.data.total_rate);
        $('#total_amount_v').html(result.data.grand_total);
        $('#refund_amount_v').html(result.data.refund_amount);
        $('#license_expiry_v').val(result.data.agreement.vehicle.revenue_license_exp_date);
        $('#smoke_expiry_v').val(result.data.agreement.vehicle.smoke_test_exp_date);
        $('#insurance_expiry_v').val(result.data.agreement.vehicle.insurance_exp_date);

        $('#print_span').empty();
        $('#print_span').append('<a href="'+print_url+'" class="btn btn-outline-secondary btn-rounded" target="_blank" id="print_v">Print</a>');


        $('#service_div').empty();

        $.each(result.data.vehicle_owner_invoice_extra_chargers, function (i, item) {
          if (item.is_remove != 1) {
            $('#service_div').append(' <div class="row mt-3" style="padding:0 25px;" id="row_'+item.id+'"> <div class="col-md-6 px-0">'+item.charge_name+'</div> <div class="col-md-2 offset-md-4 px-0" style="text-align: right;"> - '+item.charge_amount+'&nbsp;&nbsp;&nbsp;<a href="javascript:void" id="'+item.id+'" onclick="removeService(`'+item.id+'`)" style="color:red;"><i class="fa fa-trash"></i></a></div> </div>');
          }
        });

        $('#owner_invoice_modal').modal('show');
        $(".cs-overlay").css('visibility', 'hidden');
      }
    }
  });
}

function removeService(id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/removeService',
    data:{id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let result = JSON.parse(res);
     $('#row_'+id).fadeOut();
     $('#sub_total_v').html(result.data.booking.sub_total);
     $('#discount_v').html(result.data.booking.discount);
     $('#total_amount_v').html(result.data.grand_total);
     $(".cs-overlay").css('visibility', 'hidden');
   }
 });
}

$('#service_btn_v').click(function() {
  let service_name_v = $('#service_name_v').val();
  let service_fee_v = $('#service_fee_v').val();
  let invoice_id = $('#invoice_id_v').val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/addService',
    data:{service_name_v,service_fee_v,invoice_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let result = JSON.parse(res);

     $('#service_div').empty();

     $.each(result.data.vehicle_owner_invoice_extra_chargers, function (i, item) {
      if (item.is_remove != 1) {
        $('#service_div').append(' <div class="row mt-3" style="padding:0 25px;" id="row_'+item.id+'"> <div class="col-md-6 px-0">'+item.charge_name+'</div> <div class="col-md-2 offset-md-4 px-0" style="text-align:right;">'+item.charge_amount+'&nbsp;&nbsp;&nbsp;<a href="javascript:void" id="'+item.id+'" onclick="removeService(`'+item.id+'`)" style="color:red;"><i class="fa fa-trash"></i></a></div> </div>');
      }
    });

     $('#service_name_v').val('');
     $('#service_fee_v').val('');
     $('#sub_total_v').html(result.data.booking.sub_total);
     $('#discount_v').html(result.data.booking.discount);
     $('#total_amount_v').html(result.data.grand_total);
     $(".cs-overlay").css('visibility', 'hidden');
   }
 });
});


function refundInvoice(id, invoice_no, booking_no, name, max_refund_amount) {
  $('#invoice_id_i').val(id);
  $('#invoice_no_i').val(invoice_no);
  $('#invoiceno_i').html(invoice_no);
  $('#booking_no_i').val(booking_no);
  $('#owner_i').val(name);
  $('#refund_i').attr('min',0);
  $('#refund_i').attr('max',max_refund_amount);
  $('#modal_invoice').modal('show');
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>