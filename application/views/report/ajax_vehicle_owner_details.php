<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering"
      class="table table-striped table-bordered table-hover" style="width:100%">
      <thead>
        <tr>
          <th>Name</th>
          <th>NIC</th>
          <th>City</th>
          <th>Mobile</th>
          <th>Email</th>
          <th>Account No</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($data as $vehicle_owner) { ?>
          <tr>
            <td><?php echo $vehicle_owner->owner_name; ?></td>
            <td><?php echo $vehicle_owner->nic; ?></td>
            <td><?php echo $vehicle_owner->city; ?></td>
            <td><?php echo $vehicle_owner->mobile_1; ?></td>
            <td><?php echo $vehicle_owner->email; ?></td>
            <td><?php echo $vehicle_owner->account_no; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>


<script>
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Vehicle Owners',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Vehicle Owners',
    }
    ]
  });
</script>