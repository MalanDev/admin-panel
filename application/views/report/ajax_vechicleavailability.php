<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering"
      class="table table-striped table-bordered table-hover" style="width:100%">
      <thead>
        <tr>
          <th>Vehicle Type</th>
          <th>Reg No</th>    
          <th>Rev License</th>                  
          <th>Smoke Test</th>
          <th>Insurance</th>                     
          <th>Per Day Rate</th>
          <th>Owner</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($data as $vehicle) { ?>
          <tr>
            <td><?php echo $vehicle->type; ?></td>
            <td><?php echo $vehicle->registration_no; ?></td>
            <td><?php echo $vehicle->owner_name; ?></td>
            <td><?php echo $vehicle->revenue_license_exp_date; ?></td>
            <td><?php echo $vehicle->smoke_test_exp_date; ?></td>
            <td><?php echo $vehicle->insurance_exp_date; ?></td>
            <td><?php echo $vehicle->per_day_rate; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>

<script>
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Available Vehicles',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Available Vehicles',
    }
    ]
  });
</script>