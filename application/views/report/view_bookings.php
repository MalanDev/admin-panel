<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">

  <style type="text/css">
    .buttons-excel {
      background-color: #ff0208 !important;
      color: #fff !important;
    }

    .buttons-pdf {
      background-color: #092766 !important;
      color: #fff !important;
    }
  </style>
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">
    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>View Booking</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Reports</li>
                <li>View Booking</li>
              </ul>
            </div>
          </div>
        </div>


        <form id="viewBookingForm">
          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="vehicleType">Vehicle Type</label>
                      <select class="form-control-rounded form-control" name="vehicleType"
                      id="vehicletype">
                    </select>
                  </div>
                  <div class="form-group col-md-3">
                    <label for="exampleFormControlInput1">Booking No</label>
                    <select class="form-control-rounded form-control search_dropdown" id="bookingNo" name="bookingNo">
                      <option value="" disabled selected hidden>Choose Booking No</option>
                    </select>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">From</label>
                    <input type="date" class="form-control-rounded form-control"
                    id="fromDate" value="<?php echo date("Y-m-d"); ?>" onchange="changeDate()">
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">To</label>
                    <input type="date" class="form-control-rounded form-control"
                    id="toDate">
                  </div>
                  <div class="form-group col-md-3">
                    <label for="exampleFormControlInput1">Status</label>
                    <select class="form-control-rounded form-control search_dropdown" id="status" name="status">
                      <option value="" disabled selected hidden>Choose Status</option>
                    </select>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group col-md-3 d-flex align-items-center">
                    <label for="exampleFormControlInput1" style="margin-bottom: 0;">Booking Date</label>
                    <div class="vc-toggle-container ml-3">
                      <label class="vc-switch mt-2">
                        <input type="checkbox" class="vc-switch-input" id="booking_date"/>
                        <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                        <span class="vc-handle"></span>
                      </label>
                    </div>
                  </div>
                  <div class="form-group col-md-3 d-flex align-items-center">
                    <label for="exampleFormControlInput1" style="margin-bottom: 0;">Pickup Date</label>
                    <div class="vc-toggle-container ml-3">
                      <label class="vc-switch mt-2">
                        <input type="checkbox" class="vc-switch-input" checked id="pickup_date"/>
                        <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                        <span class="vc-handle"></span>
                      </label>
                    </div>
                  </div>
                  <div class="form-group col-md-3 d-flex align-items-center">
                    <label for="exampleFormControlInput1" style="margin-bottom: 0;">Return Date</label>
                    <div class="vc-toggle-container ml-3">
                      <label class="vc-switch mt-2">
                        <input type="checkbox" class="vc-switch-input" id="return_date"/>
                        <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                        <span class="vc-handle"></span>
                      </label>
                    </div>
                  </div>
                  <div class="form-group mt-1 col-md-3 text-right">
                    <input type="submit" name="time" class="btn btn-button-7 btn-rounded" value="Search">
                  </div>
                </div>

              </div>
            </div>
          </div>

        </div>
      </form>


      <!--table-->
      <div class="row" id="cancel-row">
        <div class="col-xl-12 col-lg-12 col-sm-12">
         <div class="statbox widget box box-shadow" id="table_div">
          <div class="widget-content widget-content-area">
            <div class="table-responsive mb-4">
             <table id="multi-column-ordering"
             class="table table-striped table-bordered table-hover" style="width:100%">
             <thead>
              <tr>
                <th>Booking No</th>
                <th>Full Name</th>
                <th>Booking Date</th>
                <th>Pickup Date</th>
                <th>Return Date</th>
                <th>Pickup Location</th>
                <th>Return Location</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

</div>
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });

  function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }

  //From and To Date selection
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();

  from_date = yyyy + '-' + mm + '-' + dd;

  var to_dd = String(today.getDate() + 1).padStart(2, '0');

  var days_for_month = daysInMonth(mm, yyyy);

  if (to_dd >= days_for_month) {

    var nextDay = new Date(today);
    nextDay.setDate(today.getDate() + 1);
    var hh = nextDay.getHours();
    var ii = nextDay.getMinutes();
    to_dd = String(nextDay.getDate()).padStart(2, '0');
    var mm = String(nextDay.getMonth() + 1).padStart(2, '0');
    var yyyy = nextDay.getFullYear();

  }

  to_date = yyyy + '-' + mm + '-' + to_dd;

  // $('#fromDate').attr('min', from_date);
  $('#toDate').val(to_date);
  $('#toDate').attr('min', to_date);

  function changeDate() {
    var from_today = new Date($('#fromDate').val());
    var dd = String(from_today.getDate() + 1).padStart(2, '0');
    var mm = String(from_today.getMonth() + 1).padStart(2, '0');
    var yyyy = from_today.getFullYear();

    if (dd >= days_for_month) {

      var nextDay = new Date(from_today);
      nextDay.setDate(from_today.getDate() + 1);
      var hh = nextDay.getHours();
      var ii = nextDay.getMinutes();
      var dd = String(nextDay.getDate()).padStart(2, '0');
      var mm = String(nextDay.getMonth() + 1).padStart(2, '0');
      var yyyy = nextDay.getFullYear();

    }

    from_today = yyyy + '-' + mm + '-' + dd;
    $('#toDate').attr('min', from_today);
    $('#toDate').val(from_today);
  }

  getAllVehicleTypes();

  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));

            $('#vehicletype').trigger('change');

            $('#vehicletype2').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));

            $('#vehicletype2').trigger('change');
          });
          $(".cs-overlay").css('visibility', 'hidden');

          getAllStatus();
        }        
      }
    });
  }

  function getAllStatus() {
   $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getAllStatus',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $.each(result.data, function (i, item) {
          $('#status').append($('<option>', { 
            value: item.id,
            text : item.status_name 
          }));
        });
      }
    }
  });
 }

 $('#vehicletype').on('change', function() {

  let vehicletype = $(this).val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getBookingNoByVehicleType',
    data:{vehicletype},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        
        $.each(result.data, function (i, item) {
          $('#bookingNo').append($('<option>', { 
            value: item.booking_no,
            text : item.booking_no 
          }));
        });
        $(".cs-overlay").css('visibility', 'hidden');

        $('#viewBookingForm').submit();
      }
    }
  });
});

 $('#viewBookingForm').submit(function(evt) {
  evt.preventDefault();

  if( $('#viewBookingForm').valid() ) {

    let vehicletype = $('#vehicletype').val();
    let bookingNo = $('#bookingNo').val();
    let status = $('#status').val();
    let fromDate = $('#fromDate').val();
    let toDate = $('#toDate').val();

    let booking_date = 0;
    if ($('#booking_date').is(':checked')) {
      booking_date = 1;
    }

    let pickup_date = 0;
    if ($('#pickup_date').is(':checked')) {
      pickup_date = 1;
    }

    let return_date = 0;
    if ($('#return_date').is(':checked')) {
      return_date = 1;
    }

    if (vehicletype == null) {
      vehicletype = -1;
    }

    if (bookingNo == null) {
      bookingNo = -1;
    }

    if (status == null) {
      status = -1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>report/searchBooking',
      data:{vehicletype,bookingNo,status,fromDate,toDate,booking_date,pickup_date,return_date},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $('#search').val(1);
        $('#table_div').html(res);
        $(".cs-overlay").css('visibility', 'hidden');
      }
    });
  } 
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>