<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">

  <style type="text/css">
    .buttons-excel {
      background-color: #ff0208 !important;
      color: #fff !important;
    }

    .buttons-pdf {
      background-color: #092766 !important;
      color: #fff !important;
    }
  </style>
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Vehicle Owner Details</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Reports</li>
                <li>Vehicle Owner Details</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="search_owner_form">
          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Owner Name</label>
                      <select class="form-control-rounded form-control search_dropdown" id="owner">
                        <option selected disabled>Select Owner</option>
                        <?php foreach($data as $vehicle_owner) { ?>
                          <option value="<?php echo $vehicle_owner->id; ?>"><?php echo $vehicle_owner->first_name.' '.$vehicle_owner->last_name; ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype">
                      </select>
                    </div>
                    <div class="form-group mt-4 col-md-4 offset-md-2 text-right">
                      <input type="submit" id="submit_btn" class="btn btn-button-7 btn-rounded mt-1" value="Search">
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>NIC</th>
                      <th>City</th>
                      <th>Mobile</th>
                      <th>Email</th>
                      <th>Account No</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Vehicle Owners',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Vehicle Owners',
    }
    ]
  });

  getVehicleOwners();

  function getVehicleOwners() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllOwnersForReport',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#owner').append($('<option>', { 
              value: item.id,
              text : item.first_name+' '+item.last_name 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');

          getAllVehicleTypes();
        }        
      }
    });
  }

  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');

          $('#submit_btn').trigger('click');
        }        
      }
    });
  }


  $('#search_owner_form').on('submit', function (evt) {
   evt.preventDefault();

   let owner = $('#owner').val();
   let vehicletype = $('#vehicletype').val();

   if (owner == null) {
    owner = -1;
  }

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>report/searchOwners',
    data:{vehicletype,owner},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $("#table_div").html(res);
      $(".cs-overlay").css('visibility', 'hidden');
    }
  });

});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>