<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">

  <style type="text/css">
    .buttons-excel {
      background-color: #ff0208 !important;
      color: #fff !important;
    }

    .buttons-pdf {
      background-color: #092766 !important;
      color: #fff !important;
    }
  </style>

</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>View Agreements</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Reports</li>
                <li>View Agreements</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="search_agreement_form">

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Booking No</label>
                      <select class="form-control-rounded form-control search_dropdown" name="bookingNo" id="bookingNo">
                        <option value="" disabled selected hidden>Select Booking No</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Agreement No</label>
                      <select class="form-control-rounded form-control search_dropdown" name="agreement_No" id="agreement_No">
                        <option value="" disabled selected hidden>Select Agreement No</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate" name="fromDate"
                      value="<?php echo date('Y-m-d'); ?>" onchange="changeDate()" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate" name="toDate" required>
                    </div>
                    <div class="form-group col-md-3 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">Pickup Date</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" checked id="pickup_date" />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group col-md-3 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">Return Date</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" id="return_date" />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group mt-4 col-md-3 offset-md-3 text-right">
                      <input type="submit" name="time" id="search" class="btn btn-button-7 btn-rounded"
                      value="Search">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </form>

        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Agreement No</th>
                        <th>Booking No</th>
                        <th>Full Name</th>
                        <th>Pickup Date</th>
                        <th>Return Date</th>
                        <th>Vehicle No</th>
                        <th>Pickup Location</th>
                        <th>Return Location</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

</div>
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->


<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Agreement - 10002</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <form> 

        <div class="row layout-spacing p-2">

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="row">
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Booking No</label>
                <select class="form-control-rounded form-control" id="exampleFormControlSelect1" readonly>
                  <option>Select Booking No</option>
                  <option>B10001</option>
                  <option>B10002</option>
                  <option>B10003</option>
                </select>
              </div>
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Agreement No</label>
                <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
              </div>
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Date</label>
                <input type="date" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
              </div>
            </div>
          </div>  



          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Personal Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">First Name</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Last Name</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Date of Birth</label>
                  <input type="date" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Age</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Nationality</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Passpport No</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Visa Expiry Date</label>
                  <input type="date" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Country</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Mobile No 1</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Mobile No 2 (optional)</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Email</label>
                  <input type="email" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Address in Sri Lanka</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Permanent Address</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
              </div>
            </div>
          </div>




          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Flight Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Flight Number</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Location</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Date</label>
                  <input type="date" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Time</label>
                  <input type="time" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
              </div>
            </div>
          </div>



          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Rental Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Vehicle Type</label>
                  <select class="form-control-rounded form-control" id="exampleFormControlSelect1" readonly>
                    <option>Tuk</option>
                    <option>Car</option>
                    <option>Van</option>
                  </select>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Vehicle Reg No</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">License No</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">From</label>
                  <input type="date" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">To</label>
                  <input type="date" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Milage Starts</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Milage Ends</label>
                  <input type="text" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly>
                </div>
              </div>
            </div>
          </div>

        </div>

      </form>
      <div class="modal-footer">
        <input type="submit" name="time" class="btn btn-button-7 btn-rounded mt-2 px-4" value="Print">
        <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-2" data-dismiss="modal" value="Close">
      </div>
    </div>
  </div>
</div>
</div>


<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>

<script>
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Agreements',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Agreements',
    }
    ]
  });

   //From and To Date selection
  var today = new Date();
  var hh = today.getHours();
  var ii = today.getMinutes();
  var d = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();

  var from_date = yyyy + '-' + mm + '-' + d;
  // $('#end_date').attr('min', from_date);

  var dd = String(today.getDate() + 1).padStart(2, '0');
  to_date = yyyy + '-' + mm + '-' + dd;
  $('#toDate').attr('min', to_date);
  $('#toDate').val(to_date);

  function changeDate() {
    var from_today = new Date($('#fromDate').val());
    var dd = String(from_today.getDate() + 1).padStart(2, '0');
    var mm = String(from_today.getMonth() + 1).padStart(2, '0');
    var yyyy = from_today.getFullYear();

    from_today = yyyy + '-' + mm + '-' + dd;
    $('#toDate').val(from_today);
    $('#toDate').attr('min', from_today);
  }

  getBookingNumbers();

  function getBookingNumbers() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getBookingNumbers',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {

        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          if (result.data.length > 0) {
            $.each(result.data, function (i, item) {
              $('#bookingNo').append($('<option>', { 
                value: item.id,
                text : item.booking_no 
              }));
              $('#booking_no_edit').append($('<option>', { 
                value: item.id,
                text : item.booking_no 
              }));              
            });
          }
          $(".cs-overlay").css('visibility', 'hidden');
          getAllVehicleTypes();
        }        
      }
    });
  }


  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype_edit').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));
          });

          $('#search_agreement_form').submit();

          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  $('#bookingNo').on('change', function (argument) {
    let booking_id = $(this).val();
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementNoByBookingId/'+booking_id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#agreement_No').empty();
          $('#agreement_No').append('<option value="" selected>Select Agreement No</option>');
          if (result.data.length > 0) {
            $.each(result.data, function (i, item) {
              $('#agreement_No').append($('<option>', { 
                value: item.id,
                text : item.agreement_no 
              }));
            });
          }
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  });

  $('#search_agreement_form').submit(function(evt) {
    evt.preventDefault();

    let bookingNo = $('#bookingNo').val();
    let agreement_No = $('#agreement_No').val();
    let fromDate = $('#fromDate').val();
    let toDate = $('#toDate').val();

    let pickup_date = 0;
    if ($('#pickup_date').is(':checked')) {
      pickup_date = 1;
    }

    let return_date = 0;
    if ($('#return_date').is(':checked')) {
      return_date = 1;
    }

    if (bookingNo == null || bookingNo == "") {
      bookingNo = -1;
    }

    if (agreement_No == null || agreement_No == "") {
      agreement_No = -1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>report/searchAgreements',
      data:{bookingNo,agreement_No,fromDate,toDate,pickup_date,return_date},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $('#table_div').html(res);
        $(".cs-overlay").css('visibility', 'hidden');
      }
    });
  });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>