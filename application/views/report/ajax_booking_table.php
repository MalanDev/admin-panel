  <div class="statbox widget box box-shadow" id="table_div">
    <div class="widget-content widget-content-area">
      <div class="table-responsive mb-4">
       <table id="multi-column-ordering"
       class="table table-striped table-bordered table-hover" style="width:100%">
       <thead>
        <tr>
          <th>Booking No</th>
          <th>Full Name</th>
          <th>Booking Date</th>
          <th>Pickup Date</th>
          <th>Return Date</th>
          <th>Pickup Location</th>
          <th>Return Location</th>
          <th>Status</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $booking) { ?>
          <tr>
            <td><?php echo $booking->booking_no; ?></td>
            <td><?php echo $booking->full_name; ?></td>
            <td><?php echo $booking->booking_date; ?></td>
            <td><?php echo $booking->from_date; ?></td>
            <td><?php echo $booking->to_date; ?></td>
            <td><?php echo $booking->pickup_location; ?></td>
            <td><?php echo $booking->return_location; ?></td>
            <td><?php echo $booking->status_name; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>


<script>
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Bookings',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Bookings',
    }
    ]
  });
</script>