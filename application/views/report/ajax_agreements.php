<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
          <tr>
            <th>Agreement No</th>
            <th>Booking No</th>
            <th>Full Name</th>
            <th>Pickup Date</th>
            <th>Return Date</th>
            <th>Vehicle No</th>
            <th>Pickup Location</th>
            <th>Return Location</th>
          </tr>
        </thead>
        <tbody>
         <?php foreach($data as $agreement) { ?>
          <tr>
            <td><?php echo $agreement->agreement_no; ?></td>
            <td><?php echo $agreement->booking_no; ?></td>
            <td><?php echo $agreement->first_name." ".$agreement->last_name; ?></td>
            <td><?php echo $agreement->from_date; ?></td>
            <td><?php echo $agreement->to_date; ?></td>
            <td><?php echo $agreement->registration_no; ?></td>
            <td><?php echo $agreement->pickup_location; ?></td>
            <td><?php echo $agreement->return_location; ?></td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>

<script type="text/javascript">
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Agreements',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Agreements',
    }
    ]
  });
</script>