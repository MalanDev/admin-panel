<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
          <tr>
            <th>Invoice No</th>
            <th>Booking No</th>
            <th>Customer</th>
            <th>Pickup Date</th>
            <th>Return Date</th>
            <th>Pickup Location</th>
            <th>Return Location</th>
            <th>Payment Status</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($data as $invoice) { ?>
            <tr>
              <td><?php echo $invoice->invoice_no; ?></td>
              <td><?php echo $invoice->booking_no; ?></td>
              <td><?php echo $invoice->full_name; ?></td>
              <td><?php echo $invoice->from_date; ?></td>
              <td><?php echo $invoice->to_date; ?></td>
              <td><?php echo $invoice->pickup_location; ?></td>
              <td><?php echo $invoice->return_location; ?></td>
              <td class="text-center">
                <?php if ($invoice->is_paid == 1) { ?>
                  <span class="badge badge-pill" style="width: 80px;background-color: #b6fff1;color: #1abc9c;">Paid</span>
                <?php } else { ?>
                  <span class="badge badge-pill" style="width: 80px;background-color: #dccff7;color: #805dca;">Pending</span>
                <?php } ?>
              </td>             
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>

<script type="text/javascript">
 $('#multi-column-ordering').DataTable({
  dom: 'Bfrtip',
  buttons: [
  {
    extend: 'excel',
    text: 'Excel',
    title: 'Customer Invoice',
  },
  {
    extend: 'pdf',
    text: 'Pdf',
    title: 'Customer Invoice',
  }
  ]
});
</script>