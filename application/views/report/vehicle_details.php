<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">
    <div class="overlay"></div>
    <div class="cs-overlay"></div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Vehicle Details</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Reports</li>
                <li>Vehicle Details</li>
              </ul>
            </div>
          </div>
        </div>

        <form>
          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group col-md-4">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control search_dropdown">
                        <option>Select Vehicle Type</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="exampleFormControlInput1">Register No</label>
                      <select class="form-control-rounded form-control search_dropdown">
                        <option>Select Register No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-4">
                      <label for="exampleFormControlInput1">Owner Name</label>
                      <select class="form-control-rounded form-control search_dropdown">
                        <option>Select Owner Name</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-4">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control"
                      id="exampleFormControlInput1">
                    </div>
                    <div class="form-group mb-4 col-md-4">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control"
                      id="exampleFormControlInput1">
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group col-md-3 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">License</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" checked />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group col-md-3 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">Smoke Test</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" checked />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group col-md-2 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">Insurance</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" checked />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group mt-1 col-md-4 text-right">
                      <input type="submit" name="time" class="btn btn-button-7 btn-rounded mt-1" value="Search">
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Vehicle Type</th>
                      <th>Reg No</th>
                      <th>Per Day Rate</th>
                      <th>Pickup Date</th>
                      <th>Smoke Test</th>
                      <th>Insurance</th>
                      <th>License</th>
                      <th>Owner</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>Tuk Tuk</td>
                      <td>ABC123</td>
                      <td>100.00</td>
                      <td>2022/03/25</td>
                      <td>2022/03/25</td>
                      <td>2022/03/25</td>
                      <td>2022/03/25</td>
                      <td>ABC</td>
                    </tr>
                    <tr>
                      <td>Tuk Tuk</td>
                      <td>DFG123</td>
                      <td>120.00</td>
                      <td>2022/02/22</td>
                      <td>2022/02/22</td>
                      <td>2022/02/22</td>
                      <td>2022/02/22</td>
                      <td>FGS</td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>