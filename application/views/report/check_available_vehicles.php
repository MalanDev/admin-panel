<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">

  <style type="text/css">
    .buttons-excel {
      background-color: #ff0208 !important;
      color: #fff !important;
    }

    .buttons-pdf {
      background-color: #092766 !important;
      color: #fff !important;
    }
  </style>
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">
    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Check Available Vehicles</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Reports</li>
                <li>Check Available Vehicles</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="check_availability_form">
          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicletype">
                        <option value="" disabled selected hidden>Choose Vehicle Type</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate" value="<?php echo date("Y-m-d"); ?>" onchange="changeDate()">
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate">
                    </div>
                    <div class="form-group mt-4 col-md-3 text-right">
                      <input type="submit" name="time" class="btn btn-button-7 btn-rounded mt-1" value="Search">
                    </div>
                  </div>

                </div>
              </div>
            </div>

          </div>
        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Vehicle Type</th>
                      <th>Reg No</th>    
                      <th>Rev License</th>                  
                      <th>Smoke Test</th>
                      <th>Insurance</th>                     
                      <th>Per Day Rate</th>
                      <th>Owner</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    dom: 'Bfrtip',
    buttons: [
    {
      extend: 'excel',
      text: 'Excel',
      title: 'Available Vehicles',
    },
    {
      extend: 'pdf',
      text: 'Pdf',
      title: 'Available Vehicles',
    }
    ]
  });


  function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }


  //From and To Date selection
  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();

  from_date = yyyy + '-' + mm + '-' + dd;
  var dd = String(today.getDate() + 1).padStart(2, '0');

  var days_for_month = daysInMonth(mm, yyyy);

  if (dd >= days_for_month) {

    var nextDay = new Date(today);
    nextDay.setDate(today.getDate() + 1);
    var dd = String(nextDay.getDate()).padStart(2, '0');
    var mm = String(nextDay.getMonth() + 1).padStart(2, '0');
    var yyyy = nextDay.getFullYear();

  }

  to_date = yyyy + '-' + mm + '-' + dd;
// $('#fromDate').attr('min', from_date);
  $('#toDate').attr('min', to_date);
  $('#toDate').val(to_date);


  getAllVehicleTypes();

  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));
          });
          $("#vehicletype").val(1);
          $(".cs-overlay").css('visibility', 'hidden');

          $('#check_availability_form').submit();
        }     
      }
    });
  }

  function changeDate() {
    var from_today = new Date($('#fromDate').val());
    var dd = String(from_today.getDate() + 1).padStart(2, '0');
    var mm = String(from_today.getMonth() + 1).padStart(2, '0');
    var yyyy = from_today.getFullYear();

    if (dd >= days_for_month) {

      var nextDay = new Date(from_today);
      nextDay.setDate(from_today.getDate() + 1);
      var dd = String(nextDay.getDate()).padStart(2, '0');
      var mm = String(nextDay.getMonth() + 1).padStart(2, '0');
      var yyyy = nextDay.getFullYear();

    }

    from_today = yyyy + '-' + mm + '-' + dd;
    $('#toDate').attr('min', from_today);
    $('#toDate').val(from_today);
  }

  $('#check_availability_form').on('submit', function (evt) {
   evt.preventDefault();

   let vehicletype = $('#vehicletype').val();
   let fromDate = $('#fromDate').val();
   let toDate = $('#toDate').val();

   $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>report/searchAvailableVehicles',
    data:{vehicletype,fromDate,toDate},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $("#table_div").html(res);
      $(".cs-overlay").css('visibility', 'hidden');
    }
  });

 });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>