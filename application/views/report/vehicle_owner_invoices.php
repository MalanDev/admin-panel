<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.datatables.net/buttons/2.2.3/css/buttons.dataTables.min.css">

  <style type="text/css">
    .buttons-excel {
      background-color: #ff0208 !important;
      color: #fff !important;
    }

    .buttons-pdf {
      background-color: #092766 !important;
      color: #fff !important;
    }
  </style>
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Vehicle Owner Invoice</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Reports</li>
                <li>Vehicle Owner Invoice</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="vehicle_owner_invoice_form">
          <div class="row layout-spacing">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Invoice No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="invoiceNo" name="invoiceNo">
                        <option value="" disabled selected hidden>Select Invoice No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Status</label>
                      <select class="form-control-rounded form-control search_dropdown" id="status" name="status">
                        <option value="" disabled selected hidden>Select Status</option>
                        <option value="1">Paid</option>
                        <option value="0">Not Paid</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Booking No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="bookingNo" name="bookingNo">
                        <option value="" disabled selected hidden>Select Booking No</option>
                      </select>
                    </div>
                    <div class="form-group col-md-3">
                      <label for="exampleFormControlInput1">Agreement No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="agreementno" name="agreementno">
                        <option value="" disabled selected hidden>Select Agreement No</option>
                      </select>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate" name="fromDate">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate" name="toDate">
                    </div>

                    <div class="form-group mt-4 pt-2 col-md-3 offset-md-3 text-right">
                      <input type="submit" class="btn btn-button-7 btn-rounded mt-2 search_btn" value="Search">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Invoice No</th>
                      <th>Booking No</th>
                      <th>Owner</th>
                      <th>Agreement No</th>
                      <th>Pickup Date</th>
                      <th>Return Date</th>
                      <th>Vehicle No</th>
                      <th>Milage Start</th>
                      <th>Milage End</th>
                      <th>Payment Status</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->


<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/dataTables.buttons.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/2.2.3/js/buttons.print.min.js"></script>
<script>
 $('#multi-column-ordering').DataTable({
  dom: 'Bfrtip',
  buttons: [
  {
    extend: 'excel',
    text: 'Excel',
    title: 'Vehicle Owner Invoice',
  },
  {
    extend: 'pdf',
    text: 'Pdf',
    title: 'Vehicle Owner Invoice',
  }
  ]
});

 getInvoiceNumbers();

 function getInvoiceNumbers() {
   $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getVehicleOwnerInvoiceNumbers',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#invoiceNo').append($('<option>', { 
              value: item.id,
              text : item.invoice_no 
            }));             
          });
        }
        $(".cs-overlay").css('visibility', 'hidden');
      }  

      $('#vehicle_owner_invoice_form').submit();
    }
  });
 }

 $('#invoiceNo').on('change', function () {
  let invoiceId = $(this).val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getBookingsByInvoiceId',
    data:{invoiceId},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $('#bookingNo').empty();
        $('#bookingNo').append('<option value="" selected>Select Booking No</option>');
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#bookingNo').append($('<option>', { 
              value: item.id,
              text : item.booking_no 
            }));             
          });

          $('#bookingNo').trigger('change');
        }       

        $(".cs-overlay").css('visibility', 'hidden');
      }        
    }
  });
  
});


 $('#bookingNo').on('change', function () {
  let bookingId = $(this).val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>payment/getAgreementsByBookingId',
    data:{bookingId},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $('#agreementno').empty();
        $('#agreementno').append('<option value="" selected>Select Agreement No</option>');
        if (result.data.length > 0) {
          $.each(result.data, function (i, item) {
            $('#agreementno').append($('<option>', { 
              value: item.id,
              text : item.agreement_no 
            }));             
          });
        }       

        $(".cs-overlay").css('visibility', 'hidden');
      }        
    }
  });
  
});


 $('#vehicle_owner_invoice_form').submit(function(evt) {
  evt.preventDefault();

  let invoiceNo = $('#invoiceNo').val();
  let bookingNo = $('#bookingNo').val();
  let agreementno = $('#agreementno').val();
  let status = $('#status').val();
  let fromDate = $('#fromDate').val();
  let toDate = $('#toDate').val();

  if (invoiceNo == null || invoiceNo == "") {
    invoiceNo = -1;
  }

  if (bookingNo == null || bookingNo == "") {
    bookingNo = -1;
  }

  if (agreementno == null || agreementno == "") {
    agreementno = -1;
  }

  if (fromDate == "") {
    fromDate = -1;
  }

  if (toDate == "") {
    toDate = -1;
  }

  if (status == null) {
    status = -1;
  }

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>report/VehicleOwnerInvoiceSearch',
    data:{invoiceNo,bookingNo,agreementno,status,fromDate,toDate},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $('#table_div').html(res);
      $(".cs-overlay").css('visibility', 'hidden');
    }
  });

});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>