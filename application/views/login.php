<!DOCTYPE html>
<html lang="en">
<head>

  <?php require 'includes/top_header.php' ?>
  <link href="<?php echo base_url(); ?>assets/assets/css/users/login-1.css" rel="stylesheet" type="text/css" /> 

</head>
<body class="login">

  <form class="form-login" action="<?php echo base_url(); ?>login/login" method="post">
    <div class="row">
      <div class="col-md-12 text-center mb-4">
        <img alt="logo" src="<?php echo base_url(); ?>assets/assets/img/logo-3.png" class="theme-logo">
      </div>

      <div class="col-md-12">
        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } ?>
        <label for="inputEmail" class="">Email</label>                
        <input type="email" id="inputEmail" class="form-control mb-4" placeholder="Enter your email" name="username" required >                
        <label for="inputPassword" class="">Password</label>                
        <input type="password" id="inputPassword" class="form-control mb-5" placeholder="Enter your password" name="password" required>                                
        <button class="btn btn-lg btn-gradient-danger btn-block btn-rounded mb-4 mt-5" type="submit">Login</button>
      </div>
    </div>
  </form>

  <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
  <script src="<?php echo base_url(); ?>assets/assets/js/libs/jquery-3.1.1.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bootstrap/js/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>

  <script type="text/javascript">
    setTimeout(function() {
      $('.alert-danger').slideUp();
    }, 3000);
  </script>

  <!-- END GLOBAL MANDATORY SCRIPTS -->
</body>
</html>