<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering"
      class="table table-striped table-bordered table-hover" style="width:100%">
      <thead>
        <tr>
          <th>Vehicle Type</th>
          <th>Reg No</th>
          <th>Owner</th>
          <th>Rev License</th>
          <th>Smoke Test</th>
          <th>Insurance for Tuk Tuk</th>
          <th>Per Day Rate</th>
          <th style="text-align:center;">Action</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach($data as $vehicle) { ?>
          <tr>
            <td><?php echo $vehicle->type; ?></td>
            <td><?php echo $vehicle->registration_no; ?></td>
            <td><?php echo $vehicle->owner_name; ?></td>
            <td><?php echo $vehicle->revenue_license_exp_date; ?></td>
            <td><?php echo $vehicle->smoke_test_exp_date; ?></td>
            <td><?php echo $vehicle->insurance_exp_date; ?></td>
            <td><?php echo $vehicle->per_day_rate; ?></td>
            <td class="text-center">
              <button class="btn btn-outline-info btn-rounded" onclick="showDetails('<?php echo $vehicle->vehicle_id; ?>');">View</button></td>
            </tr>
          <?php } ?>
        </tbody>
      </table>
    </div>
  </div>
</div>



<script type="text/javascript">
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });
</script>