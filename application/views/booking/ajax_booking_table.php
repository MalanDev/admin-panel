  <div class="statbox widget box box-shadow" id="table_div">
    <div class="widget-content widget-content-area">
      <div class="table-responsive mb-4">
       <table id="multi-column-ordering"
       class="table table-striped table-bordered table-hover" style="width:100%">
       <thead>
        <tr>
          <th>Booking No</th>
          <th>Full Name</th>
          <th>Booking Date</th>
          <th>Pickup Date</th>
          <th>Return Date</th>
          <th>Pickup Location</th>
          <th>Return Location</th>
          <th>Status</th>
          <th>Status Action</th>
          <th style="text-align:center;">Actions</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data as $booking) { ?>
          <tr>
            <td><?php echo $booking->booking_no; ?></td>
            <td><?php echo $booking->full_name; ?></td>
            <td><?php echo $booking->booking_date; ?></td>
            <td><?php echo $booking->from_date; ?></td>
            <td><?php echo $booking->to_date; ?></td>
            <td><?php echo $booking->pickup_location; ?></td>
            <td><?php echo $booking->return_location; ?></td>
            <td><?php echo $booking->status_name; ?></td>

            <td>

              <?php if($booking->status_name == 'Pending') { ?>

                <button class="btn btn-outline-success btn-rounded status_btn" alt="Confirm" id="<?php echo $booking->id; ?>">Confirm</button>
                <button class="btn btn-outline-danger btn-rounded status_btn" alt="Reject" id="<?php echo $booking->id; ?>">Reject</button>

              <?php } else if ($booking->status_name == 'Confirm') { ?>

                <button class="btn btn-outline-success btn-rounded status_btn" alt="Start" id="<?php echo $booking->id; ?>">Start</button>

              <?php } else if ($booking->status_name == 'Trip Start') { ?>

                <button class="btn btn-outline-danger btn-rounded status_btn" alt="End" id="<?php echo $booking->id; ?>">End</button>

              <?php } ?>

            </td>

            <td class="text-left">

             <?php if($booking->status_name == 'Pending') { ?>

              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingById('<?php echo $booking->id; ?>');">Edit</button>
              <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

            <?php } else if ($booking->status_name == 'Reject') { ?>

              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingById('<?php echo $booking->id; ?>');">Edit</button>
              <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

            <?php } else if ($booking->status_name == 'Confirm') { ?>

              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingById('<?php echo $booking->id; ?>');">Edit</button>
              <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

            <?php } else if ($booking->status_name == 'Trip Start') { ?>

              <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
              <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

            <?php } else if ($booking->status_name == 'Trip End') { ?>

             <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
             <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

           <?php } ?>
           
         </td>

       </tr>
     <?php } ?>
   </tbody>
 </table>
</div>
</div>
</div>


<script type="text/javascript">
 $('.status_btn').on('click',function () {
  let id = $(this).attr('id');
  let status = $(this).attr('alt');

  if (status == "Confirm") {
    confirmBooking(id);
  } else if (status == "Reject") {
    rejectBooking(id);
  }  else if (status == "Start") {
    startBooking(id);
  }  else if (status == "End") {
    endBooking(id);
  }

});
</script>


<script type="text/javascript">
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });
</script>