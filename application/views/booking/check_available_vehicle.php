<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />

</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Check Available Vehicle</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Booking Data</li>
                <li>Check Available Vehicle</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form id="check_availability_form">

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicletype">
                        <option value="" disabled selected hidden>Choose Vehicle Type</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control"
                      id="fromDate" value="<?php echo date("Y-m-d"); ?>" onchange="changeDate()">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control"
                      id="toDate">
                    </div>

                    <div class="form-group mt-2 pt-2 col-md-3 text-right">
                      <input type="submit" name="time" class="btn btn-button-7 btn-rounded mt-3" value="Search">
                      <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-3" value="Reset">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </form>

        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Vehicle Type</th>
                      <th>Reg No</th>
                      <th>Owner</th>
                      <th>Rev License</th>
                      <th>Smoke Test</th>
                      <th>Insurance for Tuk Tuk</th>
                      <th>Per Day Rate</th>
                      <th>Status</th>
                      <th style="text-align:center;">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>

</div>
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->


<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="vehicle_label"></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <form> 

        <div class="row p-2">

          <div class="col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Vehicle Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Vehicle Type</label>
                    <input type="text" class="form-control-rounded form-control" id="vehicletype2" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Reg No </label>
                    <input type="text" class="form-control-rounded form-control" id="regNo" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Owner</label>
                    <input type="text" class="form-control-rounded form-control" id="owner" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Contact No</label>
                    <input type="text" class="form-control-rounded form-control" id="contact" readonly>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                  <img src="https://via.placeholder.com/250" id="vehicle_img" style="width:50%;">
                </div>
              </div>

              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Revenue License Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">License No</label>
                    <input type="text" class="form-control-rounded form-control" id="licenseNo" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Expiry Date</label>
                    <input type="text" class="form-control-rounded form-control" id="license_expiry" readonly>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                  <img src="https://via.placeholder.com/250x150" id="license_img" style="width:50%;">
                </div>
              </div>

              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Smoke Test Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Test No</label>
                    <input type="text" class="form-control-rounded form-control" id="smokeNo" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Expiry Date</label>
                    <input type="text" class="form-control-rounded form-control" id="smoke_expiry" readonly>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                  <img src="https://via.placeholder.com/250x150" id="smoke_img" style="width:50%;">
                </div>
              </div>

              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Insurance for Tuk Tuk Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Insurance for Tuk Tuk No</label>
                    <input type="text" class="form-control-rounded form-control" id="insuranceNo" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Expiry Date</label>
                    <input type="text" class="form-control-rounded form-control" id="insurance_expiry" readonly>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 text-center">
                  <img src="https://via.placeholder.com/250x150" id="insurance_img" style="width:50%;">
                </div>
              </div>


              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Rate Details </h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">
                  <div class="form-group mb-4 col-md-12">
                    <label for="exampleFormControlInput1">Per Day Rate</label>
                    <input type="text" class="form-control-rounded form-control" id="per_day" readonly>
                  </div>
                </div>
              </div>


            </div>
          </div>
        </div>  

      </form>
    </div>

    <div class="modal-footer">
      <input type="reset" name="time" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
    </div>
  </div>
</div>
</div>

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script>
  $(document).ready(function() {
    getAllVehicleTypes();
  });

  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });


//From and To Date selection
var today = new Date();
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0');
var yyyy = today.getFullYear();

from_date = yyyy + '-' + mm + '-' + dd;
var dd = String(today.getDate() + 1).padStart(2, '0');
to_date = yyyy + '-' + mm + '-' + dd;
// $('#fromDate').attr('min', from_date);
// $('#toDate').attr('min', to_date);
$('#toDate').val(to_date);

function changeDate() {
  var from_today = new Date($('#fromDate').val());
  var dd = String(from_today.getDate() + 1).padStart(2, '0');
  var mm = String(from_today.getMonth() + 1).padStart(2, '0');
  var yyyy = from_today.getFullYear();

  from_today = yyyy + '-' + mm + '-' + dd;
  $('#toDate').attr('min', from_today);
  $('#toDate').val(from_today);
}


function getAllVehicleTypes() {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $.each(result.data, function (i, item) {
          $('#vehicletype').append($('<option>', { 
            value: item.id,
            text : item.type 
          }));
        });
        $("#vehicletype").val(1);
        searchAvailableVehicles();
      }     
    }
  });
}


function searchAvailableVehicles() {
  let vehicletype = $('#vehicletype').val();
  let fromDate = $('#fromDate').val();
  let toDate = $('#toDate').val();

  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/checkVehicleAvailability',
    data:{vehicletype,fromDate,toDate},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $('#table_div').html(res);
      $(".cs-overlay").css('visibility', 'hidden');
    }
  });
}


function showDetails(id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>master/getVehicleRegistrationById/'+id,
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);

        $('#vehicle_label').html(result.data.vehicle_type.type+' - '+result.data.registration_no);
        $('#vehicletype2').val(result.data.vehicle_type.type);
        $('#regNo').val(result.data.registration_no);
        $("#vehicle_img").attr("src",result.data.vehicle_image);
        $('#owner').val(result.data.vehicle_owner.first_name+'  '+result.data.vehicle_owner.last_name);
        $('#contact').val(result.data.vehicle_owner.mobile_1);
        $('#licenseNo').val(result.data.revenue_license_no);
        $('#license_expiry').val(result.data.revenue_license_exp_date);
        $('#license_img').attr("src",result.data.revenue_license_image);
        $('#smokeNo').val(result.data.smoke_test_no);
        $('#smoke_expiry').val(result.data.smoke_test_exp_date);
        $('#smoke_img').attr("src",result.data.smoke_test_image);
        $('#insuranceNo').val(result.data.insurance_no);
        $('#insurance_expiry').val(result.data.insurance_exp_date);
        $("#insurance_img").attr("src",result.data.insurance_image);
        $('#per_day').val(result.data.per_day_rate);        

        $('.modal').modal('show');
        $(".cs-overlay").css('visibility', 'hidden');
      }
    }
  });
}


$('#check_availability_form').submit(function(evt) {
  evt.preventDefault();
  searchAvailableVehicles();
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>