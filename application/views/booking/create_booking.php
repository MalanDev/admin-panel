<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />

  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/intlTelInput.css">

  <style type="text/css">
    #pickupLocation-error, #returnLocation-error{
      position: absolute;
      bottom: 0px;
      left: 20px;
    }

    .alert-messages2 {
      display: none;
    }

    .iti__selected-flag {
      border-radius: 20px 0 0 20px;
      height: auto;
      padding-top: 8px;
    }

    .iti--separate-dial-code .iti__selected-flag {
      background-color: transparent;
    }

    .iti--allow-dropdown {
      width: 100%;
    }
  </style>

</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Create Booking</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Booking Data</li>
                <li>Create Booking</li>
              </ul>
            </div>
          </div>
        </div>
        <!--page header-->

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <div class="alert alert-danger alert-messages2"></div>

        <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
          <div class="statbox widget box box-shadow">
            <div class="widget-content widget-content-area">
              <div class="row">
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <input type="text" disabled class="form-control-rounded form-control" id="bookingNo">
                </div>
                <div class="form-group mb-4 col-md-4 offset-md-4">
                  <label for="exampleFormControlInput1">Date</label>
                  <input type="datetime" class="form-control-rounded form-control" id="exampleFormControlInput1" readonly value="<?php echo date('Y-m-d h:i a')?>">
                </div>
              </div>
            </div>
          </div>
        </div>



        <div class="col-md-12 col-lg-12 col-12 layout-spacing">

          <div class="statbox widget box box-shadow">
            <div class="widget-content widget-content-area rounded-pills layout-spacing">
              <ul class="nav nav-pills nav-fill" id="rounded-pills-tab" role="tablist">
                <li class="nav-item">
                  <a class="nav-link  btn-rounded active" id="rounded-pills-home-tab"
                  href="#rounded-pills-home" role="tab" aria-controls="rounded-pills-home" aria-selected="true"><span
                  style="background-color: #fff;padding: 4px 8px;color: #222;border-radius: 50%;margin-right: 5px;border: 1px solid #222;">1</span>Check Availability</a>
                </li>
                <li class="nav-item ml-1">
                  <a class="nav-link  btn-rounded" id="rounded-pills-profile-tab"
                  href="#rounded-pills-profile" role="tab" aria-controls="rounded-pills-profile"
                  aria-selected="false" disabled><span
                  style="background-color: #fff;padding: 4px 8px;color: #222;border-radius: 50%;margin-right: 5px;border: 1px solid #222;">2</span>Reservation Details</a>
                </li>
                <li class="nav-item ml-1">
                  <a class="nav-link  btn-rounded" id="ronded-pills-contact-tab"
                  href="#rounded-pills-contact" role="tab" aria-controls="rounded-pills-contact"
                  aria-selected="false"><span
                  style="background-color: #fff;padding: 4px 8px;color: #222;border-radius: 50%;margin-right: 5px;border: 1px solid #222;">3</span>Rental Details</a>
                </li>
              </ul>
            </div>

            <div class="widget-content widget-content-area rounded-pills">
              <div class="tab-content" id="rounded-pills-tabContent">


                <!-- CHECK AVAILABILITY -->
                <div class="tab-pane fade show active" id="rounded-pills-home" role="tabpanel"
                aria-labelledby="rounded-pills-home-tab">
                <form id="check_availability_form">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="vehicleType">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicletype" onchange="getPickupLocationsByVehicleType()">
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="noOfVehicle">No of Vehicles</label>
                      <input type="number" class="form-control-rounded form-control" id="noOfVehicle" name="noOfVehicle"
                      placeholder=" Enter No of Vehicles" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="pickupLocation">Pickup Location</label>
                      <select class="form-control-rounded form-control search_dropdown" name="pickupLocation" id="pickupLocation">
                        <option value="" disabled selected hidden>Enter Pickup Location</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="returnLocation">Return Location</label>
                      <select class="form-control-rounded form-control search_dropdown" name="returnLocation" id="returnLocation">
                        <option value="" disabled selected hidden>Enter Return Location</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="fromDate">From</label>
                      <input type="datetime-local" class="form-control-rounded form-control" id="fromDate" name="fromDate"
                      value="<?php echo date('Y-m-d\TH:i'); ?>" min="<?php echo date('Y-m-d\TH:i'); ?>" onchange="changeDate()" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="toDate">To</label>
                      <input type="datetime-local" class="form-control-rounded form-control" id="toDate" name="toDate" required onchange="calDays();">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">No of Days</label>
                      <input type="text" class="form-control-rounded form-control" id="noofdays"
                      readonly>
                    </div>
                    <div class="form-group mb-4 col-md-4 offset-md-8 text-right">
                      <input type="submit" class="btn btn-button-7 btn-rounded" value="Check Availability">
                      <input type="reset" class="btn btn-button-6 btn-rounded" value="Cancel">
                    </div>
                  </div>
                </form>
              </div>


              <!-- RESERVATION DETIALS -->
              <div class="tab-pane fade" id="rounded-pills-profile" role="tabpanel"
              aria-labelledby="rounded-pills-profile-tab">
              <form id="reservation_form">
                <div class="row">

                  <div class="form-group mb-4 col-md-4">
                    <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
                      <p
                      style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
                    Vehicle</p>
                    <div class="d-flex align-items-center mb-3 mt-3 ml-4">
                      <img src="<?php echo base_url(); ?>assets/assets/img/tuk.png" style="width: 30px;"
                      class="mr-4">
                      <p class="mb-0" id="vehicle_type"></p> &nbsp; - &nbsp;<span id="no_of_vehicle"></span>
                    </div>
                    <div class="d-flex align-items-center mb-3 ml-4">
                      <img src="<?php echo base_url(); ?>assets/assets/img/date.png" style="width: 30px;"
                      class="mr-4">
                      <p class="mb-0" id="number_of_days"></p> &nbsp; Days
                    </div>
                    <div class="d-flex align-items-center ml-4">
                      <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
                      class="mr-4">
                      € &nbsp; <p class="mb-0" id="total_vehicle_type_fee"></p>
                    </div>
                  </div>
                </div>

                <div class="form-group mb-4 col-md-4">
                  <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
                    <p
                    style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
                  Pickup</p>
                  <div class="d-flex align-items-center mb-3 mt-3 ml-4">
                    <img src="<?php echo base_url(); ?>assets/assets/img/map.png" style="width: 30px;"
                    class="mr-4">
                    <p class="mb-0" id="pickup_location"></p>
                  </div>
                  <div class="d-flex align-items-center mb-3 ml-4">
                    <img src="<?php echo base_url(); ?>assets/assets/img/datetime.png" style="width: 30px;"
                    class="mr-4">
                    <p class="mb-0" id="pickup_date"></p>
                  </div>
                  <div class="d-flex align-items-center ml-4">
                    <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
                    class="mr-4">
                    € &nbsp; <p class="mb-0" id="pickup_fee"></p>
                  </div>
                </div>
              </div>

              <div class="form-group mb-4 col-md-4">
                <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
                  <p
                  style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
                Return</p>
                <div class="d-flex align-items-center mb-3 mt-3 ml-4">
                  <img src="<?php echo base_url(); ?>assets/assets/img/map.png" style="width: 30px;"
                  class="mr-4">
                  <p class="mb-0" id="return_location"></p>
                </div>
                <div class="d-flex align-items-center mb-3 ml-4">
                  <img src="<?php echo base_url(); ?>assets/assets/img/datetime.png" style="width: 30px;"
                  class="mr-4">
                  <p class="mb-0" id="return_date"></p>
                </div>
                <div class="d-flex align-items-center ml-4">
                  <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
                  class="mr-4">
                  € &nbsp; <p class="mb-0" id="return_fee"></p>
                </div>
              </div>
            </div>

          </div>

          <div class="row">
            <div class="form-group mb-1 col-md-3">
              <label for="noOfLicense">No of Sri Lankan Driving Licenses</label>
              <input type="number" class="form-control-rounded form-control" placeholder="Enter No of License"
              name="noOfLicense" id="noOfLicense" required>
            </div>
            <div class="form-group mb-1 col-md-3">
              <label for="exampleFormControlInput1">No of Insurance for Tuk Tuk</label>
              <input type="number" class="form-control-rounded form-control" id="number_of_insurance"
              readonly name="number_of_insurance">
            </div>
            <div class="form-group mb-1 col-md-3 offset-md-3">
              <label>Drop & Pick-up Service for Train Travel</label>
              <select id="new_extra_services" class="form-control-rounded form-control">
              </select>
            </div>
          </div>

          <div class="row">
            <div class="form-group mb-4 col-md-3">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12 pl-2">
                      <h4 class="pb-0 pl-0">Drop & Pick-up Service for Train Travel</h4>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row" style="padding:0 20px;">
            <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Service</div>
            <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;">Fee (€)</div>
            <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;">No of Services
            </div>
          </div>

          <div id="extra_service_table"></div>

          <div class="row mt-5">
            <div class="form-group mb-4 col-md-4 offset-md-8 text-right">
              <span class="btn btn-button-7 btn-rounded" id="form_2_back_btn">Back</span>
              <input type="submit" class="btn btn-button-7 btn-rounded" value="Continue">
              <input type="reset" class="btn btn-button-6 btn-rounded" value="Cancel">
            </div>
          </div>
        </form>
      </div>


      <!-- RENTAL DETAILS -->
      <div class="tab-pane fade" id="rounded-pills-contact" role="tabpanel"
      aria-labelledby="ronded-pills-contact-tab">
      <form id="rental_form">
        <div class="row">

          <div class="form-group mb-4 col-md-4">
            <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
              <p
              style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
            Vehicle</p>
            <div class="d-flex align-items-center mb-3 mt-3 ml-4">
              <img src="<?php echo base_url(); ?>assets/assets/img/tuk.png" style="width: 30px;"
              class="mr-4">
              <p class="mb-0" id="vehicle_type_2"></p> &nbsp; - &nbsp;<span id="no_of_vehicle_2"></span>
            </div>
            <div class="d-flex align-items-center mb-3 ml-4">
              <img src="<?php echo base_url(); ?>assets/assets/img/date.png" style="width: 30px;"
              class="mr-4">
              <p class="mb-0" id="number_of_days_2"></p> &nbsp; Days
            </div>
            <div class="d-flex align-items-center ml-4">
              <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
              class="mr-4">
              € &nbsp; <p class="mb-0" id="total_vehicle_type_fee_2"></p>
            </div>
          </div>
        </div>

        <div class="form-group mb-4 col-md-4">
          <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
            <p
            style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
          Pickup</p>
          <div class="d-flex align-items-center mb-3 mt-3 ml-4">
            <img src="<?php echo base_url(); ?>assets/assets/img/map.png" style="width: 30px;"
            class="mr-4">
            <p class="mb-0" id="pickup_location_2"></p>
          </div>
          <div class="d-flex align-items-center mb-3 ml-4">
            <img src="<?php echo base_url(); ?>assets/assets/img/datetime.png" style="width: 30px;"
            class="mr-4">
            <p class="mb-0" id="pickup_date_2"></p>
          </div>
          <div class="d-flex align-items-center ml-4">
            <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
            class="mr-4">
            € &nbsp; <p class="mb-0" id="pickup_fee_2"></p>
          </div>
        </div>
      </div>

      <div class="form-group mb-4 col-md-4">
        <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
          <p
          style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
        Return</p>
        <div class="d-flex align-items-center mb-3 mt-3 ml-4">
          <img src="<?php echo base_url(); ?>assets/assets/img/map.png" style="width: 30px;"
          class="mr-4">
          <p class="mb-0" id="return_location_2"></p>
        </div>
        <div class="d-flex align-items-center mb-3 ml-4">
          <img src="<?php echo base_url(); ?>assets/assets/img/datetime.png" style="width: 30px;"
          class="mr-4">
          <p class="mb-0" id="return_date_2"></p>
        </div>
        <div class="d-flex align-items-center ml-4">
          <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
          class="mr-4">
          € &nbsp; <p class="mb-0" id="return_fee_2"></p>
        </div>
      </div>
    </div>

  </div>

  <!-- Selected Vehicle Fee -->
  <div class="row mt-3" style="padding:0 20px;">
    <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Selected Vehicle
    Fee</div>
    <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
    <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
  </div>

  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-6 px-0" id="selected_vehicle"></div>
    <div class="col-md-4 px-0" style="text-align: right;"><span id="vehicle_price"></span>&nbsp;*&nbsp;<span id="no_of_days"></span>&nbsp;*&nbsp;<span id="vehicle_count"></span></div>
    <div class="col-md-2 px-0" style="text-align: right;"><span id="total_vehicle_fee" style="padding-left: 20px;"></span></div>
  </div>

  <!-- Location Fee -->
  <div class="row mt-3" style="padding:0 20px;">
    <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Location Fee
    </div>
    <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
    <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
  </div>

  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-6 px-0">Pick-Up Fee</div>
    <div class="col-md-4 px-0" style="text-align: right;"><span id="pick_fee" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span class="no_of_veh"></span></div>
    <div class="col-md-2 px-0" style="text-align: right;"><span id="total_pick_fee" style="padding-left: 20px;"></span></div>
  </div>

  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-6 px-0">Return Fee</div>
    <div class="col-md-4 px-0" style="text-align: right;"><span id="retur_fee" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span class="no_of_veh"></span></div>
    <div class="col-md-2 px-0" style="text-align: right;"><span id="total_retur_fee" style="padding-left: 20px;"></span></div>
  </div>


  <!-- Service Fee -->
  <div class="row mt-3" style="padding:0 20px;">
    <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Service Fee</div>
    <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
    <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
  </div>

  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-6 px-0">License</div>
    <div class="col-md-4 px-0" style="text-align: right;"><span id="licen_fee" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span id="no_of_licen"></span></div>
    <div class="col-md-2 px-0" style="text-align: right;"><span id="total_licen_fee" style="padding-left: 20px;"></span></div>
  </div>

  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-6 px-0">Insurance for Tuk Tuk</div>
    <div class="col-md-4 px-0" style="text-align: right;"><span id="insuran_fee" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span id="no_of_insuran"></span></div>
    <div class="col-md-2 px-0" style="text-align: right;"><span id="total_insuran_fee" style="padding-left: 20px;"></span></div>
  </div>


  <!-- Extra Service Fee -->
  <div class="row mt-3 extra_service_table_2_heading" style="padding:0 20px;">
    <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Drop & Pick-up Service for Train Travel Fee</div>
    <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
    <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
  </div>

  <div id="extra_service_table_2"></div>

  <hr>
  <!-- Coupon Code -->
  <div class="row mt-4" style="padding:0 25px;">
    <div class="col-md-2 px-0">Coupon Code</div>
    <div class="col-md-2 px-0"><input type="text" class="form-control" id="coupon_code"></div>
    <div class="col-md-2 px-0">
      <a href="javascript:void" class="btn btn-button-7 btn-rounded ml-2" id="apply_coupon">Apply</a>
    </div>
    <div class="col-md-2 offset-md-2 px-0 text-right pr-2"><b>Sub Total :</b></div>
    <div class="col-md-2 px-0" style="text-align: right;"><b id="sub_total"></b></div>
  </div>
  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-2 offset-md-8 px-0 text-right pr-2">Discount :</div>
    <div class="col-md-2 px-0" id="coupon_discount" style="text-align: right;">0</div>
  </div>
  <div class="row mt-3" style="padding:0 25px;">
    <div class="col-md-6 px-0 text-right" style="background-color:#092766;padding: 10px;"></div>
    <div class="col-md-4 px-0 text-right pr-2" style="background-color:#092766;padding: 10px;">
      <h5 class="text-bold text-white">Pay Now / at Pick-up:</h5>
    </div>
    <div class="col-md-2 px-0" style="background-color:#092766;padding: 10px;text-align: right;">
      <h5 class="text-bold text-white" id="grand_total"></h5>
    </div>
  </div>


  <!-- User Firm -->
  <div class="row mt-5">
    <div class="form-group mb-4 col-md-6">
      <label for="fullName">Full Name</label>
      <input type="text" class="form-control-rounded form-control" placeholder="Enter Full Name"
      name="fullName" id="fullName" required>
    </div>
    <div class="form-group mb-4 col-md-3">
      <label for="mobile">Whatsapp No <i class="fa fa-whatsapp" aria-hidden="true" style="color: green;font-size: 20px;"></i></label><br>
      <input type="number" class="form-control-rounded form-control phone" name="mobile" id="mobile" placeholder="Enter Whatsapp No" required style="width:100%">
    </div>
    <div class="form-group mb-4 col-md-3">
      <label for="email">Email</label>
      <input type="email" class="form-control-rounded form-control" name="email" id="email"
      placeholder="Enter Email" required>
    </div>
    <div class="form-group mb-4 col-md-6">
      <label for="exampleFormControlInput1">Comment</label>
      <textarea class="form-control-rounded form-control" placeholder="Comment Here...." id="comment"></textarea>
    </div>
    <div class="form-group mt-5 col-md-4 offset-md-2 text-right">
      <span class="btn btn-button-7 btn-rounded" id="form_3_back_btn">Back</span>
      <input type="submit" class="btn btn-button-7 btn-rounded" value="Book">
      <input type="reset" class="btn btn-button-6 btn-rounded" value="Cancel">
    </div>
  </div>

</form>
</div>
</div>

</div>
</form>
</div>
</div>


<div class="row" id="cancel-row">
  <div class="col-xl-12 col-lg-12 col-sm-12">
    <div class="statbox widget box box-shadow" id="table_div">
      <div class="widget-content widget-content-area">
        <div class="table-responsive mb-4">
          <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
            <thead>
              <tr>
                <th>Booking No</th>
                <th>Full Name</th>
                <th>Booking Date</th>
                <th>Pickup Date</th>
                <th>Return Date</th>
                <th>Pickup Location</th>
                <th>Return Location</th>
                <th>Status</th>
                <th>Status Action</th>
                <th style="text-align:center;">Actions</th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($data as $booking) { ?>
                <tr>
                  <td><?php echo $booking->booking_no; ?></td>
                  <td><?php echo $booking->full_name; ?></td>
                  <td><?php echo $booking->booking_date; ?></td>
                  <td><?php echo $booking->from_date; ?></td>
                  <td><?php echo $booking->to_date; ?></td>
                  <td><?php echo $booking->pickup_location; ?></td>
                  <td><?php echo $booking->return_location; ?></td>
                  <td><?php echo $booking->status_name; ?></td>

                  <td class="text-left">

                    <?php if($booking->status_name == 'Pending') { ?>

                      <button class="btn btn-outline-success btn-rounded status_btn" alt="Confirm" id="<?php echo $booking->id; ?>">Confirm</button>
                      <button class="btn btn-outline-danger btn-rounded status_btn" alt="Reject" id="<?php echo $booking->id; ?>">Reject</button>

                    <?php } else if ($booking->status_name == 'Confirm') { ?>

                      <button class="btn btn-outline-success btn-rounded status_btn" alt="Start" id="<?php echo $booking->id; ?>">Start</button>

                    <?php } else if ($booking->status_name == 'Trip Start') { ?>

                      <button class="btn btn-outline-danger btn-rounded status_btn" alt="End" id="<?php echo $booking->id; ?>">End</button>

                    <?php } ?>

                  </td>

                  <td class="text-left">

                   <?php if($booking->status_name == 'Pending') { ?>

                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingById('<?php echo $booking->id; ?>');">Edit</button>
                    <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

                  <?php } else if ($booking->status_name == 'Reject') { ?>

                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingById('<?php echo $booking->id; ?>');">Edit</button>
                    <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

                  <?php } else if ($booking->status_name == 'Confirm') { ?>

                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingById('<?php echo $booking->id; ?>');">Edit</button>
                    <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

                  <?php } else if ($booking->status_name == 'Trip Start') { ?>

                    <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
                    <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

                  <?php } else if ($booking->status_name == 'Trip End') { ?>

                   <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $booking->id; ?>')">Delete</button>
                   <button class="btn btn-outline-secondary btn-rounded" onclick="getBookingViewById('<?php echo $booking->id; ?>');">View</button>

                 <?php } ?>

               </td>

             </tr>
           <?php } ?>
         </tbody>
       </table>
     </div>
   </div>
 </div>
</div>
</div>


</div>
<!--End of contain-->
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->



<script type="text/javascript">
  $('.status_btn').on('click',function () {
    let id = $(this).attr('id');
    let status = $(this).attr('alt');

    if (status == "Confirm") {
      confirmBooking(id);
    } else if (status == "Reject") {
      rejectBooking(id);
    }  else if (status == "Start") {
      startBooking(id);
    }  else if (status == "End") {
      endBooking(id);
    }

  });
</script>



<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" id="modal_view" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel_v">View Booking</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form>
          <div class="row">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="row">
                <div class="form-group mb-4 col-md-4">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <input type="text" class="form-control-rounded form-control" id="booked_no_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-4 offset-md-4">
                  <label for="exampleFormControlInput1">Date</label>
                  <input type="text" class="form-control-rounded form-control" id="booked_date_v" readonly>
                </div>
              </div>
            </div>

            <div class="form-group mb-4 col-md-4">
              <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
                <p
                style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
              Vehicle</p>
              <div class="d-flex align-items-center mb-3 mt-3 ml-4">
                <img src="<?php echo base_url(); ?>assets/assets/img/tuk.png" style="width: 30px;"
                class="mr-4">
                <p class="mb-0" id="vehicle_type_3_v"></p> &nbsp; - &nbsp;<span id="no_of_vehicle_3_v"></span>
              </div>
              <div class="d-flex align-items-center mb-3 ml-4">
                <img src="<?php echo base_url(); ?>assets/assets/img/date.png" style="width: 30px;"
                class="mr-4">
                <p class="mb-0" id="number_of_days_3_v"></p> &nbsp; Days
              </div>
              <div class="d-flex align-items-center ml-4">
                <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
                class="mr-4">
                € &nbsp; <p class="mb-0" id="total_vehicle_type_fee_3_v"></p>
              </div>
            </div>
          </div>

          <div class="form-group mb-4 col-md-4">
            <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
              <p
              style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
            Pickup</p>
            <div class="d-flex align-items-center mb-3 mt-3 ml-4">
              <img src="<?php echo base_url(); ?>assets/assets/img/map.png" style="width: 30px;"
              class="mr-4">
              <p class="mb-0" id="pickup_location_3_v"></p>
            </div>
            <div class="d-flex align-items-center mb-3 ml-4">
              <img src="<?php echo base_url(); ?>assets/assets/img/datetime.png" style="width: 30px;"
              class="mr-4">
              <p class="mb-0" id="pickup_date_3_v"></p>
            </div>
            <div class="d-flex align-items-center ml-4">
              <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
              class="mr-4">
              € &nbsp; <p class="mb-0" id="pickup_fee_3_v"></p>
            </div>
          </div>
        </div>

        <div class="form-group mb-4 col-md-4">
          <div class="widget-content widget-content-area" style="background-color: #f4f4f4;">
            <p
            style="text-align: center;font-size: 20px;background: #ff0208;color:#fff;border-radius: 20px;">
          Return</p>
          <div class="d-flex align-items-center mb-3 mt-3 ml-4">
            <img src="<?php echo base_url(); ?>assets/assets/img/map.png" style="width: 30px;"
            class="mr-4">
            <p class="mb-0" id="return_location_3_v"></p>
          </div>
          <div class="d-flex align-items-center mb-3 ml-4">
            <img src="<?php echo base_url(); ?>assets/assets/img/datetime.png" style="width: 30px;"
            class="mr-4">
            <p class="mb-0" id="return_date_3_v"></p>
          </div>
          <div class="d-flex align-items-center ml-4">
            <img src="<?php echo base_url(); ?>assets/assets/img/money.png" style="width: 30px;"
            class="mr-4">
            € &nbsp; <p class="mb-0" id="return_fee_3_v"></p>
          </div>
        </div>
      </div>

    </div>

    <!-- Selected Vehicle Fee -->
    <div class="row mt-3" style="padding:0 20px;">
      <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Selected Vehicle
      Fee</div>
      <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
      <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
    </div>

    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-6 px-0" id="selected_vehicle_2_v"></div>
      <div class="col-md-4 px-0" style="text-align: right;"><span id="vehicle_price_2_v"></span>&nbsp;*&nbsp;<span id="no_of_days_2_v"></span>&nbsp;*&nbsp;<span id="vehicle_count_2_v"></span></div>
      <div class="col-md-2 px-0" style="text-align: right;"><span id="total_vehicle_fee_2_v" style="padding-left: 20px;"></span></div>
    </div>

    <!-- Location Fee -->
    <div class="row mt-3" style="padding:0 20px;">
      <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Location Fee
      </div>
      <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
      <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
    </div>

    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-6 px-0">Pick-Up Fee</div>
      <div class="col-md-4 px-0" style="text-align: right;"><span id="pick_fee_2_v" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span class="no_of_veh_v"></span></div>
      <div class="col-md-2 px-0" style="text-align: right;"><span id="total_pick_fee_2_v" style="padding-left: 20px;"></span></div>
    </div>

    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-6 px-0">Return Fee</div>
      <div class="col-md-4 px-0" style="text-align: right;"><span id="retur_fee_2_v" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span class="no_of_veh_v"></span></div>
      <div class="col-md-2 px-0" style="text-align: right;"><span id="total_retur_fee_2_v" style="padding-left: 20px;"></span></div>
    </div>


    <!-- Service Fee -->
    <div class="row mt-3" style="padding:0 20px;">
      <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Service Fee</div>
      <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
      <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
    </div>

    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-6 px-0">License</div>
      <div class="col-md-4 px-0" style="text-align: right;"><span id="licen_fee_2_v" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span id="no_of_licen_2_v"></span></div>
      <div class="col-md-2 px-0" style="text-align: right;"><span id="total_licen_fee_2_v" style="padding-left: 20px;"></span></div>
    </div>

    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-6 px-0">Insurance for Tuk Tuk</div>
      <div class="col-md-4 px-0" style="text-align: right;"><span id="insuran_fee_2_v" style="padding-left: 20px;"></span>&nbsp;*&nbsp;<span id="no_of_insuran_2_v"></span></div>
      <div class="col-md-2 px-0" style="text-align: right;"><span id="total_insuran_fee_2_v" style="padding-left: 20px;"></span></div>
    </div>


    <!-- Extra Service Fee -->
    <div class="row mt-3 extra_service_table_3_v_heading" style="padding:0 20px;">
      <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Drop & Pick-up Service for Train Travel Fee</div>
      <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Price (€)</div>
      <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;text-align: right;">Total (€)</div>
    </div>

    <div id="extra_service_table_3_v"></div>

    <hr>
    <!-- Coupon Code -->
    <div class="row mt-4" style="padding:0 25px;">
      <div class="col-md-2 px-0">Coupon Code</div>
      <div class="col-md-2 px-0"><input type="text" class="form-control" readonly id="coupon_code_v"></div>
      <div class="col-md-2 px-0"></div>
      <div class="col-md-2 offset-md-2 px-0 text-right pr-2"><b>Sub Total :</b></div>
      <div class="col-md-2 px-0" style="text-align: right;"><b id="sub_total_2_v"></b></div>
    </div>
    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-2 offset-md-8 px-0 text-right pr-2">Discount :</div>
      <div class="col-md-2 px-0" id="discount_v" style="text-align: right;"></div>
    </div>
    <div class="row mt-3" style="padding:0 25px;">
      <div class="col-md-6 px-0 text-right" style="background-color:#092766;padding: 10px;"></div>
      <div class="col-md-4 px-0 text-right pr-2" style="background-color:#092766;padding: 10px;">
        <h5 class="text-bold text-white">Pay Now / at Pick-up:</h5>
      </div>
      <div class="col-md-2 px-0" style="background-color:#092766;padding: 10px;text-align: right;">
        <h5 class="text-bold text-white" id="grand_total_2_v"></h5>
      </div>
    </div>


    <!-- User Firm -->
    <div class="row mt-5">
      <div class="form-group mb-4 col-md-6">
        <label for="exampleFormControlInput1">Full Name</label>
        <input type="text" class="form-control-rounded form-control" id="fullName2_v" readonly>
      </div>
      <div class="form-group mb-4 col-md-3">
        <label for="exampleFormControlInput1">Whatsapp No <i class="fa fa-whatsapp" aria-hidden="true" style="color: green;font-size: 20px;"></i></label>
        <input type="text" class="form-control-rounded form-control phone3" id="mobile2_v" readonly style="width:100%">
      </div>
      <div class="form-group mb-4 col-md-3">
        <label for="exampleFormControlInput1">Email</label>
        <input type="email" class="form-control-rounded form-control" id="email2_v" readonly>
        <input type="hidden" class="form-control-rounded form-control" id="booked_id_v" readonly>
      </div>
      <div class="form-group mb-4 col-md-6">
        <label for="exampleFormControlInput1">Comment</label>
        <textarea class="form-control-rounded form-control" id="comment2_v" readonly></textarea>
      </div>
      <div class="form-group mt-5 col-md-4 offset-md-2 text-right">
        <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
      </div>
    </div>
  </form>
</div>
</div>
</div>
</div>


<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_edit">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Edit Booking - <span id="bookingNoLabel"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="booking_edit_form">
          <div class="row">
            <div class="form-group mb-4 col-md-3">
              <label for="vehicleType">Vehicle Type</label>
              <select class="form-control-rounded form-control" id="vehicletype2" name="vehicletype2" style="width:100%;">
              </select>
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="noOfVehicle">No of Vehicles</label>
              <input type="number" class="form-control-rounded form-control" id="no_of_vehicle_3" name="no_of_vehicle_3"
              placeholder=" Enter No of Vehicles" required>
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="pickupLocation">Pickup Location</label>
              <select class="form-control-rounded form-control search_dropdown" name="pickup_location_3" id="pickup_location_3" style="width:100%;">
                <option value="" disabled selected hidden>Enter Pickup Location</option>
              </select>
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="returnLocation">Return Location</label>
              <select class="form-control-rounded form-control search_dropdown" name="return_location_3" id="return_location_3" style="width:100%;">
                <option value="" disabled selected hidden>Enter Return Location</option>
              </select>
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="fromDate">From</label>
              <input type="datetime-local" class="form-control-rounded form-control" id="pickup_date_3" name="pickup_date_3" onchange="changeDateEdit()" required>
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="toDate">To</label>
              <input type="datetime-local" class="form-control-rounded form-control" id="return_date_3" name="return_date_3" required onchange="calDaysEdit();">
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="exampleFormControlInput1">No of Days</label>
              <input type="text" class="form-control-rounded form-control" id="number_of_days_3" name="number_of_days_3" readonly>
            </div>
            <div class="form-group mb-1 col-md-3">
              <label for="noOfLicense">No of Sri Lankan Driving Licenses</label>
              <input type="number" class="form-control-rounded form-control" placeholder="Enter No of License" name="no_of_licen_3" id="no_of_licen_3" required>
            </div>
          </div>

          <div class="row mt-2">
            <div class="col-md-3">
              <label>Coupon Code</label>
              <input type="text" name="couponCode2" id="couponCode2" class="form-control-rounded form-control">
            </div>
            <div class="col-md-3 mt-4">
              <a href="javascript:void" class="btn btn-button-7 btn-rounded ml-2 mt-1" id="apply_coupon_edit">Apply</a>
            </div>
            <div class="col-md-3"></div>
            <div class="col-md-3">
              <label>Drop & Pick-up Service for Train Travel</label>
              <select id="edit_extra_services" class="form-control-rounded form-control">
              </select>
            </div>
          </div>

          <!-- Extra Service Fee -->
          <div class="row mt-4" style="padding:0 20px;">
            <div class="col-md-6" style="background-color:#092766;color:#fff;padding: 10px;">Drop & Pick-up Service for Train Travel Fee</div>
            <div class="col-md-4" style="background-color:#092766;color:#fff;padding: 10px;">Price (€)</div>
            <div class="col-md-2" style="background-color:#092766;color:#fff;padding: 10px;">Total (€)</div>
          </div>

          <div id="extra_service_table_3"></div>

          <!-- User Firm -->
          <div class="row mt-5">
            <div class="form-group mb-4 col-md-6">
              <label for="exampleFormControlInput1">Full Name</label>
              <input type="text" class="form-control-rounded form-control" id="fullName2" name="fullName2">
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="exampleFormControlInput1">Whatsapp No <i class="fa fa-whatsapp" aria-hidden="true" style="color: green;font-size: 20px;"></i></label>
              <input type="text" class="form-control-rounded form-control phone2" id="mobile2" name="mobile2" style="width:100%">
            </div>
            <div class="form-group mb-4 col-md-3">
              <label for="exampleFormControlInput1">Email</label>
              <input type="email" class="form-control-rounded form-control" id="email2" name="email2">
              <input type="hidden" class="form-control-rounded form-control" id="booked_id">
            </div>
            <div class="form-group mb-4 col-md-6">
              <label for="exampleFormControlInput1">Comment</label>
              <textarea class="form-control-rounded form-control" id="comment2" name="comment2"></textarea>
            </div>
            <div class="form-group mt-5 col-md-4 offset-md-2 text-right">
             <input type="submit" class="btn btn-button-7 btn-rounded" value="Update">
             <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
           </div>
         </div>
       </form>
     </div>
   </div>
 </div>
</div>


<input type="hidden" id="search" value="0">


<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/intlTelInput.js"></script>

<script>

  var input = document.querySelector(".phone");
  var iti = window.intlTelInput(input, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  var input2 = document.querySelector(".phone2");
  var iti2 = window.intlTelInput(input2, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  var input3 = document.querySelector(".phone3");
  var iti3 = window.intlTelInput(input3, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  setTimeout(function() {
    $('.alert-messages').slideUp();
  }, 5000);


  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });


  getBookingNo();

  function getBookingNo() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>booking/getBookingNo',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#bookingNo').val(result.data.booking_no);
          $(".cs-overlay").css('visibility', 'hidden');
          getAllVehicleTypes();
        }        
      }
    });
  }


  function getPickupLocationsByVehicleType() {
    let vehicletype = $('#vehicletype').val();
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>booking/getPickupLocationsByVehicleType',
      data:{vehicletype},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);

          $.each(result.data, function (i, item) {
            $('#pickupLocation').append($('<option>', { 
              value: item.id,
              text : item.pickup_location+' '+item.fee+'(€)'
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
          getReturnLocationsByVehicleType();
        }        
      }
    });
  }


  function getReturnLocationsByVehicleType() {
    let vehicletype = $('#vehicletype').val();
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>booking/getReturnLocationsByVehicleType',
      data:{vehicletype},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#returnLocation').append($('<option>', { 
              value: item.id,
              text : item.return_location+' '+item.fee+'(€)'
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  function daysInMonth (month, year) {
    return new Date(year, month, 0).getDate();
  }
  
  //From and To Date selection
  var today = new Date();
  var hh = today.getHours();
  var ii = today.getMinutes();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();

  var dd = String(today.getDate() + 1).padStart(2, '0');

  var days_for_month = daysInMonth(mm, yyyy);

  if (dd >= days_for_month) {

    var nextDay = new Date(today);
    nextDay.setDate(today.getDate() + 1);
    var hh = nextDay.getHours();
    var ii = nextDay.getMinutes();
    var dd = String(nextDay.getDate()).padStart(2, '0');
    var mm = String(nextDay.getMonth() + 1).padStart(2, '0');
    var yyyy = nextDay.getFullYear();

  } 

  to_date = yyyy + '-' + mm + '-' + dd + 'T00:00';
  
  $('#toDate').attr('min', to_date);
  $('#toDate').val(to_date);

  function changeDate() {
    var from_today = new Date($('#fromDate').val());
    var dd = String(from_today.getDate() + 1).padStart(2, '0');
    var mm = String(from_today.getMonth() + 1).padStart(2, '0');
    var yyyy = from_today.getFullYear();

    if (dd >= days_for_month) {

      var nextDay = new Date(from_today);
      nextDay.setDate(from_today.getDate() + 1);
      var hh = nextDay.getHours();
      var ii = nextDay.getMinutes();
      var dd = String(nextDay.getDate()).padStart(2, '0');
      var mm = String(nextDay.getMonth() + 1).padStart(2, '0');
      var yyyy = nextDay.getFullYear();
    }

    from_today = yyyy + '-' + mm + '-' + dd + 'T00:00';
    $('#toDate').val(from_today);
    $('#toDate').attr('min', from_today);

    calDays();
  }


  function changeDateEdit() {
    var from_today = new Date($('#pickup_date_3').val());
    var dd = String(from_today.getDate() + 1).padStart(2, '0');
    var mm = String(from_today.getMonth() + 1).padStart(2, '0');
    var yyyy = from_today.getFullYear();

    from_today = yyyy + '-' + mm + '-' + dd + 'T00:00';
    $('#return_date_3').val(from_today);
    $('#return_date_3').attr('min', from_today);

    calDaysEdit();
  }


  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#vehicletype').empty();
          $.each(result.data, function (i, item) {
            $('#vehicletype').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));

            $('#vehicletype').trigger('change');

            $('#vehicletype2').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));

            $('#vehicletype2').trigger('change');
          });
          // $(".cs-overlay").css('visibility', 'hidden');

          calDays();
        }        
      }
    });
  }


    //validation forms
  $(document).ready(function () {
    $("#check_availability_form").validate({
      rules: {
        vehicletype: {
          required: true,
        },
        noOfVehicle: {
          required: true,
        },
        pickupLocation: {
          required: true,
        },
        returnLocation: {
          required: true,
        },
        fromDate: {
          required: true,
        },
        toDate: {
          required: true,
        },
      },
      messages: {
        vehicletype: {
          required: "Please enter Vehicle Type",
        },
        noOfVehicle: {
          required: "Please enter No of Vehicles",
        },
        pickupLocation: {
          required: "Please enter Pickup Location",
        },
        returnLocation: {
          required: "Please enter Return Location",
        },
        fromDate: {
          required: "Please enter Date",
          min:"Please enter a value greater than or equal to today's date & time",
        },
        toDate: {
          required: "Please enter Date",
        },
      }
    });


    $("#reservation_form").validate({
      rules: {
        noOfLicense: {
          required: true,
        },
      },
      messages: {
        noOfLicense: {
          required: "Please enter No of License",
        },
      }
    });


    $("#rental_form").validate({
      rules: {
        fullName: {
          required: true,
        },
        mobile: {
          required: true,
        },
        email: {
          required: true,
        },
      },
      messages: {
        fullName: {
          required: "Please enter Full Name",
        },
        mobile: {
          required: "Please enter Mobile No",
        },
        email: {
          required: "Please enter Email",
        },
      }
    });
  });

  $(document).ready(function(){
    $("#booking_edit_form").validate({
      rules: {
        vehicletype2: {
          required: true,
        },
        no_of_vehicle_3: {
          required: true,
        },
        pickup_location_3: {
          required: true,
        },
        return_location_3: {
          required: true,
        },
        pickup_date_3: {
          required: true,
        },
        return_date_3: {
          required: true,
        },
        no_of_licen_3: {
          required: true,
        },
        fullName2: {
          required: true,
        },
        mobile2: {
          required: true,
        },
        email2: {
          required: true,
        },
      },
      messages: {
        vehicletype2: {
          required: "Please enter Vehicle Type",
        },
        no_of_vehicle_3: {
          required: "Please enter No of Vehicles",
        },
        pickup_location_3: {
          required: "Please enter Pickup Location",
        },
        return_location_3: {
          required: "Please enter Return Location",
        },
        pickup_date_3: {
          required: "Please enter Date",
        },
        return_date_3: {
          required: "Please enter Date",
        },
        no_of_licen_3: {
          required: "Please enter No of License",
        },
        fullName2: {
          required: "Please enter Full Name",
        },
        mobile2: {
          required: "Please enter Mobile No",
        },
        email2: {
          required: "Please enter Email",
        },
      }
    });
  });

  $('#booking_edit_form').submit(function(evt) {
    evt.preventDefault();
    if( $('#booking_edit_form').valid() ) {
      let mobile_code2 = '+'+iti2.selectedCountryData.dialCode;
      let no_of_licen_3 = $('#no_of_licen_3').val();
      let vehicletype2 = $('#vehicletype2').val();
      let no_of_vehicle_3 = $('#no_of_vehicle_3').val();
      let pickup_location_3 = $('#pickup_location_3').val();
      let return_location_3 = $('#return_location_3').val();
      let pickup_date_3 = $('#pickup_date_3').val();
      let return_date_3 = $('#return_date_3').val();
      let number_of_days_3 = $('#number_of_days_3').val();
      let fullName2 = $('#fullName2').val();
      let mobile2 = $('#mobile2').val();
      let email2 = $('#email2').val();
      let comment2 = $('#comment2').val();
      let booked_id = $('#booked_id').val();
      let couponCode2 = $('#couponCode2').val();

      let jsonObj = [];
      $(".no_of_service_3").each(function() {
        var extra_service_id = $(this).attr("service_id");
        var no_of_service_3 = $(this).val();

        if (no_of_service_3 == "") {
          no_of_service_3 = 0;
        }

        item = {}
        item ["extra_service_id"] = extra_service_id;
        item ["no_of_service"] = no_of_service_3;

        jsonObj.push(item);
      });

      let extra_services_2 = JSON.stringify(jsonObj);

      $.ajax({
        type:'post',
        url:'<?php echo base_url(); ?>booking/updateBooking',
        data:{vehicletype2,no_of_vehicle_3,pickup_location_3,mobile_code2,return_location_3,pickup_date_3,return_date_3,number_of_days_3,no_of_licen_3,couponCode2,fullName2,mobile2,email2,comment2,extra_services_2,booked_id},
        beforeSend: function(){
          $(".cs-overlay").css('visibility', 'visible');
        },
        success:function (res) {
          getAllBookings('updated');   
          $('#modal_edit').modal('hide');   
        }
      });
    } 
  });


  $('#check_availability_form').submit(function(evt) {
    evt.preventDefault();

    if( $('#check_availability_form').valid() ) {

      let vehicletype = $('#vehicletype').val();
      let noOfVehicle = $('#noOfVehicle').val();
      let pickupLocation = $('#pickupLocation').val();
      let returnLocation = $('#returnLocation').val();
      let fromDate = $('#fromDate').val();
      let toDate = $('#toDate').val();
      let noofdays = $('#noofdays').val();

      $.ajax({
        type:'post',
        url:'<?php echo base_url(); ?>booking/checkAvailability',
        data:{vehicletype,noOfVehicle,pickupLocation,returnLocation,fromDate,toDate,noofdays},
        beforeSend: function(){
          $(".cs-overlay").css('visibility', 'visible');
        },
        success:function (res) {
          let result = JSON.parse(res);

          if (result.error) {

            $("html, body").animate({ scrollTop: 0 }, "fast");
            $('.alert-danger').html(result.error[0]);

            setTimeout(function() {
              $('.alert-messages2').slideDown();
            }, 100);

            setTimeout(function() {
              $('.alert-messages2').slideUp();
            }, 5000);

            $(".cs-overlay").css('visibility', 'hidden');

          } else {
            $('#vehicle_type').html(result.data.vehicle.type);
            $('#no_of_vehicle').html(result.data.vehicle.no_of_vehicle);
            $('#number_of_days').html(result.data.vehicle.number_of_days);
            $('#total_vehicle_type_fee').html(result.data.vehicle.total_vehicle_type_fee);
            $('#pickup_location').html(result.data.pickup_location.pickup_location);
            $('#pickup_date').html(result.data.pickup_location.pickup_date);
            $('#pickup_fee').html(result.data.pickup_location.total_fee);
            $('#return_location').html(result.data.return_location.return_location);
            $('#return_date').html(result.data.return_location.return_date);
            $('#return_fee').html(result.data.return_location.total_fee);
            $('#number_of_insurance').val(result.data.number_of_insurance);
            $('#noOfLicense').attr('min', result.data.vehicle.no_of_vehicle);

            $('#extra_service_table').html('');
            $('#new_extra_services').empty();
            $('#new_extra_services').append('<option value="" selected>Select Drop & Pick-up Service for Train Travel</option>');

            $.each(result.data.extra_services, function (i, item) {
              $('#new_extra_services').append('<option value="'+item.fee+'" nid="'+item.id+'" label="'+item.service+'">'+item.service+'</option>');
            });

            $(".cs-overlay").css('visibility', 'hidden');

            $('[href="#rounded-pills-profile"]').tab('show');
          }        
        }
      });

    } 
  });


  $('#reservation_form').submit(function(evt) {
    evt.preventDefault();

    if( $('#reservation_form').valid() ) {

      let noOfLicense = $('#noOfLicense').val();
      let vehicletype = $('#vehicletype').val();
      let noOfVehicle = $('#noOfVehicle').val();
      let pickupLocation = $('#pickupLocation').val();
      let returnLocation = $('#returnLocation').val();
      let fromDate = $('#fromDate').val();
      let toDate = $('#toDate').val();
      let noofdays = $('#noofdays').val();

      let jsonObj = [];
      $(".no_of_service").each(function() {
        var extra_service_id = $(this).attr("service_id");
        var no_of_service = $(this).val();

        if (no_of_service == "") {
          no_of_service = 0;
        }

        item = {}
        item ["extra_service_id"] = extra_service_id;
        item ["no_of_service"] = no_of_service;

        jsonObj.push(item);
      });

      let extra_services = JSON.stringify(jsonObj);

      $.ajax({
        type:'post',
        url:'<?php echo base_url(); ?>booking/getRentalDetails',
        data:{noOfLicense,vehicletype,noOfVehicle,pickupLocation,returnLocation,fromDate,toDate,noofdays,extra_services},
        beforeSend: function(){
          $(".cs-overlay").css('visibility', 'visible');
        },
        success:function (res) {
          if (!res) {
            location.reload();  
          } else {
            let result = JSON.parse(res);

            $('#vehicle_type_2').html(result.data.vehicle.type);
            $('#no_of_vehicle_2').html(result.data.vehicle.no_of_vehicle);
            $('#number_of_days_2').html(result.data.vehicle.number_of_days);
            $('#total_vehicle_type_fee_2').html(result.data.vehicle.total_vehicle_type_fee);
            $('#pickup_location_2').html(result.data.pickup_location.pickup_location);
            $('#pickup_date_2').html(result.data.pickup_location.pickup_date);
            $('#pickup_fee_2').html(result.data.pickup_location.total_fee);
            $('#return_location_2').html(result.data.return_location.return_location);
            $('#return_date_2').html(result.data.return_location.return_date);
            $('#return_fee_2').html(result.data.return_location.total_fee);
            $('#selected_vehicle').html(result.data.vehicle.type);
            $('#vehicle_price').html(result.data.vehicle.vehicle_fee);
            $('#no_of_days').html(result.data.vehicle.number_of_days);
            $('#vehicle_count').html(result.data.vehicle.no_of_vehicle);
            $('#total_vehicle_fee').html(result.data.vehicle.total_vehicle_type_fee);
            $('#pick_fee').html(result.data.pickup_location.fee);
            $('.no_of_veh').html(result.data.vehicle.no_of_vehicle);
            $('#retur_fee').html(result.data.return_location.fee);
            $('#total_pick_fee').html(result.data.pickup_location.total_fee);
            $('#total_retur_fee').html(result.data.return_location.total_fee);
            $('#licen_fee').html(result.data.service.license_fee);
            $('#no_of_licen').html(result.data.service.no_of_license);
            $('#total_licen_fee').html(result.data.service.total_license_fee);
            $('#insuran_fee').html(result.data.service.insurance_fee);
            $('#no_of_insuran').html(result.data.service.no_of_insurance);
            $('#total_insuran_fee').html(result.data.service.total_insurance_fee);
            $('#sub_total').html(result.data.total_amount);
            $('#grand_total').html(result.data.total_amount);
            $('#extra_service_table_2').html('');

            $.each(result.data.extra_services, function (i, item) {
              if (item.no_of_service != 0) {
                $('#extra_service_table_2').append('<div class="row mt-3" style="padding:0 25px;"> <div class="col-md-6 px-0">'+item.service+'</div> <div class="col-md-4 px-0" style="text-align: right;"><span style="padding-left:20px;">'+item.fee+'</span>&nbsp;*&nbsp;<span>'+item.no_of_service+'</span></div> <div class="col-md-2 px-0" style="text-align: right;"><span style="padding-left:20px;">'+item.total_extra_service_fee+'</span></div> </div>');
              }                
            });

            if ($('#extra_service_table_2').is(':empty')){
              $('.extra_service_table_2_heading').hide();
            } else {
              $('.extra_service_table_2_heading').show();
            }

            $(".cs-overlay").css('visibility', 'hidden');
          }        
        }
      });

      $('[href="#rounded-pills-contact"]').tab('show');
    } 
  });



$('#rental_form').submit(function(evt) {
  evt.preventDefault();

  if( $('#rental_form').valid() ) {

    let mobile_code = '+'+iti.selectedCountryData.dialCode;
    let noOfLicense = $('#noOfLicense').val();
    let vehicletype = $('#vehicletype').val();
    let noOfVehicle = $('#noOfVehicle').val();
    let pickupLocation = $('#pickupLocation').val();
    let returnLocation = $('#returnLocation').val();
    let fromDate = $('#fromDate').val();
    let toDate = $('#toDate').val();
    let noofdays = $('#noofdays').val();
    let couponCode = $('#coupon_code').val();
    let fullName = $('#fullName').val();
    let mobile = $('#mobile').val();
    let email = $('#email').val();
    let comment = $('#comment').val();

    let jsonObj = [];
    $(".no_of_service").each(function() {
      var extra_service_id = $(this).attr("service_id");
      var no_of_service = $(this).val();

      if (no_of_service == "") {
        no_of_service = 0;
      }

      item = {}
      item ["extra_service_id"] = extra_service_id;
      item ["no_of_service"] = no_of_service;

      jsonObj.push(item);
    });

    let extra_services = JSON.stringify(jsonObj);

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>booking/saveBooking',
      data:{vehicletype,noOfVehicle,pickupLocation,returnLocation,mobile_code,fromDate,toDate,noofdays,noOfLicense,couponCode,fullName,mobile,email,comment,extra_services},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        location.reload();        
      }
    });
  } 
});


$('#form_2_back_btn').on('click', function () {
  $('[href="#rounded-pills-home"]').tab('show');
});

$('#form_3_back_btn').on('click', function () {
  $('[href="#rounded-pills-profile"]').tab('show');
});

$("select").on("select2:close", function (e) {  
  $(this).valid(); 
});


function calDays() {
 var start = new Date($('#fromDate').val());
 var end = new Date($('#toDate').val());

 start.setHours(0,0,0,0);
 end.setHours(0,0,0,0);

 var diff = new Date(end - start);

 var days = Math.ceil(diff/1000/60/60/24) + 1;

 $('#noofdays').val(days);
}


function calDaysEdit() {
 var start = new Date($('#pickup_date_3').val());
 var end = new Date($('#return_date_3').val());

 start.setHours(0,0,0,0);
 end.setHours(0,0,0,0);

 var diff = new Date(end - start);

 var days = Math.ceil(diff/1000/60/60/24) + 1;

 $('#number_of_days_3').val(days);
}


function getBookingById(id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getBookingById',
    data:{id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);

        iti2.setNumber(result.data.mobile_country_code);
        $('#bookingNoLabel').html(result.data.booking_no);
        $('#vehicletype2').val(result.data.vehicle.vehicle_type_id);
        $('#no_of_vehicle_3').val(result.data.vehicle.no_of_vehicle);
        $('#number_of_days_3').val(result.data.vehicle.no_of_days);
        $('#total_vehicle_type_fee_3').html(result.data.vehicle.total_vehicle_type_fee);
        $('#pickup_date_3').val(result.data.pickup.pickup_date);
        $('#return_date_3').val(result.data.return.return_date);
        $('#return_date_3').attr('min', result.data.pickup.pickup_date);
        $('#no_of_licen_3').val(result.data.service.no_of_license);
        $('#fullName2').val(result.data.full_name);
        $('#mobile2').val(result.data.mobile_no);
        $('#email2').val(result.data.email);
        $('#comment2').val(result.data.comment);
        $('#booked_id').val(result.data.id);
        $('#couponCode2').val(result.data.coupon_code);

        $('#extra_service_table_3').html('');
        $('#edit_extra_services').empty();
        $('#edit_extra_services').append('<option value="" selected>Select Drop & Pick-up Service for Train Travel</option>');

        let extra_services_id_arr = [];
        let extra_services_val_arr = [];

        $.each(result.data.extra_service, function (e, e_item) {
          extra_services_id_arr.push(e_item.extra_service_id);
          extra_services_val_arr.push(e_item.no_of_service);
        });

        $.each(result.data.all_extra_service, function (i, item) {
          if (extra_services_id_arr.includes(item.id)) {
            let index_val = extra_services_id_arr.indexOf(item.id);
            $('#extra_service_table_3').append('<div class="row mt-3" style="padding:0 25px;" id="row_'+item.id+'"> <div class="col-md-6 px-0">'+item.service+'</div> <div class="col-md-4 px-0">'+item.fee+'</div> <div class="col-md-2 px-0"><input type="number" value="'+extra_services_val_arr[index_val]+'" id="service_id_'+item.id+'" class="form-control no_of_service_3" service_id="'+item.id+'" style="width:120px;display:inline;" readonly> &nbsp; <a href="javascript:void" onclick="removeService(`'+item.id+'`, `'+item.fee+'`, `'+item.service+'`)" style="color:red;"><i class="fa fa-trash"></i></a> </div> </div>');
          } else {
            $('#edit_extra_services').append('<option value="'+item.fee+'" eid="'+item.id+'" label="'+item.service+'">'+item.service+'</option>');
          }          
        });

        $('#action_btn_div').html('');

        if(result.data.status_name == 'Pending') {

          $('#action_btn_div').append(`<button class="btn btn-outline-success btn-rounded status_btn" alt="Confirm" id="`+result.data.id+`">Confirm</button>
            <button class="btn btn-outline-danger btn-rounded status_btn" alt="Reject" id="`+result.data.id+`">Reject</button>
            <button class="btn btn-outline-info btn-rounded" onclick="checkDelete(`+result.data.id+`)">Delete</button>
            <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">`);

        } else if (result.data.status_name == 'Reject') {

          $('#action_btn_div').append(`<button class="btn btn-outline-info btn-rounded" onclick="checkDelete(`+result.data.id+`)">Delete</button> <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">`);

        } else if (result.data.status_name == 'Confirm') {

          $('#action_btn_div').append(`<button class="btn btn-outline-success btn-rounded status_btn" alt="Start" id="`+result.data.id+`">Start</button>
            <button class="btn btn-outline-info btn-rounded" onclick="checkDelete(`+result.data.id+`)">Delete</button> <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">`);

        } else if (result.data.status_name == 'Trip Start') {

          $('#action_btn_div').append(`<button class="btn btn-outline-danger btn-rounded status_btn" alt="End" id="`+result.data.id+`">End</button>
            <button class="btn btn-outline-info btn-rounded" onclick="checkDelete(`+result.data.id+`)">Delete</button> <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">`);

        } else if (result.data.status_name == 'Trip End') {

          $('#action_btn_div').append(`<button class="btn btn-outline-info btn-rounded" onclick="checkDelete(`+result.data.id+`)">Delete</button> <input type="reset" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">`);

        }

        $('#modal_edit').modal('show');
        $(".cs-overlay").css('visibility', 'hidden');
        triggerPickupLocationsByVehicleType(result.data.pickup.pickup_location_id, result.data.return.return_location_id);
      }
    }
  });
}


function triggerPickupLocationsByVehicleType(pickup_location, return_location) {
  let vehicletype = $('#vehicletype2').val();
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getPickupLocationsByVehicleType',
    data:{vehicletype},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $('#pickup_location_3').empty();
        $('#pickup_location_3').append('<option value="" selected>Select Pickup Location</option>');
        $.each(result.data, function (i, item) {
          $('#pickup_location_3').append($('<option>', { 
            value: item.id,
            text : item.pickup_location 
          }));
        });

        $('#pickup_location_3').val(pickup_location);
        $('#pickup_location_3').trigger('change');

        $(".cs-overlay").css('visibility', 'hidden');
        triggerReturnLocationsByVehicleType(return_location);
      }        
    }
  });
}



function triggerReturnLocationsByVehicleType(return_location) {
  let vehicletype = $('#vehicletype2').val();
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getReturnLocationsByVehicleType',
    data:{vehicletype},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        $('#return_location_3').empty();
        $('#return_location_3').append('<option value="" selected>Select Return Location</option>');
        $.each(result.data, function (i, item) {
          $('#return_location_3').append($('<option>', { 
            value: item.id,
            text : item.return_location 
          }));
        });

        $('#return_location_3').val(return_location);
        $('#return_location_3').trigger('change');

        $(".cs-overlay").css('visibility', 'hidden');
      }        
    }
  });
}

function getBookingViewById(id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getBookingById',
    data:{id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      if (!res) {
        location.reload();  
      } else {
        let result = JSON.parse(res);
        iti3.setNumber(result.data.mobile_country_code);
        $('#booked_no_v').val(result.data.booking_no);
        $('#booked_date_v').val(result.data.booking_date);
        $('#vehicle_type_3_v').html(result.data.vehicle.type);
        $('#no_of_vehicle_3_v').html(result.data.vehicle.no_of_vehicle);
        $('#number_of_days_3_v').html(result.data.vehicle.no_of_days);
        $('#total_vehicle_type_fee_3_v').html(result.data.vehicle.total_vehicle_type_fee);
        $('#pickup_location_3_v').html(result.data.pickup.pickup_location);
        $('#pickup_date_3_v').html(result.data.pickup.pickup_date);
        $('#pickup_fee_3_v').html(result.data.pickup.total_pickup_fee);
        $('#return_location_3_v').html(result.data.return.return_location);
        $('#return_date_3_v').html(result.data.return.return_date);
        $('#return_fee_3_v').html(result.data.return.total_return_fee);
        $('#selected_vehicle_2_v').html(result.data.vehicle.type);
        $('#vehicle_price_2_v').html(result.data.vehicle.vehicle_type_fee);
        $('#no_of_days_2_v').html(result.data.vehicle.no_of_days);
        $('#vehicle_count_2_v').html(result.data.vehicle.no_of_vehicle);
        $('#total_vehicle_fee_2_v').html(result.data.vehicle.total_vehicle_type_fee);
        $('.no_of_veh_v').html(result.data.vehicle.no_of_vehicle);
        $('#pick_fee_2_v').html(result.data.pickup.pickup_fee);
        $('#retur_fee_2_v').html(result.data.return.return_fee);
        $('#total_pick_fee_2_v').html(result.data.pickup.total_pickup_fee);
        $('#total_retur_fee_2_v').html(result.data.return.total_return_fee);
        $('#licen_fee_2_v').html(result.data.service.license_fee);
        $('#no_of_licen_2_v').html(result.data.service.no_of_license);
        $('#total_licen_fee_2_v').html(result.data.service.total_license_fee);
        $('#insuran_fee_2_v').html(result.data.service.insurance_fee);
        $('#no_of_insuran_2_v').html(result.data.service.no_of_insurance);
        $('#total_insuran_fee_2_v').html(result.data.service.total_insurance_fee);
        $('#sub_total_2_v').html(result.data.sub_total);
        $('#discount_v').html(result.data.discount);    
        $('#fullName2_v').val(result.data.full_name);
        $('#mobile2_v').val(result.data.mobile_no);
        $('#email2_v').val(result.data.email);
        $('#comment2_v').text(result.data.comment);
        $('#grand_total_2_v').html(result.data.total_amount);
        $('#booked_id_v').val(result.data.id);
        $('#coupon_code_v').val(result.data.coupon_code);

        $('#extra_service_table_3_v').html('');

        $.each(result.data.extra_service, function (i, item) {
          if (item.no_of_service != 0) {
            $('#extra_service_table_3_v').append('<div class="row mt-3" style="padding:0 25px;"> <div class="col-md-6 px-0">'+item.service+'</div> <div class="col-md-4 px-0" style="text-align: right;"><span style="padding-left:20px;">'+item.fee+'</span>&nbsp;*&nbsp;<span>'+item.no_of_service+'</span></div> <div class="col-md-2 px-0" style="text-align: right;"><span style="padding-left:20px;">'+item.total_extra_service_fee+'</span></div> </div>');
          }
        });

        if ($('#extra_service_table_3_v').is(':empty')){
          $('.extra_service_table_3_v_heading').hide();
        } else {
          $('.extra_service_table_3_v_heading').show();
        }

        $('#modal_view').modal('show');
        $(".cs-overlay").css('visibility', 'hidden');
      }
    }
  });
}


function rejectBooking(booked_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/rejectBooking',
    data:{booked_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let search = $('#search').val();
     if (search == 1) {
      $('#viewBookingForm').submit();
    } else {
      getAllBookings('rejected');
    } 
  }
});
}


function confirmBooking(booked_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/confirmBooking',
    data:{booked_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let search = $('#search').val();
     if (search == 1) {
      $('#viewBookingForm').submit();
    } else {
      getAllBookings('confirmed');
    } 
  }
});
}

function startBooking(booked_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/startBooking',
    data:{booked_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {

      if (res == 'success') {

        let search = $('#search').val();
        if (search == 1) {
          $('#viewBookingForm').submit();
        } else {
          getAllBookings('started');
        } 

      } else {

       $(".cs-overlay").css('visibility', 'hidden');

       $.confirm({
        title: 'Error!',
        content: res,
        type: 'red',
        draggable: false,
        buttons: {
          tryAgain: {
            text: 'Close',
            action: function(){
            }
          },
        }
      });

     }
   }
 });
}

function endBooking(booked_id) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/endBooking',
    data:{booked_id},
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
     let search = $('#search').val();
     if (search == 1) {
      $('#viewBookingForm').submit();
    } else {
      getAllBookings('ended');
    } 
  }
});
}

function checkDelete(id) {
  $.confirm({
    title: 'Confirm!',
    content: 'Are you sure, do you want to delete this?',
    type: 'red',
    buttons: {
      confirm: function(){
        $.ajax({
          type:'post',
          url:'<?php echo base_url(); ?>booking/deleteBooking/'+id,
          beforeSend: function(){
            $(".cs-overlay").css('visibility', 'visible');
          },
          success:function (res) {
            let search = $('#search').val();
            if (search == 1) {
              $('#viewBookingForm').submit();
            } else {
              getAllBookings('deleted');
            }            
          }
        });
      },
      cancel : {

      }
    }
  });
}


function getAllBookings(message) {
  $.ajax({
    type:'post',
    url:'<?php echo base_url(); ?>booking/getAllBookings',
    beforeSend: function(){
      $(".cs-overlay").css('visibility', 'visible');
    },
    success:function (res) {
      $('#table_div').html(res);   
      $(".cs-overlay").css('visibility', 'hidden');  

      if (message == 'updated') {

        $.confirm({
          title: 'Success!',
          content: 'Booking successfully '+message+'!',
          type: 'green',
          draggable: false,
          buttons: {
            showView: {
              text: 'Show Details',
              action: function(){
                let updated_id = $('#booked_id').val();
                getBookingViewById(updated_id);
              }
            },
            tryAgain: {
              text: 'Close',
              action: function(){
              }
            },
          }
        });

      } else {

        $.confirm({
          title: 'Success!',
          content: 'Booking successfully '+message+'!',
          type: 'green',
          draggable: false,
          buttons: {
            tryAgain: {
              text: 'Close',
              action: function(){
              }
            },
          }
        });

      }
    }
  });
}


$('#edit_extra_services').on('change', function () {
  var no_of_ser_e = $('#no_of_vehicle_3').val();
  if ($('option:selected', this).val() != "") {
    $('#extra_service_table_3').append('<div class="row mt-3" style="padding:0 25px;" id="row_'+$('option:selected', this).attr('eid')+'"> <div class="col-md-6 px-0">'+$('option:selected', this).attr('label')+'</div> <div class="col-md-4 px-0">'+$('option:selected', this).val()+'</div> <div class="col-md-2 px-0"><input type="number"  id="service_id_'+$('option:selected', this).attr('eid')+'" value="'+no_of_ser_e+'" class="form-control no_of_service_3" service_id="'+$('option:selected', this).attr('eid')+'" style="width:120px;display:inline;" readonly> &nbsp; <a href="javascript:void" onclick="removeService(`'+$('option:selected', this).attr('eid')+'`, `'+$('option:selected', this).val()+'`, `'+$('option:selected', this).attr('label')+'`)" style="color:red;"><i class="fa fa-trash"></i></a></div> </div>');

    $('#edit_extra_services option:selected').remove();
  }  
});

$('#new_extra_services').on('change', function () {
  var no_of_ser = $('#number_of_insurance').val();
  if ($('option:selected', this).val() != "") {
    $('#extra_service_table').append('<div class="row mt-3" style="padding:0 25px;" id="row_'+$('option:selected', this).attr('nid')+'"> <div class="col-md-6 px-0">'+$('option:selected', this).attr('label')+'</div> <div class="col-md-4 px-0">'+$('option:selected', this).val()+'</div> <div class="col-md-2 px-0"><input type="number"  id="service_id_'+$('option:selected', this).attr('nid')+'" value="'+no_of_ser+'" class="form-control no_of_service" service_id="'+$('option:selected', this).attr('nid')+'" style="width:120px;display:inline;" readonly> &nbsp; <a href="javascript:void" onclick="removeServiceNew(`'+$('option:selected', this).attr('nid')+'`, `'+$('option:selected', this).val()+'`, `'+$('option:selected', this).attr('label')+'`)" style="color:red;"><i class="fa fa-trash"></i></a></div> </div>');

    $('#new_extra_services option:selected').remove();
  }  
});

function removeService(service_id, fee, service) {
  $('#service_id_'+service_id).val(0);
  $('#row_'+service_id).remove();
  $('#edit_extra_services').append('<option value="'+fee+'" eid="'+service_id+'" label="'+service+'">'+service+'</option>');
  $('#booking_edit_form').submit();
}

function removeServiceNew(service_id, fee, service) {
  $('#service_id_'+service_id).val(0);
  $('#row_'+service_id).remove();
  $('#new_extra_services').append('<option value="'+fee+'" nid="'+service_id+'" label="'+service+'">'+service+'</option>');
}

$('#apply_coupon').on('click', function () {
  let couponCode = $('#coupon_code').val();
  let noOfLicense = $('#noOfLicense').val();
  let vehicletype = $('#vehicletype').val();
  let noOfVehicle = $('#noOfVehicle').val();
  let pickupLocation = $('#pickupLocation').val();
  let returnLocation = $('#returnLocation').val();
  let fromDate = $('#fromDate').val();
  let toDate = $('#toDate').val();
  let noofdays = $('#noofdays').val();

  let jsonObj = [];
  $(".no_of_service").each(function() {
    var extra_service_id = $(this).attr("service_id");
    var no_of_service = $(this).val();

    if (no_of_service == "") {
      no_of_service = 0;
    }

    item = {}
    item ["extra_service_id"] = extra_service_id;
    item ["no_of_service"] = no_of_service;

    jsonObj.push(item);
  });

  let extra_services = JSON.stringify(jsonObj);

  if (couponCode != '') {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>booking/applyCouponCode/',
      data:{couponCode,noOfLicense,vehicletype,noOfVehicle,pickupLocation,returnLocation,fromDate,toDate,noofdays,extra_services},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $(".cs-overlay").css('visibility', 'hidden');   
        let result = JSON.parse(res);

        if (result.error) {

          $("html, body").animate({ scrollTop: 0 }, "fast");
          $('.alert-danger').html(result.error[0]);

          setTimeout(function() {
            $('.alert-messages2').slideDown();
          }, 100);

          setTimeout(function() {
            $('.alert-messages2').slideUp();
          }, 5000);

        } else {
          $('#sub_total').html(result.data.sub_total);
          $('#coupon_discount').html(result.data.discount);
          $('#grand_total').html(result.data.total_amount);
        }          
      }
    });
  }
});


$('#apply_coupon_edit').on('click', function () {
  let couponCode = $('#couponCode2').val();
  let noOfLicense = $('#no_of_licen_3').val();
  let vehicletype = $('#vehicletype2').val();
  let noOfVehicle = $('#no_of_vehicle_3').val();
  let pickupLocation = $('#pickup_location_3').val();
  let returnLocation = $('#return_location_3').val();
  let fromDate = $('#pickup_date_3').val();
  let toDate = $('#return_date_3').val();
  let noofdays = $('#number_of_days_3').val();

  let jsonObj = [];
  $(".no_of_service_3").each(function() {
    var extra_service_id = $(this).attr("service_id");
    var no_of_service = $(this).val();

    if (no_of_service == "") {
      no_of_service = 0;
    }

    item = {}
    item ["extra_service_id"] = extra_service_id;
    item ["no_of_service"] = no_of_service;

    jsonObj.push(item);
  });

  let extra_services = JSON.stringify(jsonObj);

  if (couponCode != '') {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>booking/applyCouponCode/',
      data:{couponCode,noOfLicense,vehicletype,noOfVehicle,pickupLocation,returnLocation,fromDate,toDate,noofdays,extra_services},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $(".cs-overlay").css('visibility', 'hidden');   
        let result = JSON.parse(res);

        if (result.data.discount == 0) {

          $.confirm({
            title: 'Error!',
            content: 'Coupon is Invalid!',
            type: 'red',
            draggable: false,
            buttons: {
              tryAgain: {
                text: 'Close',
                action: function(){
                }
              },
            }
          });

        } else {

          $.confirm({
            title: 'Success!',
            content: 'Coupon successfully applied!',
            type: 'green',
            draggable: false,
            buttons: {
              tryAgain: {
                text: 'Close',
                action: function(){
                }
              },
            }
          });

        }          
      }
    });
  }
});
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>