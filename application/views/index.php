<!DOCTYPE html>
<html lang="en">
<head>

  <?php require 'includes/top_header.php'; ?> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link href="<?php echo base_url(); ?>assets/dist/css/vendor/foundation.css" media="screen" rel="stylesheet" type="text/css" />
  <link href="<?php echo base_url(); ?>assets/dist/css/pizza.css" media="screen, projector, print" rel="stylesheet" type="text/css" />

  <style type="text/css">
    #multi-column-ordering_filter label {
      float: right;
    }

    #sidebar ul {
      margin-left: 0;
    }
  </style>
</head>
<body>
  <!-- Tab Mobile View Header -->
  <?php require 'includes/header.php'; ?> 
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require 'includes/sidebar.php'; ?> 

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Dashboard</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Dashboard</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <div class="row">
          <div class="col-xl-8 col-lg-12 col-md-12 col-12 layout-spacing">

            <div class="row">
              <div class="col-lg-6 col-md-6 col-sm-6 mb-4">
                <div class="widget-content widget-content-area h-100 br-4">
                  <div id="top-sale" class="widget-card">
                    <div class="row">
                      <div class="col-12">
                        <h6 class="mb-0">Total Income</h6>
                        <p class="t-s-stats" id="total_income"><?php echo $data->income; ?></p>
                      </div>

                      <div class="col-12">
                        <div id="t-s-chart"></div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>

              <div class="col-xl-6 col-lg-12 col-12 mb-4">
                <div class="widget-content widget-content-area expanditure-content br-4">
                  <div class="expanditure-header">
                    <div class="row">
                      <div class="col-md-5 col-sm-5 col-6">
                        <h6 class="">Total Bookings</h6>
                      </div>
                      <div class="col-md-7 col-sm-7 col-6 text-sm-right">
                      </div>
                    </div>
                  </div>

                  <div class="expanditure-body">

                    <div class="tab-content" id="nav-tabContent">

                      <div class="tab-pane fade show active" id="expanditure-monthly" role="tabpanel" aria-labelledby="expanditure-monthly-tab">
                        <p class="e-meta-date"><?php echo date('d M Y'); ?></p>

                        <div class="row">
                          <div class="col-md-12 col-sm-12 col-12 mt-4 text-center">
                            <img src="<?php echo base_url(); ?>assets/assets/img/charthome.png">
                          </div>

                          <div class="col-md-12 col-sm-12 col-12">
                            <p class="e-value mt-3 text-center" id="booking_count"><?php echo $data->booking_count; ?></p>
                          </div>
                        </div>
                      </div>

                    </div>

                  </div>

                </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-6">
                <div class="widget-content widget-content-area h-100 br-4">
                  <div id="top-sale2" class="widget-card">
                    <div class="row">
                      <div class="col-12">
                        <h6 class="mb-0">Total Agreements</h6>
                        <p class="t-s-stats" id="agreement_count"><?php echo $data->agreement_count; ?></p>
                      </div>

                      <div class="col-12">
                        <div id="t-s-chart2"></div>
                      </div>

                    </div>
                  </div>
                </div>
              </div>


              <div class="col-lg-6 col-md-6 col-sm-6 mb-sm-0 mb-4">
                <div class="widget-content widget-content-area h-100 br-4">
                  <div id="total-reviews" class="widget-card">
                    <div class="row">
                      <div class="col-12">
                        <h6 class="mb-5">Total Vehicles</h6>
                      </div>

                      <div class="col-md-6 col-sm-12 col-12 mb-md-0 mb-5 text-center" style="display:flex;align-items: center;justify-content: center;">
                        <div style="border:1px solid;border-radius: 50%;padding:30px;">
                          <h3 id="all_vehicle_count"><?php echo $data->all_vehicle_count; ?></h3>
                          <p style="margin-bottom: 0 !important;">Vehicles</p>
                        </div>
                      </div>

                      <div class="col-md-6 col-sm-12 col-12">

                        <div class="media positive-stats">
                          <img alt="emoji" src="<?php echo base_url(); ?>assets/assets/img/available_tuk.png" class="img-fluid mr-3">
                          <div class="media-body">
                            <h6 class="mb-0">Booked</h6>
                            <p><?php echo $data->booked_vehicle_count; ?> vehicles</p>
                          </div>
                        </div>

                        <div class="media negative-stats">
                          <img alt="emoji" src="<?php echo base_url(); ?>assets/assets/img/booked_tuk.png" class="img-fluid mr-3">
                          <div class="media-body">
                            <h6 class="mb-0">Available</h6>
                            <p><?php echo $data->available_vehicle_count; ?> vehicles</p>
                          </div>
                        </div>

                      </div>
                      <div class="col-md-12">
                        <p class="e-value mt-3 text-center"><br/><br/><br/></p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>

          <div class="col-xl-4 layout-spacing">
            <div class="widget-content widget-content-area h-100  br-4">

              <div class="revenue-statistics">

                <div class="revenue-statistics-title">
                  <h6>Revenue</h6>
                </div>

                <div class="revenue-statistics-body">

                  <div class="row">
                    <div class="col-md-12">
                      <div id="pie"></div>
                    </div>
                    <div class="col-md-12 mt-5" style="display: flex;justify-content: center;">
                      <ul data-pie-id="pie" style="list-style: disc;">
                        <?php if ($data->revenue_pie_chart_data->total_vehicle_type_fee == 0 && $data->revenue_pie_chart_data->total_pickup_fee == 0 && $data->revenue_pie_chart_data->total_return_fee == 0 && $data->revenue_pie_chart_data->total_extra_service_fee == 0 && $data->revenue_pie_chart_data->total_insurance_fee == 0 && $data->revenue_pie_chart_data->total_license_fee == 0) { ?>
                          <li data-value="100" data-text="{{percent}}" class="ml-5">No Data</li>
                        <?php } else { ?>
                          <li data-value="<?php echo $data->revenue_pie_chart_data->total_vehicle_type_fee; ?>" data-text="{{percent}}" class="ml-5">Total Vehicle Fee </li>
                          <li data-value="<?php echo $data->revenue_pie_chart_data->total_pickup_fee; ?>" data-text="{{percent}}" class="ml-5">Total Pickup Fee </li>
                          <li data-value="<?php echo $data->revenue_pie_chart_data->total_return_fee; ?>" data-text="{{percent}}" class="ml-5">Total Return Fee</li>
                          <li data-value="<?php echo $data->revenue_pie_chart_data->total_extra_service_fee; ?>" data-text="{{percent}}" class="ml-5">Total Extra Service Fee </li>
                          <li data-value="<?php echo $data->revenue_pie_chart_data->total_insurance_fee; ?>" data-text="{{percent}}" class="ml-5">Total Insurance Fee</li>
                          <li data-value="<?php echo $data->revenue_pie_chart_data->total_license_fee; ?>" data-text="{{percent}}" class="ml-5">Total License Fee </li>
                        <?php } ?>
                      </ul>
                    </div>
                  </div>

                </div>

              </div>


            </div>
          </div>

        </div>


        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <h5 style="font-weight: bold;">Vehicle Owner Invoices</h5><br>
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Invoice No</th>
                        <th>Booking No</th>
                        <th>Owner</th>
                        <th>Agreement No</th>
                        <th>Pickup Date</th>
                        <th>Return Date</th>
                        <th>Vehicle No</th>
                        <th>Milage Start</th>
                        <th>Milage End</th>
                        <th>Payment Status</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--  END CONTENT PART  -->
  </div>
  <!-- END MAIN CONTAINER -->

  <!--  BEGIN FOOTER  -->
  <?php require 'includes/footer.php'; ?> 
  <!--  END FOOTER  -->

  <!-- BEGIN GLOBAL MANDATORY STYLES -->
  <?php require 'includes/bottom_footer.php'; ?> 
  <!-- END GLOBAL MANDATORY STYLES -->   

  <!-- BEGIN PAGE LEVEL SCRIPTS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/vendor/dependencies.js"></script>
  <script src="<?php echo base_url(); ?>assets/dist/js/pizza.js"></script>
  <script>
    $(window).on('load', function(){
      Pizza.init();
      $(document).foundation();
    });

    setTimeout(function() {
      $('.alert-messages').slideUp();
    }, 3000);


    $('#multi-column-ordering').DataTable({
      "language": {
        "paginate": { "previous": "<i class='flaticon-arrow-left-1'></i>", "next": "<i class='flaticon-arrow-right'></i>" },
        "info": "Showing page _PAGE_ of _PAGES_"
      },
      columnDefs: [ {
        targets: [ 0 ],
        orderData: [ 0, 1 ]
      }, {
        targets: [ 1 ],
        orderData: [ 1, 0 ]
      }, {
        targets: [ 4 ],
        orderData: [ 4, 0 ]
      } ]
    });


    getvehicleOwnerInvoices();

    function getvehicleOwnerInvoices() {
     $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>welcome/getvehicleOwnerInvoices',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {

       $('#table_div').html(res);
       $(".cs-overlay").css('visibility', 'hidden');

     }
   });
   }

 </script>
 <!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>