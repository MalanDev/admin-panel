<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
<title>Tuk Tuk Rental </title>
<link rel="icon" type="image/x-icon" href="<?php  echo base_url(); ?>assets/assets/img/logo-3.png"/>
<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<link href="<?php  echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>assets/assets/css/plugins.css" rel="stylesheet" type="text/css" />
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="<?php  echo base_url(); ?>assets/assets/css/support-chat.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>assets/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.css" rel="stylesheet" type="text/css" />
<link href="<?php  echo base_url(); ?>assets/plugins/charts/chartist/chartist.css" rel="stylesheet" type="text/css">
<link href="<?php  echo base_url(); ?>assets/assets/css/car-dashboard/style.css" rel="stylesheet" type="text/css" />    
<link href="<?php  echo base_url(); ?>assets/assets/css/accounting-dashboard/style.css" rel="stylesheet" type="text/css" />

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
<link href="<?php  echo base_url(); ?>assets/plugins/dropzone/dropzone.min.css" rel="stylesheet" type="text/css" />


<!--  BEGIN CUSTOM STYLE FILE  -->
<style>
	.form-control {
		border: 1px solid #ccc;
		color: #888ea8;
		font-size: 15px;
	}
	code { color: #3862f5; }
	.form-control:disabled, .form-control[readonly] { background-color: #f1f3f9; border-color: #f1f3f1; }
	.btn-primary.disabled, .btn-primary:disabled { background-color: #3862f5; border-color: #3862f5; }
	label { color: #3b3f5c; margin-bottom: 10px; }
	.form-control::-webkit-input-placeholder { color: #888ea8; font-size: 15px; }
	.form-control::-ms-input-placeholder { color: #888ea8; font-size: 15px; }
	.form-control::-moz-placeholder { color: #888ea8; font-size: 15px; }
	.form-control:focus { border-color: #3862f5; }
	.input-group-text {
		background-color: #f3f4f7;
		border-color: #e9ecef;
		color: #6156ce;
	}
	select.form-control {
		display: inline-block;
		width: 100%;
		height: calc(2.25rem + 2px);
		vertical-align: middle;
		background: #fff url(<?php echo base_url(); ?>assets/assets/img/arrow-down.png) no-repeat right .75rem center;
		background-size: 13px 14px;
		-webkit-appearance: none;
		-moz-appearance: none;
		appearance: none;
	}
	select.form-control::-ms-expand { display: none; }

	.error {
		color: red;
	}

	.select2 {
		width: 100%;
	}

	.cs-overlay {
		opacity: 0.7;
		display: flex;
		align-items: center;
		justify-content: center;
		visibility: hidden;
		background-color: #fff;
		z-index: 10000;
	}
</style>
  <!--  BEGIN CUSTOM STYLE FILE  -->