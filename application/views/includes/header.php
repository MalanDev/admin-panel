<header class="tabMobileView header navbar fixed-top d-lg-none">
  <div class="nav-toggle">
    <a href="javascript:void(0);" class="nav-link sidebarCollapse" data-placement="bottom">
      <i class="flaticon-menu-line-2"></i>
    </a>
    <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/assets/img/logo-3.png" class="img-fluid" alt="logo"></a>
  </div>
</header>
<header class="header navbar fixed-top navbar-expand-sm">
  <!-- <a href="javascript:void(0);" class="sidebarCollapse d-none d-lg-block" data-placement="bottom"><i class="flaticon-menu-line-2"></i></a> -->

  <ul class="navbar-nav flex-row ml-lg-auto">

    <li class="nav-item dropdown user-profile-dropdown ml-lg-0 mr-lg-2 ml-3 order-lg-0 order-1">
      <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="userProfileDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="flaticon-user-12"></span>
      </a>
      <div class="dropdown-menu  position-absolute" aria-labelledby="userProfileDropdown">
        <a class="dropdown-item" href="<?php echo base_url(); ?>login/logout">
          <i class="mr-1 flaticon-power-button"></i> <span>Log Out</span>
        </a>
      </div>
    </li>

  </ul>
</header>