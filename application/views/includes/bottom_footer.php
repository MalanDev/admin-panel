<script src="<?php echo base_url(); ?>assets/bootstrap/js/popper.min.js"></script>
<script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/app.js"></script>
<script>
    $(document).ready(function() {
        App.init();
    });
</script>
<script src="<?php echo base_url(); ?>assets/assets/js/custom.js"></script>
<!-- END GLOBAL MANDATORY SCRIPTS -->

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/charts/chartist/chartist.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/maps/vector/jvector/jquery-jvectormap-2.0.3.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/maps/vector/jvector/worldmap_script/jquery-jvectormap-world-mill-en.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/calendar/pignose/moment.latest.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/calendar/pignose/pignose.calendar.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/progressbar/progressbar.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/car-dashboard/car-custom.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/accounting-dashboard/accounting-custom.js"></script> 
<script src="<?php echo base_url(); ?>assets/assets/js/support-chat.js"></script>

<!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/dropzone/dropzone.min.js"></script>


<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.min.js"></script>
