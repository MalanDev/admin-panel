<div class="sidebar-wrapper sidebar-theme">

 <div id="dismiss" class="d-lg-none"><i class="flaticon-cancel-12"></i></div>

 <nav id="sidebar">

  <ul class="navbar-nav theme-brand flex-row  d-none d-lg-flex justify-content-center">
    <li class="nav-item d-flex">
      <a href="<?php echo base_url(); ?>" class="navbar-brand">
        <img src="<?php  echo base_url(); ?>assets/assets/img/logo-3.png" class="img-fluid" alt="logo">
      </a>
    </li>
  </ul>


  <ul class="list-unstyled menu-categories" id="accordionExample">
    <li class="menu">
      <a href="<?php echo base_url(); ?>" class="dropdown-toggle">
        <div class="">
          <i class="flaticon-computer-6 ml-3"></i>
          <span>Dashboard</span>
        </div>
      </a>
    </li>

    <?php 
    $master_urls = ['vehicle-types', 'pickup-locations', 'return-locations', 'service-charges', 'extra-services', 'coupons', 'vehicle-owner-registration', 'vehicle-registration'];
    ?>

    <?php 
    $booking_urls = ['check-available-vehicle', 'view-booking', 'create-booking'];
    ?>

    <?php 
    $agreement_urls = ['create-agreement', 'view-agreement'];
    ?>

    <?php 
    $payment_urls = ['customer-invoice','vehicle-owner-invoice'];
    ?>

    <?php 
    $report_urls = ['vehicle-owner-details','vehicle-details', 'check-available-vehicles', 'view-bookings', 'view-agreements', 'customer-invoices', 'vehicle-owner-invoices'];
    ?>

    <li class="menu">
      <a href="#master-data" data-toggle="collapse" aria-expanded="<?php echo in_array($this->uri->segment(1), $master_urls) ? 'true' : 'false'; ?>" class="dropdown-toggle">
        <div class="">
          <i class="flaticon-edit-2"></i>
          <span>Master Data</span>
        </div>
        <div>
          <i class="flaticon-right-arrow"></i>
        </div>
      </a>
      <ul class="collapse submenu list-unstyled <?php echo in_array($this->uri->segment(1), $master_urls) ? 'show' : ''; ?>" id="master-data"  data-parent="#accordionExample">
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-types') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-types">Vahicle Types</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'pickup-locations') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>pickup-locations">Pickup Locations</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'return-locations') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>return-locations">Return Locations</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'service-charges') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>service-charges">Service Charges</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'extra-services') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>extra-services">Extra Services</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'coupons') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>coupons">Coupons</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-owner-registration') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-owner-registration">Vehicle Owner Registraion</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-registration') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-registration">Vehicle Registraion</a>
        </li>
      </ul>
    </li>

    <li class="menu">
      <a href="#booking-data" data-toggle="collapse" aria-expanded="<?php echo in_array($this->uri->segment(1), $booking_urls) ? 'true' : 'false'; ?>" class="dropdown-toggle">
        <div class="">
          <i class="flaticon-calendar-1"></i>
          <span>Bookings</span>
        </div>
        <div>
          <i class="flaticon-right-arrow"></i>
        </div>
      </a>
      <ul class="collapse submenu list-unstyled <?php echo in_array($this->uri->segment(1), $booking_urls) ? 'show' : ''; ?>" id="booking-data"  data-parent="#accordionExample">
        <li class="<?php echo ($this->uri->segment(1) == 'create-booking') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>create-booking">Create Booking</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'view-booking') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>view-booking">View Bookings</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'check-available-vehicle') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>check-available-vehicle">Check Availability Vehicles</a>
        </li>
      </ul>
    </li>

    <li class="menu">
      <a href="#agreement-data" data-toggle="collapse" aria-expanded="<?php echo in_array($this->uri->segment(1), $agreement_urls) ? 'true' : 'false'; ?>" class="dropdown-toggle">
        <div class="">
          <i class="flaticon-copy-line"></i>
          <span>Agreements</span>
        </div>
        <div>
          <i class="flaticon-right-arrow"></i>
        </div>
      </a>
      <ul class="collapse submenu list-unstyled <?php echo in_array($this->uri->segment(1), $agreement_urls) ? 'show' : ''; ?>" id="agreement-data"  data-parent="#accordionExample">
        <li class="<?php echo ($this->uri->segment(1) == 'create-agreement') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>create-agreement">Create Agreement</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'view-agreement') ? 'active' : ''; ?>">
          <a href="view-agreement">View Agreement</a>
        </li>
      </ul>
    </li>

    <li class="menu">
      <a href="#payment-data" data-toggle="collapse" aria-expanded="<?php echo in_array($this->uri->segment(1), $payment_urls) ? 'true' : 'false'; ?>" class="dropdown-toggle">
        <div class="">
          <i class="flaticon-money"></i>
          <span>Payments</span>
        </div>
        <div>
          <i class="flaticon-right-arrow"></i>
        </div>
      </a>
      <ul class="collapse submenu list-unstyled  <?php echo in_array($this->uri->segment(1), $payment_urls) ? 'show' : ''; ?>" id="payment-data"  data-parent="#accordionExample">
        <li class="<?php echo ($this->uri->segment(1) == 'customer-invoice') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>customer-invoice">Customer Invoice</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-owner-invoice') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-owner-invoice">Vehicle Owner Invoice</a>
        </li>
      </ul>
    </li>

    <li class="menu">
      <a href="#report-data" data-toggle="collapse" aria-expanded="<?php echo in_array($this->uri->segment(1), $report_urls) ? 'true' : 'false'; ?>" class="dropdown-toggle">
        <div class="">
          <i class="flaticon-money"></i>
          <span>Reports</span>
        </div>
        <div>
          <i class="flaticon-right-arrow"></i>
        </div>
      </a>
      <ul class="collapse submenu list-unstyled  <?php echo in_array($this->uri->segment(1), $report_urls) ? 'show' : ''; ?>" id="report-data"  data-parent="#accordionExample">
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-owner-details') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-owner-details">Vehicle Owner Details</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-details') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-details">Vehicle Details</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'check-available-vehicles') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>check-available-vehicles">Check Available Vehicles</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'view-bookings') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>view-bookings">View Bookings</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'view-agreements') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>view-agreements">View Agreements</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'customer-invoices') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>customer-invoices">Customer Invoices</a>
        </li>
        <li class="<?php echo ($this->uri->segment(1) == 'vehicle-owner-invoices') ? 'active' : ''; ?>">
          <a href="<?php echo base_url(); ?>vehicle-owner-invoices">Vehicle Owner Invoices</a>
        </li>
      </ul>
    </li>

  </ul>
</nav>

</div>