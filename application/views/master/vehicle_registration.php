<!DOCTYPE html>
<html lang="en">
<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

  <style type="text/css">
    #owner-error{
      position: absolute;
      bottom: 50px;
      left: 20px;
    }
  </style>
</head>
<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?> 
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?> 

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Vehicle Registration</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Master Data</li>
                <li>Vehicle Registration</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form id="vehicle_register_form" action="<?php echo base_url(); ?>master/saveVehicleRegistration" method="post" enctype="multipart/form-data"> 

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Vehicle Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlSelect1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicletype">
                      </select>
                      <input type="hidden" id="vehicle_id" name="vehicle_id">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlSelect1">Owner</label>
                      <select class="form-control-rounded form-control search_dropdown" id="owner" name="owner">
                        <option value="" disabled selected hidden>Choose Owner</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <label for="registerNo">Registration No</label>
                      <input type="text" class="form-control-rounded form-control" id="registerNo" name="registerNo" placeholder="Enter Registration No">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <label for="exampleFormControlFile1">Image</label>
                      <input type="file" class="form-control-file form-control-file-rounded" id="registerNo_image" name="registerNo_image">
                      <input type="hidden" name="registerNo_hidden" id="registerNo_hidden">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <img src="https://via.placeholder.com/100x100" id="registerNo_img" width="150px" class="img">
                    </div>
                  </div>
                </div>
              </div>
            </div>  



            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4  class="pb-0">Revenue License Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="licenseNo">License No</label>
                      <input type="text" class="form-control-rounded form-control" id="licenseNo" name="licenseNo" placeholder="Enter License No">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Expiry Date</label>
                      <input type="date" class="form-control-rounded form-control" id="license_expiry" name="license_expiry" value="<?php echo date("Y-m-d"); ?>">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <label for="exampleFormControlFile1">Image</label>
                      <input type="file" class="form-control-file form-control-file-rounded" id="license_image" name="license_image">
                      <input type="hidden" name="license_hidden" id="license_hidden">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <img src="https://via.placeholder.com/100x100" id="license_img" width="150px" class="img">
                    </div>
                  </div>
                </div>
              </div>
            </div>   



            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4  class="pb-0">Smoke Test Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="testNo">Test No</label>
                      <input type="text" class="form-control-rounded form-control" id="testNo" name="testNo" placeholder="Enter Test No">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Expiry Date</label>
                      <input type="date" class="form-control-rounded form-control" id="smoke_expiry" name="smoke_expiry" value="<?php echo date("Y-m-d"); ?>">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <label for="exampleFormControlFile1">Image</label>
                      <input type="file" class="form-control-file form-control-file-rounded" id="smoke_image" name="smoke_image">
                      <input type="hidden" name="smoke_hidden" id="smoke_hidden">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <img src="https://via.placeholder.com/100x100" id="smoke_img" width="150px" class="img">
                    </div>
                  </div>
                </div>
              </div>
            </div> 


            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4  class="pb-0">Insurance Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="insuranceNo">Insurance No</label>
                      <input type="text" class="form-control-rounded form-control" id="insuranceNo" name="insuranceNo" placeholder="Enter Insurance No">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Expiry Date</label>
                      <input type="date" class="form-control-rounded form-control" id="insurance_expiry" name="insurance_expiry" value="<?php echo date("Y-m-d"); ?>">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <label for="exampleFormControlFile1">Image</label>
                      <input type="file" class="form-control-file form-control-file-rounded" id="insurance_image" name="insurance_image">
                      <input type="hidden" name="insurance_hidden" id="insurance_hidden">
                    </div>
                    <div class="form-group mb-4 col-md-2">
                      <img src="https://via.placeholder.com/100x100" id="insurance_img" width="150px" class="img">
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">

                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Rate Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Per Day Rate</label>
                      <input type="number" class="form-control-rounded form-control" id="per_day" name="per_day" placeholder="Enter Per Day Rate">
                    </div>
                  </div>
                </div>

              </div>
            </div>

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                   <div class="form-group mt-2 pt-2 col-md-3 offset-md-9 text-right">
                    <input type="submit" id="submit_button" name="time" class="btn btn-button-7 btn-rounded">
                    <input type="reset" id="reset_button" name="time" class="btn btn-button-6 btn-rounded" value="Cancel">
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </form> 




      <div class="row" id="cancel-row">
        <div class="col-xl-12 col-lg-12 col-sm-12">
          <div class="statbox widget box box-shadow">
            <div class="widget-content widget-content-area">
              <div class="table-responsive mb-4">
                <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Vehicle Type</th>
                      <th>Register No</th>
                      <th>Per Day Rate</th>
                      <th>Rev. License Expiry Date</th>
                      <th>Smoke Test Expiry Date</th>
                      <th>Insurance Expiry Date</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($data as $vehicle) { ?>
                      <tr>
                        <td><?php echo $vehicle->vehicle_type->type; ?></td>
                        <td><?php echo $vehicle->registration_no; ?></td>
                        <td><?php echo $vehicle->per_day_rate; ?></td>
                        <td><?php echo $vehicle->revenue_license_exp_date; ?></td>
                        <td><?php echo $vehicle->smoke_test_exp_date; ?></td>
                        <td><?php echo $vehicle->insurance_exp_date; ?></td>
                        <td class="text-center">
                         <div class="vc-toggle-container">
                          <label class="vc-switch mt-2">
                            <input type="checkbox" class="vc-switch-input" id="status_<?php echo $vehicle->id; ?>" <?php if($vehicle->is_active == 1) echo "checked"; ?> onchange="changeStatus('<?php echo $vehicle->id; ?>');" />
                            <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                            <span class="vc-handle"></span>
                          </label>
                        </div>
                      </td>
                      <td class="text-center">
                        <button class="btn btn-outline-secondary btn-rounded" onclick="editVehicleRegistration('<?php echo $vehicle->id; ?>')">Edit</button>
                        <button class="btn btn-outline-danger btn-rounded" onclick="checkDelete('<?php echo $vehicle->id; ?>')">Delete</button>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?> 
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?> 
<!-- END GLOBAL MANDATORY STYLES -->   

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script>
  setTimeout(function() {
    $('.alert-messages').slideUp();
  }, 3000);

  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": { "previous": "<i class='flaticon-arrow-left-1'></i>", "next": "<i class='flaticon-arrow-right'></i>" },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 4 ],
      orderData: [ 4, 0 ]
    } ]
  });


  $(document).ready(function() {
    getAllVehicleTypes();

    $("#vehicle_register_form").validate({

      rules: {
        vehicletype : {
          required: true,
        },
        owner : {
          required: true,
        },
        registerNo : {
          required: true,
        },
        licenseNo : {
          required: true,
        },
        testNo: {
          required: true,
        },
        insuranceNo: {
          required: true,
        },
        license_expiry : {
          required: true,
        },
        license_image : {
          required: function(element){
            return $("#license_hidden").val()=="";
          },
        },
        smoke_expiry : {
          required: true,
        },
        smoke_image : {
          required: function(element){
            return $("#smoke_hidden").val()=="";
          },
        },
        insurance_expiry : {
          required: true,
        },
        insurance_image : {
          required: function(element){
            return $("#insurance_hidden").val()=="";
          },
        },
        per_day : {
          required: true,
        },
      },
      messages : {
        vehicletype: {
          required: "Please select Vehicle Type",
        },
        owner: {
          required: "Please select Owner",
        },
        registerNo: {
          required: "Please enter Registration No",
        },
        licenseNo: {
          required: "Please enter License No",
        },
        testNo: {
          required: "Please enter Test No",
        },
        insuranceNo: {
          required: "Please enter Insurance No",
        },
        license_expiry: {
          required: "Please select License Expiry Date",
        },
        license_image: {
          required: "Please Upload License Image",
        },
        smoke_expiry: {
          required: "Please select Smoke Test Expiry Date",
        },
        smoke_image: {
          required: "Please Upload Smoke Test Image",
        },
        insurance_expiry: {
          required: "Please select Insurance Expiry Date",
        },
        insurance_image: {
          required: "Please Upload Insurance Image",
        },
        per_day: {
          required: "Please enter Per Day Rate",
        },
      }
    });
  });


  var today = new Date();
  var dd = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();

  expiry_date = yyyy + '-' + mm + '-' + dd;
  $('#license_expiry').attr('min',expiry_date);
  $('#smoke_expiry').attr('min',expiry_date);
  $('#insurance_expiry').attr('min',expiry_date);


  function changeStatus(id) {
    let status = 0;
    if ($('#status_'+id).is(':checked')) {
      status = 1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/vehicleRegistrationStatus/'+id+'/'+status,
      success:function () {}
    });
  }

  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
          getAllOwners();
        }        
      }
    });
  }

  function getAllOwners() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllOwners',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#owner').append($('<option>', { 
              value: item.id,
              text : item.first_name 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  function checkDelete(id) {
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure, do you want to delete this?',
      type: 'red',
      buttons: {
        confirm: function(){
          location.href = '<?php echo base_url(); ?>master/deleteVehicleRegistration/'+id;
        },
        cancel : {

        }
      }
    });
  }

  function editVehicleRegistration(id) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getVehicleRegistrationById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#vehicletype option[value="' + result.data.vehicle_type_id +'"]').prop("selected", true);
          $('#owner').val(result.data.vehicle_owner_id).trigger('change');
          $('#registerNo').val(result.data.registration_no);
          $("#registerNo_img").attr("src",result.data.vehicle_image);
          $("#registerNo_hidden").val(result.data.vehicle_image);
          $('#licenseNo').val(result.data.revenue_license_no);
          $('#license_expiry').val(result.data.revenue_license_exp_date);
          $('#license_expiry').attr('min',expiry_date);
          $("#license_img").attr("src",result.data.revenue_license_image);
          $("#license_hidden").val(result.data.revenue_license_image);
          $('#testNo').val(result.data.smoke_test_no);   
          $('#smoke_expiry').val(result.data.smoke_test_exp_date);
          $('#smoke_expiry').attr('min',expiry_date);
          $("#smoke_img").attr("src",result.data.smoke_test_image);
          $("#smoke_hidden").val(result.data.smoke_test_image); 
          $('#insuranceNo').val(result.data.insurance_no);   
          $('#insurance_expiry').val(result.data.insurance_exp_date);
          $('#insurance_expiry').attr('min',expiry_date);
          $("#insurance_img").attr("src",result.data.insurance_image);
          $("#insurance_hidden").val(result.data.insurance_image);     
          $('#per_day').val(result.data.per_day_rate);
          $('#vehicle_id').val(result.data.id); 

          $("#submit_button").val("Update");
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  $('#reset_button').on('click', function() {
    $("#submit_button").val("Submit");
    $('#servicecharge_id').val("");   
    $("form").validate().resetForm();
    $('.img').attr('src', 'https://via.placeholder.com/100x100');
  });

  $("select").on("select2:close", function (e) {  
    $(this).valid(); 
  });


  registerNo_image.onchange = evt => {
    const [file] = registerNo_image.files
    if (file) {
     registerNo_img.src = URL.createObjectURL(file)
   }
 }

 license_image.onchange = evt => {
  const [file2] = license_image.files
  if (file2) {
    license_img.src = URL.createObjectURL(file2)
  }
}

smoke_image.onchange = evt => {
  const [file3] = smoke_image.files
  if (file3) {
    smoke_img.src = URL.createObjectURL(file3)
  }
}

insurance_image.onchange = evt => {
  const [file4] = insurance_image.files
  if (file4) {
    insurance_img.src = URL.createObjectURL(file4)
  }
}
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>