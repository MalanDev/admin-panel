<!DOCTYPE html>
<html lang="en">
<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

</head>
<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?> 
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?> 

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Coupons</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Master Data</li>
                <li>Coupons</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form id="coupon_form" action="<?php echo base_url(); ?>master/saveCoupon" method="post"> 

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicletype">
                      </select>

                      <input type="hidden" name="coupon_id" id="coupon_id">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="couponName">Coupon Name</label>
                      <input type="text" class="form-control-rounded form-control" id="couponName" name="couponName" required placeholder="Enter Coupon Name">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="couponCode">Coupon Code</label>
                      <input type="text" class="form-control-rounded form-control" id="couponCode" name="couponCode" required placeholder="Enter Coupon Code">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate" name="fromDate" value="<?php echo date("Y-m-d"); ?>" onchange="changeDate()">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate" name="toDate">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="discount">Discount</label>&nbsp;&nbsp;
                      <input type="radio" name="discount_select" value="1" id="price_discount"> €  &nbsp;&nbsp;
                      <input type="radio" name="discount_select" value="2" id="percentage_discount" checked> %
                      <input type="number" class="form-control-rounded form-control" id="discount" name="discount" placeholder="Enter Discount">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="minAmount">Minimum Amount (€)</label>
                      <input type="number" class="form-control-rounded form-control" id="minAmount" name="minAmount" placeholder="Enter Minimum Amount">
                    </div>
                    <div class="form-group mt-4 col-md-3 text-right">
                      <input type="submit" class="btn btn-button-7 btn-rounded">
                      <input type="reset" id="reset_button" class="btn btn-button-6 btn-rounded" value="Cancel">
                    </div>
                  </div>
                </div>
              </div>
            </div>  

          </div>

        </form> 


        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Coupon Name</th>
                        <th>Coupon Code</th>
                        <th>Vehicle Type</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Discount</th>
                        <th>Min Amount (€)</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data as $coupon) { ?>
                        <tr>
                          <td><?php echo $coupon->coupon_name; ?></td>
                          <td><?php echo $coupon->coupon_code; ?></td>
                          <td><?php echo $coupon->vehicle_type->type; ?></td>
                          <td><?php echo $coupon->from_date; ?></td>
                          <td><?php echo $coupon->to_date; ?></td>
                          <td><?php echo $coupon->discount; ?><?php echo ($coupon->discount_type == 1) ? '' : '%'; ?></td>
                          <td><?php echo $coupon->minimum_amount; ?></td>
                          <td class="text-center">
                            <div class="vc-toggle-container">
                              <label class="vc-switch mt-2">
                                <input type="checkbox" class="vc-switch-input" id="status_<?php echo $coupon->id; ?>" <?php if($coupon->is_active == 1) echo "checked"; ?> onchange="changeStatus('<?php echo $coupon->id; ?>');" />
                                <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                                <span class="vc-handle"></span>
                              </label>
                            </div>
                          </td>
                          <td class="text-center">
                            <button class="btn btn-outline-secondary btn-rounded" onclick="editCouon('<?php echo $coupon->id; ?>')">Edit</button>
                            <button class="btn btn-outline-info btn-rounded" onclick="checkDelete('<?php echo $coupon->id; ?>')">Delete</button></td>
                          </tr>
                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!--  END CONTENT PART  -->
    </div>
    <!-- END MAIN CONTAINER -->

    <!--  BEGIN FOOTER  -->
    <?php require __DIR__.'../../includes/footer.php'; ?> 
    <!--  END FOOTER  -->

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <?php require __DIR__.'../../includes/bottom_footer.php'; ?> 
    <!-- END GLOBAL MANDATORY STYLES -->   

    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
    <script>

      setTimeout(function() {
        $('.alert-messages').slideUp();
      }, 3000);

      $('#multi-column-ordering').DataTable({
        "language": {
          "paginate": { "previous": "<i class='flaticon-arrow-left-1'></i>", "next": "<i class='flaticon-arrow-right'></i>" },
          "info": "Showing page _PAGE_ of _PAGES_"
        },
        columnDefs: [ {
          targets: [ 0 ],
          orderData: [ 0, 1 ]
        }, {
          targets: [ 1 ],
          orderData: [ 1, 0 ]
        }, {
          targets: [ 4 ],
          orderData: [ 4, 0 ]
        } ]
      });


      getAllVehicleTypes();


      $(document).ready(function() {
        $("#coupon_form").validate({

          rules: {
            couponName : {
              required: true,
            },
            couponCode : {
              required: true,
            },
            minAmount : {
              min:0,
            },
            discount : {
              min:0,
              max: function (fg) {
                if($('input[name=discount_select]:checked').val() == '2') {
                  return 100;
                } else {
                  return 99999999;
                }             
              }
            },

          },
          messages : {
            couponName: {
              required: "Please enter Coupon Name",
            },
            couponCode: {
              required: "Please enter Coupon Code",
            },
            minAmount : {
              min:"Minimum amount should be 0",
            },
            discount : {
              min:"Minimum discount should be 0",
              max:"Maximum discount should be 100"
            },
          }
        });
      });


      function getAllVehicleTypes() {
        $.ajax({
          type:'post',
          url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
          beforeSend: function(){
            $(".cs-overlay").css('visibility', 'visible');
          },
          success:function (res) {
            if (!res) {
              location.reload();  
            } else {
              let result = JSON.parse(res);
              $.each(result.data, function (i, item) {
                $('#vehicletype').append($('<option>', { 
                  value: item.id,
                  text : item.type 
                }));
              });
              $(".cs-overlay").css('visibility', 'hidden');
            }        
          }
        });
      }


      var today = new Date();
      var dd = String(today.getDate()).padStart(2, '0');
      var mm = String(today.getMonth() + 1).padStart(2, '0');
      var yyyy = today.getFullYear();

      from_date = yyyy + '-' + mm + '-' + dd;
      var dd = String(today.getDate()+1).padStart(2, '0');
      to_date = yyyy + '-' + mm + '-' + dd;

      $('#fromDate').attr('min',from_date);
      $('#toDate').attr('min',to_date);
      $('#toDate').val(to_date);


      function changeDate() {
        var from_today = new Date($('#fromDate').val());
        var dd = String(from_today.getDate() + 1).padStart(2, '0');
        var mm = String(from_today.getMonth() + 1).padStart(2, '0');
        var yyyy = from_today.getFullYear();

        from_today = yyyy + '-' + mm + '-' + dd;
        $('#toDate').attr('min',from_today);
      }

      $('#reset_button').on('click', function() {
        $("#submit_button").val("Submit");
        $('#coupon_id').val("");   
        $("form").validate().resetForm();
      });

      function checkDelete(id) {
        $.confirm({
          title: 'Confirm!',
          content: 'Are you sure, do you want to delete this?',
          type: 'red',
          buttons: {
            confirm: function(){
              location.href = '<?php echo base_url(); ?>master/deleteCoupon/'+id;
            },
            cancel : {

            }
          }
        });
      }

      function changeStatus(id) {
        let status = 0;
        if ($('#status_'+id).is(':checked')) {
          status = 1;
        }

        $.ajax({
          type:'post',
          url:'<?php echo base_url(); ?>master/couponStatus/'+id+'/'+status,
          success:function () {}
        });
      }

      function editCouon(id) {
        $.ajax({
          type:'post',
          url:'<?php echo base_url(); ?>master/getCouponById/'+id,
          beforeSend: function(){
            $(".cs-overlay").css('visibility', 'visible');
          },
          success:function (res) {
            if (!res) {
              location.reload();  
            } else {
              let result = JSON.parse(res);
              $('#vehicletype option[value="' + result.data.vehicle_type_id +'"]').prop("selected", true);
              $('#coupon_id').val(result.data.id);
              $('#couponName').val(result.data.coupon_name);
              $('#couponCode').val(result.data.coupon_code);  
              $('#fromDate').val(result.data.from_date);  
              $('#toDate').val(result.data.to_date);    
              $('#discount').val(result.data.discount);  
              $('#minAmount').val(result.data.minimum_amount); 
              if (result.data.discount_type == 1) {
                $('#price_discount').prop("checked", true);
              } else {
                $('#percentage_discount').prop("checked", true);
              }
              $("#submit_button").val("Update");
              $(".cs-overlay").css('visibility', 'hidden');
            }        
          }
        });
      }
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->
  </body>
  </html>