<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Service Charges</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Master Data</li>
                <li>Service Charges</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form id="service_charges_form" action="<?php echo base_url(); ?>master/saveServiceCharge" method="post">

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicletype">
                      </select>
                      <input type="hidden" id="servicecharge_id" name="servicecharge_id">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="drivingLicense">Driving License Fee (€)</label>
                      <input type="number" class="form-control-rounded form-control" id="drivingLicenseFee" name="drivingLicenseFee" required placeholder="Enter Driving License Fee">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="Dduration">Driving License Duration (Days)</label>
                      <input type="number" class="form-control-rounded form-control" id="Dduration" name="Dduration" required placeholder="Enter Duration">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="insuranceFee">Insurance Fee (€)</label>
                      <input type="number" class="form-control-rounded form-control" id="insuranceFee" name="insuranceFee" required placeholder="Enter Insurance Fee">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="Iduration">Insurance Duration (Days)</label>
                      <input type="number" class="form-control-rounded form-control" id="Iduration" name="Iduration" required placeholder="Enter Duration">
                    </div>                    
                    <div class="form-group col-md-3 offset-md-6 text-right mt-4">
                      <input type="submit" id="submit_button" name="time" class="btn btn-button-7 btn-rounded">
                      <input type="reset" id="reset_button" name="time" class="btn btn-button-6 btn-rounded" value="Cancel">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </form>


        <!--table-->
        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering"
                  class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Vehicle Type</th>
                      <th>Driving License Fee (€)</th>
                      <th>Driving License Duration (Days)</th>
                      <th>Insurance Fee (€)</th>
                      <th>Insurance Duration (Days)</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($data as $service_charge) { ?>
                      <tr>
                       <td><?php echo $service_charge->vehicle_type->type; ?></td>
                       <td><?php echo $service_charge->driving_license_fee; ?></td>
                       <td><?php echo $service_charge->driving_license_days; ?></td>
                       <td><?php echo $service_charge->insurance_fee; ?></td>
                       <td><?php echo $service_charge->insurance_days; ?></td>
                       <td class="text-center">
                         <div class="vc-toggle-container">
                          <label class="vc-switch mt-2">
                            <input type="checkbox" class="vc-switch-input" id="status_<?php echo $service_charge->id; ?>" <?php if($service_charge->is_active == 1) echo "checked"; ?> onchange="changeStatus('<?php echo $service_charge->id; ?>');" />
                            <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                            <span class="vc-handle"></span>
                          </label>
                        </div>
                      </td>
                      <td class="text-center">
                        <button class="btn btn-outline-secondary btn-rounded" onclick="editServiceCharge('<?php echo $service_charge->id; ?>')">Edit</button>
                        <button class="btn btn-outline-danger btn-rounded" onclick="checkDelete('<?php echo $service_charge->id; ?>')">Delete</button>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script>
  setTimeout(function() {
    $('.alert-messages').slideUp();
  }, 3000);

  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });



  $(document).ready(function() {
    getAllVehicleTypes();

    $("#service_charges_form").validate({

      rules: {
        vehicletype : {
          required: true,
        },
        drivingLicenseFee : {
          required: true,
        },
        Dduration : {
          required: true,
        },
        insuranceFee : {
          required: true,
        },
        Iduration : {
          required: true,
        },
      },
      messages : {
        vehicletype: {
          required: "Please select Vehicle Type",
        },
        drivingLicenseFee: {
          required: "Please enter Driving license fee",
        },
        Dduration: {
          required: "Please enter Duration",
        },
        insuranceFee: {
          required: "Please enter Insurance fee",
        },
        Iduration: {
          required: "Please enter Duration",
        },
      }
    });
  });

  function changeStatus(id) {
    let status = 0;
    if ($('#status_'+id).is(':checked')) {
      status = 1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/serviceChargeStatus/'+id+'/'+status,
      success:function () {}
    });
  }

  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  function checkDelete(id) {
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure, do you want to delete this?',
      type: 'red',
      buttons: {
        confirm: function(){
          location.href = '<?php echo base_url(); ?>master/deleteServiceCharge/'+id;
        },
        cancel : {

        }
      }
    });
  }

  function editServiceCharge(id) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getServiceChargeById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#vehicletype option[value="' + result.data.vehicle_type_id +'"]').prop("selected", true);
          $('#drivingLicenseFee').val(result.data.driving_license_fee);
          $('#Dduration').val(result.data.driving_license_days);
          $('#insuranceFee').val(result.data.insurance_fee);
          $('#Iduration').val(result.data.insurance_days);   
          $('#servicecharge_id').val(result.data.id);          
          $("#submit_button").val("Update");
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  $('#reset_button').on('click', function() {
    $("#submit_button").val("Submit");
    $('#servicecharge_id').val("");
    $("form").validate().resetForm();
  });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>