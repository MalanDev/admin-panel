<!DOCTYPE html>
<html lang="en">
<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/countrySelect.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/intlTelInput.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

  <style type="text/css">
    .country-select, .iti--separate-dial-code {
      width: 100%;
    }

    .iti__selected-flag {
      border-radius: 20px 0 0 20px;
      height: auto;
      padding-top: 8px;
    }

    .iti--separate-dial-code .iti__selected-flag {
      background-color: transparent;
    }

    .country-select.inside .flag-dropdown:hover .selected-flag {
      background-color: transparent;
    }

    #bank-error {
      position: absolute;
      bottom: 0px;
      left: 20px;
    }
  </style>
</head>
<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?> 
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?> 

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Vehicle Owner Registration</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Master Data</li>
                <li>Vehicle Owner Registration</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form id="vehicle_owner_register_form" action="<?php echo base_url(); ?>master/saveVehicleOwner" method="post"> 

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Personal Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="firstName">Frist Name</label>
                      <input type="text" class="form-control-rounded form-control" id="firstName" name="firstName" placeholder="Enter Frist Name">
                      <input type="hidden" id="vehicle_owner_id" name="vehicle_owner_id">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="lastName">Last Name</label>
                      <input type="text" class="form-control-rounded form-control" id="lastName" name="lastName" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="nic">NIC No</label>
                      <input type="text" class="form-control-rounded form-control" id="nic" name="nic" placeholder="Enter NIC No">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="drivingLNo">Driving License No</label>
                      <input type="text" class="form-control-rounded form-control" id="drivingLNo" name="drivingLNo" placeholder="Enter Driving License No">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="address">Address Line 1</label>
                      <input type="text" class="form-control-rounded form-control" id="address" name="address" placeholder="Enter Address Line 1">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Address Line 2</label>
                      <input type="text" class="form-control-rounded form-control" id="address2" name="address2" placeholder="Enter Address Line 2">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="city">City</label>
                      <input type="text" class="form-control-rounded form-control" id="city" name="city" placeholder="Enter City">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="country">Country</label><br>
                      <input id="country" name="country" class="form-control-rounded form-control" type="text" placeholder="Enter Country">

                      <div class="form-item" style="display:none;">
                        <input type="text" id="country_code" name="country_code" data-countrycodeinput="1" readonly="readonly" placeholder="Selected country code will appear here" />
                      </div>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="mobile1">Mobile no 1</label><br>
                      <input type="text" class="form-control-rounded form-control phone" id="mobile1" name="mobile1" placeholder="Enter Mobile no 1">
                      <input type="hidden" id="mobile_country_code" name="mobile_country_code" data-countrycodeinput="1" readonly="readonly" />
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="mobile2">Mobile no 2 (optional)</label>
                      <input type="text" class="form-control-rounded form-control phone2" id="mobile2" name="mobile2" placeholder="Enter Mobile no 2">
                      <input type="hidden" id="mobile_country_code2" name="mobile_country_code2" data-countrycodeinput="1" readonly="readonly" />
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="email">Email</label>
                      <input type="email" class="form-control-rounded form-control" id="email" name="email" placeholder="Enter Email">
                    </div>
                  </div>
                </div>
              </div>
            </div>  



            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Bank Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="accountNo">Account No</label>
                      <input type="number" class="form-control-rounded form-control" id="accountNo" name="accountNo" placeholder="Enter Account No">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="accountName">Account Name</label>
                      <input type="text" class="form-control-rounded form-control" id="accountName" name="accountName" placeholder="Enter Account Name">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="bank">Bank</label>
                      <select class="form-control-rounded form-control search_dropdown" id="bank" name="bank">
                        <option value="" disabled selected hidden>Select Bank</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="branch">Branch</label>
                      <input type="text" class="form-control-rounded form-control" id="branch" name="branch" placeholder="Enter Branch">
                    </div>
                  </div>
                </div>
              </div>
            </div>


            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                   <div class="form-group mt-2 pt-2 col-md-3 offset-md-9 text-right">
                    <input type="submit" id="submit_button" name="time" class="btn btn-button-7 btn-rounded">
                    <input type="reset" id="reset_button" name="time" class="btn btn-button-6 btn-rounded" value="Cancel">
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>

      </form> 


      <div class="row" id="cancel-row">
        <div class="col-xl-12 col-lg-12 col-sm-12">
          <div class="statbox widget box box-shadow">
            <div class="widget-content widget-content-area">
              <div class="table-responsive mb-4">
                <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                  <thead>
                    <tr>
                      <th>Name</th>
                      <th>NIC No</th>
                      <th>City</th>
                      <th>Mobile</th>
                      <th>Email</th>
                      <th>Account No</th>
                      <th>Bank</th>
                      <th class="text-center">Status</th>
                      <th class="text-center">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($data as $owner_details) { ?>
                      <tr>
                        <td><?php echo $owner_details->first_name.' '.$owner_details->last_name; ?></td>
                        <td><?php echo $owner_details->nic; ?></td>
                        <td><?php echo $owner_details->city; ?></td>
                        <td><?php echo $owner_details->mobile_1; ?></td>
                        <td><?php echo $owner_details->email; ?></td>
                        <td><?php echo $owner_details->vehicle_owner_bank_details->account_no; ?></td>
                        <td><?php echo $owner_details->vehicle_owner_bank_details->bank->bank_name; ?></td>
                        <td class="text-center">
                         <div class="vc-toggle-container">
                          <label class="vc-switch mt-2">
                            <input type="checkbox" class="vc-switch-input" id="status_<?php echo $owner_details->id; ?>" <?php if($owner_details->is_active == 1) echo "checked"; ?> onchange="changeStatus('<?php echo $owner_details->id; ?>');" />
                            <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                            <span class="vc-handle"></span>
                          </label>
                        </div>
                      </td>
                      <td class="text-center">
                        <button class="btn btn-outline-secondary btn-rounded" onclick="editVehicleOwner('<?php echo $owner_details->id; ?>')">Edit</button>
                        <button class="btn btn-outline-danger btn-rounded" onclick="checkDelete('<?php echo $owner_details->id; ?>')">Delete</button>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?> 
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?> 
<!-- END GLOBAL MANDATORY STYLES -->   

<!-- BEGIN PAGE LEVEL SCRIPTS -->

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/countrySelect.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/intlTelInput.js"></script>
<script>
  setTimeout(function() {
    $('.alert-messages').slideUp();
  }, 3000);

  $("#country").countrySelect({
    responsiveDropdown: true,
    preferredCountries: ['lk', 'us', 'gb']
  });

  var input = document.querySelector(".phone");
  var input2 = document.querySelector(".phone2");

  var iti = window.intlTelInput(input, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  $("#mobile_country_code").val(iti.getSelectedCountryData().iso2);

  var iti2 = window.intlTelInput(input2, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  $("#mobile_country_code2").val(iti2.getSelectedCountryData().iso2);

  $("#country").on('change', function(){
    var code = $("#country").countrySelect("getSelectedCountryData").iso2;
    iti.setCountry(code);
    iti2.setCountry(code);
    $("#mobile_country_code").val(code);
    $("#mobile_country_code2").val(code);
  });

  $("#mobile1").on('countrychange', function(e, countryData){
    $("#mobile_country_code").val(iti.getSelectedCountryData().iso2);
  });

  $("#mobile2").on('countrychange', function(e, countryData){
    $("#mobile_country_code2").val(iti2.getSelectedCountryData().iso2);
  });

</script>

<script type="text/javascript">
  $(document).ready(function() {
    getAllBanks();

    $("#vehicle_owner_register_form").validate({

      rules: {
        firstName : {
          required: true,
        },
        lastName : {
          required: true,
        },
        nic: {
          required: true,
        },
        address: {
          required: true,
        },
        city: {
          required: true,
        },
        mobile1: {
          number: true,
          required: true,
        },
        mobile2: {
          number: true,
        },
        email: {
          email: true,
        },
        accountNo: {
          required: true,
        },
        accountName: {
          required: true,
        },
        bank: {
          required: true,
        },
        branch: {
          required: true,
        },
      },
      messages : {
        firstName: {
          required: "Please enter First Name",
        },
        lastName: {
          required: "Please enter Last Name",
        },
        nic: {
          required: "Please enter NIC No",
        },
        address: {
          required: "Please enter Address",
        },
        city: {
          required: "Please enter City",
        },
        mobile1: {
          required: "Please enter Mobile",
        },
        accountNo: {
          required: "Please enter Account No",
        },
        accountName: {
          required: "Please enter Account Name",
        },
        bank: {
          required: "Please select Bank",
        },
        branch: {
          required: "Please enter Branch",
        },
      }
    });
  });
</script>
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": { "previous": "<i class='flaticon-arrow-left-1'></i>", "next": "<i class='flaticon-arrow-right'></i>" },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 4 ],
      orderData: [ 4, 0 ]
    } ]
  });


  function changeStatus(id) {
    let status = 0;
    if ($('#status_'+id).is(':checked')) {
      status = 1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/vehicleOwnerStatus/'+id+'/'+status,
      success:function () {}
    });
  }

  function checkDelete(id) {
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure, do you want to delete this?',
      type: 'red',
      buttons: {
        confirm: function(){
          location.href = '<?php echo base_url(); ?>master/deleteVehicleOwner/'+id;
        },
        cancel : {

        }
      }
    });
  }

  function editVehicleOwner(id) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getVehicleOwnerById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#firstName').val(result.data.first_name);
          $('#lastName').val(result.data.last_name);
          $('#nic').val(result.data.nic);
          $('#drivingLNo').val(result.data.driving_license_no);
          $('#address').val(result.data.address_line_1);
          $('#address2').val(result.data.address_line_2);
          $('#city').val(result.data.city);
          $('#country_code').val(result.data.country.country_code);
          $("#country").countrySelect("selectCountry", result.data.country.country_code);
          $('#mobile1').val(result.data.mobile_1);
          $('#mobile2').val(result.data.mobile_2);
          $('#mobile_country_code').val(result.data.mobile_1_country_code);
          $('#mobile_country_code2').val(result.data.mobile_2_country_code);
          iti.setCountry(result.data.mobile_1_country_code);
          iti2.setCountry(result.data.mobile_2_country_code);
          $('#email').val(result.data.email);
          $('#accountNo').val(result.data.vehicle_owner_bank_details.account_no);
          $('#accountName').val(result.data.vehicle_owner_bank_details.account_name);
          $('#bank option[value="' + result.data.vehicle_owner_bank_details.bank_id +'"]').prop("selected", true);
          $('#branch').val(result.data.vehicle_owner_bank_details.branch);
          $('#vehicle_owner_id').val(result.data.id);
          $("#submit_button").val("Update");
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  $('#reset_button').on('click', function() {
    $("#submit_button").val("Submit");
    $('#extraService_id').val("");   
    $("form").validate().resetForm();
  });

  function getAllBanks() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllBanks',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#bank').append($('<option>', { 
              value: item.id,
              text : item.bank_name 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

</script>

<!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>