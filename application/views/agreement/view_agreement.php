<!DOCTYPE html>
<html lang="en">

<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?>

  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css"
  href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/countrySelect.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/intlTelInput.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">


  <style type="text/css">
    .country-select, .iti--separate-dial-code {
      width: 100%;
    }

    .iti__selected-flag {
      border-radius: 20px 0 0 20px;
      height: auto;
      padding-top: 8px;
    }

    .iti--separate-dial-code .iti__selected-flag {
      background-color: transparent;
    }

    .country-select.inside .flag-dropdown:hover .selected-flag {
      background-color: transparent;
    }

    .country-list {
      width: 260px !important;
    }
  </style>

</head>

<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?>
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?>

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>View Agreements</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Agreement Data</li>
                <li>View Agreements</li>
              </ul>
            </div>
          </div>
        </div>

        <form id="search_agreement_form">

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Booking No</label>
                      <select class="form-control-rounded form-control search_dropdown" name="bookingNo" id="bookingNo">
                        <option value="" disabled selected hidden>Select Booking No</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Agreement No</label>
                      <select class="form-control-rounded form-control search_dropdown" name="agreement_No" id="agreement_No">
                        <option value="" disabled selected hidden>Select Agreement No</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">From</label>
                      <input type="date" class="form-control-rounded form-control" id="fromDate" name="fromDate"
                      value="<?php echo date('Y-m-d'); ?>" onchange="changeDate()" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">To</label>
                      <input type="date" class="form-control-rounded form-control" id="toDate" name="toDate" required>
                    </div>
                    <div class="form-group col-md-3 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">Pickup Date</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" checked id="pickup_date" />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group col-md-3 d-flex align-items-center">
                      <label for="exampleFormControlInput1" style="margin-bottom: 0;">Return Date</label>
                      <div class="vc-toggle-container ml-3">
                        <label class="vc-switch mt-2">
                          <input type="checkbox" class="vc-switch-input" id="return_date" />
                          <span class="vc-switch-label" data-on="Active" data-off="Inactive"></span>
                          <span class="vc-handle"></span>
                        </label>
                      </div>
                    </div>
                    <div class="form-group mt-4 col-md-3 offset-md-3 text-right">
                      <input type="submit" name="time" class="btn btn-button-7 btn-rounded"
                      value="Search">
                      <input type="reset" name="time" id="reset_button" class="btn btn-button-6 btn-rounded"
                      value="Reset">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </form>

        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow" id="table_div">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Agreement No</th>
                        <th>Booking No</th>
                        <th>Full Name</th>
                        <th>Pickup Date</th>
                        <th>Return Date</th>
                        <th>Vehicle No</th>
                        <th>Pickup Location</th>
                        <th>Return Location</th>
                        <th>Status Action</th>
                        <th style="text-align:center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                     <?php foreach($data as $agreement) { ?>
                      <tr>
                        <td><?php echo $agreement->agreement_no; ?></td>
                        <td><?php echo $agreement->booking_no; ?></td>
                        <td><?php echo $agreement->first_name." ".$agreement->last_name; ?></td>
                        <td><?php echo $agreement->from_date; ?></td>
                        <td><?php echo $agreement->to_date; ?></td>
                        <td><?php echo $agreement->registration_no; ?></td>
                        <td><?php echo $agreement->pickup_location; ?></td>
                        <td><?php echo $agreement->return_location; ?></td>
                        <td>
                         <?php if($agreement->is_end == 0) { ?>
                          <button class="btn btn-outline-secondary btn-rounded" onclick="endAgreement('<?php echo $agreement->id; ?>', '<?php echo $agreement->agreement_no; ?>', '<?php echo $agreement->first_name; ?>', '<?php echo $agreement->last_name; ?>', '<?php echo $agreement->from_date; ?>', '<?php echo $agreement->to_date; ?>', '<?php echo $agreement->milage_starts; ?>');">End</button>
                        <?php } ?>
                      </td>
                      <td class="text-center" style="width: 230px;">
                        <?php if($agreement->is_end == 0) { ?>
                          <button class="btn btn-outline-secondary btn-rounded" onclick="editAgreement('<?php echo $agreement->id; ?>');">Edit</button>
                        <?php } ?>
                        <a href="<?php echo base_url(); ?>agreement/printAgreement/<?php echo $agreement->id; ?>" class="btn btn-outline-warning btn-rounded" target="_blank">Print</a>
                        <button class="btn btn-outline-info btn-rounded" onclick="deleteAgreement('<?php echo $agreement->id; ?>');">Delete</button>
                        <button class="btn btn-outline-success btn-rounded" onclick="viewAgreement('<?php echo $agreement->id; ?>');">View</button>
                      </td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>

</div>
</div>
<!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->


<div class="modal fade bd-example2-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_end">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Agreement - <span id="agreementno"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="end_agreement_form">
          <div class="row p-2">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="row">
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Date</label>
                  <input type="date" value="<?php echo date('Y-m-d'); ?>" class="form-control-rounded form-control" id="end_date" name="end_date" required>
                  <input type="hidden" name="agreementid" id="agreementid">
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Milage Ends</label>
                  <input type="number" class="form-control-rounded form-control" id="milage_end" name="milage_end" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleFormControlInput1">Client name</label>
                  <input type="text" class="form-control-rounded form-control" id="client_name" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">From Date</label>
                  <input type="text" class="form-control-rounded form-control" id="from_date" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">To Date</label>
                  <input type="text" class="form-control-rounded form-control" id="to_date" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Milage Start</label>
                  <input type="text" class="form-control-rounded form-control" id="milage_start" readonly>
                </div>
                <div class="form-group col-md-3 text-right mt-4">
                 <input type="submit" name="time" class="btn btn-button-7 btn-rounded px-4" value="End">
                 <input type="reset" name="time" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
               </div>
             </div>
           </div>
         </div> 
       </form>
     </div>
   </div>
 </div>
</div>




<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_view">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Agreement - <span id="agreement_no_v_label"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <form> 

        <div class="row layout-spacing p-2">

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="row">
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Booking No</label>
                <input type="text" class="form-control-rounded form-control" id="booking_no_v" readonly>
              </div>
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Agreement No</label>
                <input type="text" class="form-control-rounded form-control" id="agreement_no_v" readonly>
              </div>
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Date</label>
                <input type="text" class="form-control-rounded form-control" id="created_at_v" readonly>
              </div>
            </div>
          </div>  

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Personal Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">First Name</label>
                  <input type="text" class="form-control-rounded form-control" id="fName_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Last Name</label>
                  <input type="text" class="form-control-rounded form-control" id="lName_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Date of Birth</label>
                  <input type="date" class="form-control-rounded form-control" id="birthday_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Age</label>
                  <input type="text" class="form-control-rounded form-control" id="age_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Nationality</label>
                  <input type="text" class="form-control-rounded form-control" id="nationality_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Passpport No</label>
                  <input type="text" class="form-control-rounded form-control" id="passport_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Visa Expiry Date</label>
                  <input type="date" class="form-control-rounded form-control" id="expiryDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Country</label>
                  <input type="text" class="form-control-rounded form-control" id="country_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Mobile No 1</label>
                  <input type="text" class="form-control-rounded form-control" id="mobile1_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Mobile No 2 (optional)</label>
                  <input type="text" class="form-control-rounded form-control" id="mobile2_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Email</label>
                  <input type="email" class="form-control-rounded form-control" id="email_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Address in Sri Lanka</label>
                  <input type="text" class="form-control-rounded form-control" id="address_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Permanent Address</label>
                  <input type="text" class="form-control-rounded form-control" id="permenentAdd_v" readonly>
                </div>
              </div>
            </div>
          </div>




          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Flight Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Flight Number</label>
                  <input type="text" class="form-control-rounded form-control" id="flightNo_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Location</label>
                  <input type="text" class="form-control-rounded form-control" id="departure_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Date</label>
                  <input type="date" class="form-control-rounded form-control" id="depDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Time</label>
                  <input type="time" class="form-control-rounded form-control" id="depTime_v" readonly>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Rental Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Vehicle Type</label>
                  <input type="text" class="form-control-rounded form-control" id="vehicletype_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Vehicle Reg No</label>
                  <input type="text" class="form-control-rounded form-control" id="regNo_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Sri Lankan Driving License No</label>
                  <input type="text" class="form-control-rounded form-control" id="licenseNo_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">From</label>
                  <input type="text" class="form-control-rounded form-control" id="fromDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">To</label>
                  <input type="text" class="form-control-rounded form-control" id="toDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Milage Starts</label>
                  <input type="text" class="form-control-rounded form-control" id="milageStart_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Milage Ends</label>
                  <input type="text" class="form-control-rounded form-control" id="milageEnd_v" readonly>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <a href="" class="btn btn-outline-secondary btn-rounded mt-2" target="_blank" id="print_v">Print</a>
          <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-2" data-dismiss="modal" value="Close">
        </div>

      </form>

    </div>
  </div>
</div>
</div>



<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_edit">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Agreement - <span id="agreement_no_edit_label"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="agreement_edit_form"> 

          <div class="row layout-spacing p-2">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Booking No</label>
                  <select class="form-control-rounded form-control search_dropdown" id="booking_no_edit" name="booking_no_edit" required style="width:100%;">
                    <option value="" disabled selected hidden>Select Booking No</option>
                  </select>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Agreement No</label>
                  <input type="text" class="form-control-rounded form-control" id="agreement_no_edit" readonly>
                  <input type="hidden" class="form-control-rounded form-control" id="agreement_id_edit">
                  <input type="hidden" id="vehicle_id_edit" name="vehicle_id_edit">
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Date</label>
                  <input type="text" class="form-control-rounded form-control" id="created_at_edit" readonly>
                </div>
              </div>
            </div>  

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                      <h4 class="p-0 pb-4">Personal Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="row">
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">First Name</label>
                    <input type="text" class="form-control-rounded form-control" id="fName_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Last Name</label>
                    <input type="text" class="form-control-rounded form-control" id="lName_edit">
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Date of Birth</label>
                    <input type="date" class="form-control-rounded form-control" id="birthday_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Age</label>
                    <input type="text" class="form-control-rounded form-control" id="age_edit" readonly>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Nationality</label>
                    <input type="text" class="form-control-rounded form-control" id="nationality_edit">
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Passpport No</label>
                    <input type="text" class="form-control-rounded form-control" id="passport_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Visa Expiry Date</label>
                    <input type="date" class="form-control-rounded form-control" id="expiryDate_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="country">Country</label><br>
                    <input id="country_selector" name="country_selector" class="form-control-rounded form-control" type="text" required>

                    <div class="form-item" style="display:none;">
                      <input type="text" id="country_selector_code" name="country_selector_code" data-countrycodeinput="1" readonly="readonly" placeholder="Selected country code will appear here" />
                    </div>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Mobile No 1</label>
                    <input type="number" class="form-control-rounded form-control phone" id="mobile1" name="mobile1" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Mobile No 2 (optional)</label>
                    <input type="number" class="form-control-rounded form-control phone2" name="mobile2" id="mobile2">
                  </div>
                  <div class="form-group mb-4 col-md-6">
                    <label for="exampleFormControlInput1">Email</label>
                    <input type="email" class="form-control-rounded form-control" id="email_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-6">
                    <label for="exampleFormControlInput1">Address in Sri Lanka</label>
                    <input type="text" class="form-control-rounded form-control" id="address_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-6">
                    <label for="exampleFormControlInput1">Permanent Address</label>
                    <input type="text" class="form-control-rounded form-control" id="permenentAdd_edit" required>
                  </div>
                </div>
              </div>
            </div>




            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                      <h4 class="p-0 pb-4">Flight Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="row">
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Flight Number</label>
                    <input type="text" class="form-control-rounded form-control" id="flightNo_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Departure Location</label>
                    <input type="text" class="form-control-rounded form-control" id="departure_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Departure Date</label>
                    <input type="date" class="form-control-rounded form-control" id="depDate_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Departure Time</label>
                    <input type="time" class="form-control-rounded form-control" id="depTime_edit" required>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                      <h4 class="p-0 pb-4">Rental Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="row">
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Vehicle Type</label>
                    <select class="form-control-rounded form-control" id="vehicletype_edit" name="vehicletype_edit" required style="width:100%;">
                    </select>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Vehicle Reg No</label>
                    <select class="form-control-rounded form-control search_dropdown" name="regNo_edit" id="regNo_edit" required style="width:100%;">
                      <option value="" disabled selected hidden>Select Vehicle Reg No</option>
                    </select>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Sri Lankan Driving License No</label>
                    <input type="text" class="form-control-rounded form-control" id="licenseNo_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">From</label>
                    <input type="datetime-local" class="form-control-rounded form-control" id="fromDate_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">To</label>
                    <input type="datetime-local" class="form-control-rounded form-control" id="toDate_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Milage Starts</label>
                    <input type="text" class="form-control-rounded form-control" id="milageStart_edit" required>
                  </div>
                  <div class="form-group mb-4 col-md-3">
                    <label for="exampleFormControlInput1">Milage Ends</label>
                    <input type="text" class="form-control-rounded form-control" id="milageEnd_edit" readonly>
                  </div>
                </div>
              </div>
            </div>

          </div>

          <div class="modal-footer">
            <input type="submit" class="btn btn-button-7 btn-rounded mt-2" value="Update">
            <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-2" data-dismiss="modal" value="Close">
          </div>

        </form>

      </div>
    </div>
  </div>
</div>




<input type="hidden" id="search" value="0">

<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?>
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?>
<!-- END GLOBAL MANDATORY STYLES -->

<script src="<?php echo base_url(); ?>assets/assets/js/countrySelect.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/intlTelInput.js"></script>

<script>
  $("#country_selector").countrySelect({
    responsiveDropdown: true,
    preferredCountries: ['lk', 'us', 'gb']
  });

  var input = document.querySelector(".phone");
  var input2 = document.querySelector(".phone2");

  var iti = window.intlTelInput(input, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  var iti2 = window.intlTelInput(input2, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  $("#country_selector").on('change', function(){
    var code = $("#country_selector").countrySelect("getSelectedCountryData").iso2;
    iti.setCountry(code);
    iti2.setCountry(code);
  });

</script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });

  //From and To Date selection
  var today = new Date();
  var hh = today.getHours();
  var ii = today.getMinutes();
  var d = String(today.getDate()).padStart(2, '0');
  var mm = String(today.getMonth() + 1).padStart(2, '0');
  var yyyy = today.getFullYear();

  var from_date = yyyy + '-' + mm + '-' + d;
  // $('#end_date').attr('min', from_date);

  var dd = String(today.getDate() + 1).padStart(2, '0');
  to_date = yyyy + '-' + mm + '-' + dd;
  $('#toDate').attr('min', to_date);
  $('#toDate').val(to_date);

  function changeDate() {
    var from_today = new Date($('#fromDate').val());
    var dd = String(from_today.getDate() + 1).padStart(2, '0');
    var mm = String(from_today.getMonth() + 1).padStart(2, '0');
    var yyyy = from_today.getFullYear();

    from_today = yyyy + '-' + mm + '-' + dd;
    $('#toDate').val(from_today);
    $('#toDate').attr('min', from_today);
  }

  getBookingNumbers();

  function getBookingNumbers() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getBookingNumbers',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {

        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          if (result.data.length > 0) {
            $.each(result.data, function (i, item) {
              $('#bookingNo').append($('<option>', { 
                value: item.id,
                text : item.booking_no 
              }));
              $('#booking_no_edit').append($('<option>', { 
                value: item.id,
                text : item.booking_no 
              }));              
            });
          }
          $(".cs-overlay").css('visibility', 'hidden');
          getAllVehicleTypes();
        }        
      }
    });
  }


  function getAllVehicleTypes() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>master/getAllVehicleTypes',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#vehicletype_edit').append($('<option>', { 
              value: item.id,
              text : item.type 
            }));
          });
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }


  function viewAgreement(id) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          let print_url = '<?php echo base_url(); ?>agreement/printAgreement/'+result.data.id;
          $('#booking_no_v').val(result.data.booking.booking_no);
          $('#agreement_no_v').val(result.data.agreement_no);
          $('#agreement_no_v_label').html(result.data.agreement_no);            
          $('#vehicletype_v').val(result.data.vehicle.vehicle_type.type);
          $('#created_at_v').val(result.data.created_at);             
          $('#fName_v').val(result.data.first_name); 
          $('#lName_v').val(result.data.last_name); 
          $('#birthday_v').val(result.data.date_of_birth); 
          $('#age_v').val(result.data.age); 
          $('#nationality_v').val(result.data.nationality); 
          $('#passport_v').val(result.data.passport_no); 
          $('#expiryDate_v').val(result.data.visa_expiry_date); 
          $('#country_v').val(result.data.country.country); 
          $('#mobile1_v').val(result.data.mobile_1); 
          $('#mobile2_v').val(result.data.mobile_2); 
          $('#email_v').val(result.data.email); 
          $('#address_v').val(result.data.address_in_srilanka); 
          $('#permenentAdd_v').val(result.data.permanent_address); 
          $('#flightNo_v').val(result.data.flight_number); 
          $('#departure_v').val(result.data.departure_location); 
          $('#depDate_v').val(result.data.departure_date); 
          $('#depTime_v').val(result.data.departure_time); 
          $('#fromDate_v').val(result.data.from_date); 
          $('#toDate_v').val(result.data.to_date); 
          $('#regNo_v').val(result.data.vehicle_id); 
          $('#licenseNo_v').val(result.data.license_no); 
          $('#milageStart_v').val(result.data.milage_starts); 
          $('#milageEnd_v').val(result.data.milage_ends); 
          $('#print_v').attr('href', print_url)
          $('#modal_view').modal('show');
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }


  function deleteAgreement(id) {
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure, do you want to delete this?',
      type: 'red',
      buttons: {
        confirm: function(){
          $.ajax({
            type:'post',
            url:'<?php echo base_url(); ?>agreement/deleteAgreement/'+id,
            beforeSend: function(){
              $(".cs-overlay").css('visibility', 'visible');
            },
            success:function (res) {
              let search = $('#search').val();
              if (search == 1) {
                $.confirm({
                  title: 'Success!',
                  content: 'Agreement successfully deleted!',
                  type: 'green',
                  draggable: false,
                  buttons: {
                    tryAgain: {
                      text: 'Close',
                      action: function(){
                       $('#search_agreement_form').submit();
                     }
                   },
                 }
               });              
              } else {
                getAllAgreements('deleted');
              }            
            }
          });
        },
        cancel : {

        }
      }
    });
  }


  function getAllAgreements(message) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAllAgreements',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $('#table_div').html(res);   
        $(".cs-overlay").css('visibility', 'hidden');  

        if (message == 'updated') {

          $.confirm({
            title: 'Success!',
            content: 'Agreement successfully '+message+'!',
            type: 'green',
            draggable: false,
            buttons: {
              showView: {
                text: 'Print',
                action: function(){
                  let updated_id = $('#agreement_id_edit').val();
                  window.open(
                    '<?php echo base_url(); ?>agreement/printAgreement/'+updated_id,
                    '_blank' 
                    );
                }
              },
              tryAgain: {
                text: 'Close',
                action: function(){
                }
              },
            }
          });

        } else {

          $.confirm({
            title: 'Success!',
            content: 'Agreement successfully '+message+'!',
            type: 'green',
            draggable: false,
            buttons: {
              tryAgain: {
                text: 'Close',
                action: function(){
                }
              },
            }
          });

        }
      }
    });
  }


  function endAgreement(id, agreement_no, fName, lName, fromDate, toDate, milage_starts) {
    $('#agreementid').val(id);
    $('#agreementno').html(agreement_no);
    $('#client_name').val(fName+' '+lName);
    $('#from_date').val(fromDate);
    $('#to_date').val(toDate);
    $('#milage_start').val(milage_starts);
    $('#milage_end').attr('min',milage_starts);
    $('#modal_end').modal('show');
  }

  $('#bookingNo').on('change', function (argument) {
    let booking_id = $(this).val();
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementNoByBookingId/'+booking_id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#agreement_No').empty();
          $('#agreement_No').append('<option value="" selected>Select Agreement No</option>');
          if (result.data.length > 0) {
            $.each(result.data, function (i, item) {
              $('#agreement_No').append($('<option>', { 
                value: item.id,
                text : item.agreement_no 
              }));
            });
          }
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  });

  $('#reset_button').on('click', function() {
    $('#bookingNo').empty();
    $('#bookingNo').append('<option value="" disabled selected hidden>Select Booking No</option>');
    getBookingNumbers();
    $('#agreement_No').empty();
    $('#agreement_No').append('<option value="" disabled selected hidden>Select Agreement No</option>');
    $("form").validate().resetForm();
  });


  
  $('#search_agreement_form').submit(function(evt) {
    evt.preventDefault();

    let bookingNo = $('#bookingNo').val();
    let agreement_No = $('#agreement_No').val();
    let fromDate = $('#fromDate').val();
    let toDate = $('#toDate').val();

    let pickup_date = 0;
    if ($('#pickup_date').is(':checked')) {
      pickup_date = 1;
    }

    let return_date = 0;
    if ($('#return_date').is(':checked')) {
      return_date = 1;
    }

    if (bookingNo == null || bookingNo == "") {
      bookingNo = -1;
    }

    if (agreement_No == null || agreement_No == "") {
      agreement_No = -1;
    }

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/searchAgreement',
      data:{bookingNo,agreement_No,fromDate,toDate,pickup_date,return_date},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $('#search').val(1);
        $('#table_div').html(res);
        $(".cs-overlay").css('visibility', 'hidden');
      }
    });
  });



  $('#vehicletype_edit').on('change', function () {
    let vehicle_type_id = $(this).val();

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getVehicleRegistrationNumberByVehicleType',
      data:{vehicle_type_id},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $.each(result.data, function (i, item) {
            $('#regNo_edit').append($('<option>', { 
              value: item.vehicle_id,
              text : item.registration_no 
            }));
          });
          if ($('#vehicle_id_edit').val() != "") {
            $('#regNo_edit').val($('#vehicle_id_edit').val());
            $('#regNo_edit').trigger('change');
          }
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  });



  $('#end_agreement_form').submit(function(evt) {
    evt.preventDefault();
    let agreementid = $('#agreementid').val();
    let end_date = $('#end_date').val();
    let milage_end = $('#milage_end').val();

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/endAgreement',
      data:{agreementid,end_date,milage_end},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        $('#modal_end').modal('hide');
        let search = $('#search').val();
        if (search == 1) {
          $.confirm({
            title: 'Success!',
            content: 'Agreement successfully ended!',
            type: 'green',
            draggable: false,
            buttons: {
              tryAgain: {
                text: 'Close',
                action: function(){
                 $('#search_agreement_form').submit();
               }
             },
           }
         });              
        } else {
          getAllAgreements('ended');
        } 
      }
    });
  });


  $('#birthday_edit').on('change', function() {
    let dob = $(this).val();
    dob = new Date(dob);
    let today = new Date();
    let age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
    $('#age_edit').val(age);
  });


  function editAgreement(id) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#booking_no_edit').val(result.data.booking_id);
          $('#booking_no_edit').trigger('change');
          $('#agreement_no_edit').val(result.data.agreement_no);
          $('#agreement_no_edit_label').html(result.data.agreement_no);            
          $('#vehicletype_edit').val(result.data.vehicle.vehicle_type_id);
          $('#vehicletype_edit').trigger('change');
          $('#created_at_edit').val(result.data.created_at);             
          $('#fName_edit').val(result.data.first_name); 
          $('#lName_edit').val(result.data.last_name); 
          $('#birthday_edit').val(result.data.date_of_birth); 
          $('#age_edit').val(result.data.age); 
          $('#nationality_edit').val(result.data.nationality); 
          $('#passport_edit').val(result.data.passport_no); 
          $('#expiryDate_edit').val(result.data.visa_expiry_date); 
          $("#country_selector").countrySelect("selectCountry", result.data.country_code);
          iti.setNumber(result.data.mobile_1_country_code);
          iti2.setNumber(result.data.mobile_2_country_code);
          $('#mobile1').val(result.data.mobile_1); 
          $('#mobile2').val(result.data.mobile_2); 
          $('#email_edit').val(result.data.email); 
          $('#address_edit').val(result.data.address_in_srilanka); 
          $('#permenentAdd_edit').val(result.data.permanent_address); 
          $('#flightNo_edit').val(result.data.flight_number); 
          $('#departure_edit').val(result.data.departure_location); 
          $('#depDate_edit').val(result.data.departure_date); 
          $('#depTime_edit').val(result.data.departure_time); 
          $('#fromDate_edit').val(result.data.from_date); 
          $('#toDate_edit').val(result.data.to_date); 
          $('#regNo_edit').val(result.data.vehicle_id); 
          $('#licenseNo_edit').val(result.data.license_no); 
          $('#milageStart_edit').val(result.data.milage_starts); 
          $('#milageEnd_edit').val(result.data.milage_ends); 
          $('#agreement_id_edit').val(result.data.id); 
          $('#vehicle_id_edit').val(result.data.vehicle_id); 
          $('#modal_edit').modal('show');
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }


  
  $('#agreement_edit_form').submit(function(evt) {
    evt.preventDefault();
    let mobile_code = '+'+iti.selectedCountryData.dialCode;
    let mobile2_code = '+'+iti2.selectedCountryData.dialCode;
    let booking_no_edit = $('#booking_no_edit').val();
    let vehicletype_edit = $('#vehicletype_edit').val();
    let created_at_edit = $('#created_at_edit').val();
    let fName_edit = $('#fName_edit').val();
    let lName_edit = $('#lName_edit').val();
    let birthday_edit = $('#birthday_edit').val();
    let age_edit = $('#age_edit').val();
    let nationality_edit = $('#nationality_edit').val();
    let passport_edit = $('#passport_edit').val();
    let expiryDate_edit = $('#expiryDate_edit').val();
    let country_selector_code = $('#country_selector_code').val();
    let mobile_country_code = mobile_code;
    let mobile2_country_code = mobile2_code;
    let mobile1 = $('#mobile1').val();
    let mobile2 = $('#mobile2').val();
    let email_edit = $('#email_edit').val();
    let address_edit = $('#address_edit').val();
    let permenentAdd_edit = $('#permenentAdd_edit').val();
    let flightNo_edit = $('#flightNo_edit').val();
    let departure_edit = $('#departure_edit').val();
    let depDate_edit = $('#depDate_edit').val();
    let depTime_edit = $('#depTime_edit').val();
    let fromDate_edit = $('#fromDate_edit').val();
    let toDate_edit = $('#toDate_edit').val();
    let regNo_edit = $('#regNo_edit').val();
    let licenseNo_edit = $('#licenseNo_edit').val();
    let milageStart_edit = $('#milageStart_edit').val();
    let milageEnd_edit = $('#milageEnd_edit').val();
    let agreement_id_edit = $('#agreement_id_edit').val();

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/updateAgreement',
      data:{booking_no_edit, vehicletype_edit, created_at_edit, fName_edit, lName_edit, birthday_edit, age_edit, nationality_edit, passport_edit, expiryDate_edit, country_selector_code, mobile_country_code, mobile2_country_code, mobile1, mobile2, email_edit, address_edit, permenentAdd_edit, flightNo_edit, departure_edit, depDate_edit, depTime_edit, fromDate_edit, toDate_edit, regNo_edit, licenseNo_edit, milageStart_edit, milageEnd_edit, agreement_id_edit},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        getAllAgreements('updated');   
        $('#modal_edit').modal('hide');   
      }
    });
  });
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>

</html>