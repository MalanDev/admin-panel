<!DOCTYPE html>
<html lang="en">
<head>

  <?php require __DIR__.'../../includes/top_header.php'; ?> 

  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/plugins/table/datatable/custom_dt_multi_col_ordering.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/assets/css/vc-toggle-switch.css" />

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/countrySelect.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/assets/css/intlTelInput.css">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
  <link href="<?php echo base_url(); ?>assets/select2.css" rel="stylesheet" />
  <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

  <style type="text/css">
    .country-select, .iti--separate-dial-code {
      width: 100%;
    }

    .iti__selected-flag {
      border-radius: 20px 0 0 20px;
      height: auto;
      padding-top: 8px;
    }

    .iti--separate-dial-code .iti__selected-flag {
      background-color: transparent;
    }

    .country-select.inside .flag-dropdown:hover .selected-flag {
      background-color: transparent;
    }

    #bookingNo-error {
      position: absolute;
      bottom: -30px;
      left: 20px;
    }

    #regNo-error {
      position: absolute;
      bottom: 0;
      left: 20px;
    }
  </style>

</head>
<body>
  <!-- Tab Mobile View Header -->
  <?php require __DIR__.'../../includes/header.php'; ?> 
  <!--  END NAVBAR  -->

  <!--  BEGIN MAIN CONTAINER  -->
  <div class="main-container" id="container">

    <div class="cs-overlay">
      <img src="<?php echo base_url(); ?>assets/assets/img/loader.gif">
    </div>

    <!--  BEGIN SIDEBAR  -->

    <?php require __DIR__.'../../includes/sidebar.php'; ?> 

    <!--  END SIDEBAR  -->

    <!--  BEGIN CONTENT PART  -->
    <div id="content" class="main-content">
      <div class="container">
        <div class="page-header">
          <div class="page-title">
            <h3>Create Agreement</h3>
            <div class="crumbs">
              <ul id="breadcrumbs" class="breadcrumb">
                <li><a href="<?php echo base_url(); ?>"><i class="flaticon-home-fill"></i></a></li>
                <li>Agreement Data</li>
                <li>Create Agreement</li>
              </ul>
            </div>
          </div>
        </div>

        <?php if($this->session->flashdata('error')) { ?>
          <div class="alert alert-danger alert-messages">
            <?php echo $this->session->flashdata('error'); ?>
          </div>
        <?php } else if ($this->session->flashdata('success')) { ?>
          <div class="alert alert-success alert-messages">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php } ?>

        <form id="agreement_form"> 

          <div class="row layout-spacing">

            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Booking No</label>
                      <select class="form-control-rounded form-control search_dropdown" id="bookingNo" name="bookingNo" required>
                        <option value="" disabled selected hidden>Select Booking No</option>
                      </select>
                      <input type="hidden" id="agreement_id" name="agreement_id">
                      <input type="hidden" id="vehicle_id_edit" name="vehicle_id_edit">
                    </div>
                    <div class="form-group mt-4 col-md-3">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Agreement No</label>
                      <input type="text" class="form-control-rounded form-control" id="agreement_no" name="agreement_no" readonly>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Date</label>
                      <input type="datetime" value="<?php echo date('Y-m-d h:i a'); ?>" class="form-control-rounded form-control" id="ddate" name="ddate" readonly>
                    </div>
                  </div>
                </div>
              </div>
            </div>  



            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Personal Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="fName">First Name</label>
                      <input type="text" class="form-control-rounded form-control" id="fName" 
                      name="fName" placeholder="Enter First Name" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Last Name</label>
                      <input type="text" class="form-control-rounded form-control" name="lName" 
                      id="lName" placeholder="Enter Last Name">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="birthday">Date of Birth</label>
                      <input type="date" class="form-control-rounded form-control" name="birthday" id="birthday" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Age</label>
                      <input type="text" class="form-control-rounded form-control" id="age" name="age" readonly>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Nationality</label>
                      <input type="text" class="form-control-rounded form-control" name="nationality" 
                      id="nationality" placeholder="Enter Nationality">
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="passport">Passport No</label>
                      <input type="text" class="form-control-rounded form-control" id="passport" 
                      name="passport" placeholder="Enter Passport No" required >
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="expiryDate">Visa Expiry Date</label>
                      <input type="date" class="form-control-rounded form-control" id="expiryDate" name="expiryDate" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="country">Country</label><br>
                      <input id="country_selector" name="country_selector" class="form-control-rounded form-control" type="text" required>

                      <div class="form-item" style="display:none;">
                        <input type="text" id="country_selector_code" name="country_selector_code" data-countrycodeinput="1" readonly="readonly" placeholder="Selected country code will appear here" />
                      </div>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="mobile">Mobile No 1</label>
                      <input type="number" class="form-control-rounded form-control phone" id="mobile1" name="mobile1" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Mobile No 2 (optional)</label>
                      <input type="number" class="form-control-rounded form-control phone2" name="mobile2" id="mobile2">
                    </div>
                    <div class="form-group mb-4 col-md-6">
                      <label for="email">Email</label>
                      <input type="email" class="form-control-rounded form-control" id="email" name="email" placeholder="Enter Email" required>
                    </div>
                    <div class="form-group mb-4 col-md-6">
                      <label for="address">Address in Sri Lanka</label>
                      <input type="text" class="form-control-rounded form-control" id="address" name="address" placeholder="Enter Address" required>
                    </div>
                    <div class="form-group mb-4 col-md-6">
                      <label for="permenentAdd">Permanent Address</label>
                      <input type="text" class="form-control-rounded form-control" id="permenentAdd" name="permenentAdd"
                      placeholder="Enter Permanent Address" required>
                    </div>
                  </div>
                </div>
              </div>
            </div>




            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Flight Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="flightNo">Flight Number</label>
                      <input type="text" class="form-control-rounded form-control" id="flightNo" name="flightNo"
                      placeholder="Enter Flight Number" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="departure">Departure Location</label>
                      <input type="text" class="form-control-rounded form-control" id="departure" name="departure" 
                      placeholder="Enter Departure Location" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="depDate">Departure Date</label>
                      <input type="date" class="form-control-rounded form-control" id="depDate" name="depDate" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="depTime">Departure Time</label>
                      <input type="time" class="form-control-rounded form-control" id="depTime" name="depTime" required>
                    </div>
                  </div>
                </div>
              </div>
            </div>



            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="statbox widget box box-shadow">
                <div class="widget-header">                                
                  <div class="row">
                    <div class="col-xl-12 col-md-12 col-sm-12 col-12">
                      <h4 class="pb-0">Rental Details</h4>
                    </div>                                                      
                  </div>
                </div>
                <div class="widget-content widget-content-area">
                  <div class="row">
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Vehicle Type</label>
                      <select class="form-control-rounded form-control" id="vehicletype" name="vehicleType" readonly>
                        <!-- <option value="" disabled selected hidden>Select Vehicle Type</option> -->
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="fromDate">From</label>
                      <input type="datetime-local" class="form-control-rounded form-control" id="fromDate" name="fromDate"
                      value="<?php echo date('Y-m-d\TH:i'); ?>" onchange="changeDate()" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="toDate">To</label>
                      <input type="datetime-local" class="form-control-rounded form-control" id="toDate" onchange="changeToDate()" name="toDate" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="regNo">Vehicle Reg No</label>
                      <select class="form-control-rounded form-control search_dropdown" name="regNo" id="regNo" required>
                        <option value="" disabled selected hidden>Select Vehicle Reg No</option>
                      </select>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="licenseNo">Sri Lankan Driving License No</label>
                      <input type="text" class="form-control-rounded form-control" 
                      name="licenseNo" id="licenseNo" placeholder="Enter License No"required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="milageStart">Milage Starts</label>
                      <input type="number" class="form-control-rounded form-control" id="milageStart" 
                      name="milageStart" placeholder="Enter Milage Starts" required>
                    </div>
                    <div class="form-group mb-4 col-md-3">
                      <label for="exampleFormControlInput1">Milage Ends</label>
                      <input type="number" class="form-control-rounded form-control" name="milageEnd" 
                      id="milageEnd" placeholder="Enter Milage Ends" readonly>
                    </div>
                    <div class="form-group col-md-3 text-right mt-4">
                      <input type="submit" name="time" class="btn btn-button-7 btn-rounded mt-2" id="submit_button" value="Save">
                      <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-2" id="reset_button" value="Cancel">
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

        </form> 


        <div class="row" id="cancel-row">
          <div class="col-xl-12 col-lg-12 col-sm-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-content widget-content-area">
                <div class="table-responsive mb-4">
                  <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
                    <thead>
                      <tr>
                        <th>Agreement No</th>
                        <th>Booking No</th>
                        <th>Full Name</th>
                        <th>Pickup Date</th>
                        <th>Return Date</th>
                        <th>Vehicle Reg No</th>
                        <th>Pickup Location</th>
                        <th>Return Location</th>
                        <th>Status Action</th>
                        <th style="text-align:center;">Action</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach($data as $agreement) { ?>
                        <tr>
                          <td><?php echo $agreement->agreement_no; ?></td>
                          <td><?php echo $agreement->booking_no; ?></td>
                          <td><?php echo $agreement->first_name." ".$agreement->last_name; ?></td>
                          <td><?php echo $agreement->from_date; ?></td>
                          <td><?php echo $agreement->to_date; ?></td>
                          <td><?php echo $agreement->registration_no; ?></td>
                          <td><?php echo $agreement->pickup_location; ?></td>
                          <td><?php echo $agreement->return_location; ?></td>
                          <td>
                           <?php if($agreement->is_end == 0) { ?>
                            <button class="btn btn-outline-secondary btn-rounded" onclick="endAgreement('<?php echo $agreement->id; ?>', '<?php echo $agreement->agreement_no; ?>', '<?php echo $agreement->first_name; ?>', '<?php echo $agreement->last_name; ?>', '<?php echo $agreement->from_date; ?>', '<?php echo $agreement->to_date; ?>', '<?php echo $agreement->milage_starts; ?>');">End</button>
                          <?php } ?>
                        </td>
                        <td class="text-center" style="width: 230px;">
                          <?php if($agreement->is_end == 0) { ?>
                            <button class="btn btn-outline-secondary btn-rounded" onclick="editAgreement('<?php echo $agreement->id; ?>');">Edit</button>
                          <?php } ?>
                          <a href="<?php echo base_url(); ?>agreement/printAgreement/<?php echo $agreement->id; ?>" class="btn btn-outline-warning btn-rounded" target="_blank">Print</a>
                          <button class="btn btn-outline-info btn-rounded" onclick="deleteAgreement('<?php echo $agreement->id; ?>');">Delete</button>
                          <button class="btn btn-outline-success btn-rounded" onclick="viewAgreement('<?php echo $agreement->id; ?>');">View</button>
                        </td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--  END CONTENT PART  -->
</div>
<!-- END MAIN CONTAINER -->




<div class="modal fade bd-example2-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_end">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Agreement - <span id="agreementno"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?php echo base_url(); ?>agreement/endAgreement" method="post">
          <div class="row p-2">
            <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="row">
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Date</label>
                  <input type="date" value="<?php echo date('Y-m-d'); ?>" class="form-control-rounded form-control" id="end_date" name="end_date" required>
                  <input type="hidden" name="agreementid" id="agreementid">
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Milage Ends</label>
                  <input type="number" class="form-control-rounded form-control" id="milage_end" name="milage_end" required>
                </div>
                <div class="form-group col-md-6">
                  <label for="exampleFormControlInput1">Client name</label>
                  <input type="text" class="form-control-rounded form-control" id="client_name" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">From Date</label>
                  <input type="text" class="form-control-rounded form-control" id="from_date" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">To Date</label>
                  <input type="text" class="form-control-rounded form-control" id="to_date" readonly>
                </div>
                <div class="form-group col-md-3">
                  <label for="exampleFormControlInput1">Milage Start</label>
                  <input type="text" class="form-control-rounded form-control" id="milage_start" readonly>
                </div>
                <div class="form-group col-md-3 text-right mt-4">
                 <input type="submit" name="time" class="btn btn-button-7 btn-rounded px-4" value="End">
                 <input type="reset" name="time" class="btn btn-button-6 btn-rounded" data-dismiss="modal" value="Close">
               </div>
             </div>
           </div>
         </div> 
       </form>
     </div>
   </div>
 </div>
</div>




<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true" id="modal_view">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="myExtraLargeModalLabel">Agreement - <span id="agreement_no_v_label"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
       <form> 

        <div class="row layout-spacing p-2">

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="row">
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Booking No</label>
                <input type="text" class="form-control-rounded form-control" id="booking_no_v" readonly>
              </div>
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Agreement No</label>
                <input type="text" class="form-control-rounded form-control" id="agreement_no_v" readonly>
              </div>
              <div class="form-group mb-4 col-md-3">
                <label for="exampleFormControlInput1">Date</label>
                <input type="text" class="form-control-rounded form-control" id="created_at_v" readonly>
              </div>
            </div>
          </div>  

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Personal Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">First Name</label>
                  <input type="text" class="form-control-rounded form-control" id="fName_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Last Name</label>
                  <input type="text" class="form-control-rounded form-control" id="lName_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Date of Birth</label>
                  <input type="date" class="form-control-rounded form-control" id="birthday_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Age</label>
                  <input type="text" class="form-control-rounded form-control" id="age_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Nationality</label>
                  <input type="text" class="form-control-rounded form-control" id="nationality_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Passpport No</label>
                  <input type="text" class="form-control-rounded form-control" id="passport_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Visa Expiry Date</label>
                  <input type="date" class="form-control-rounded form-control" id="expiryDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Country</label>
                  <input type="text" class="form-control-rounded form-control" id="country_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Mobile No 1</label>
                  <input type="text" class="form-control-rounded form-control" id="mobile1_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Mobile No 2 (optional)</label>
                  <input type="text" class="form-control-rounded form-control" id="mobile2_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Email</label>
                  <input type="email" class="form-control-rounded form-control" id="email_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Address in Sri Lanka</label>
                  <input type="text" class="form-control-rounded form-control" id="address_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-6">
                  <label for="exampleFormControlInput1">Permanent Address</label>
                  <input type="text" class="form-control-rounded form-control" id="permenentAdd_v" readonly>
                </div>
              </div>
            </div>
          </div>




          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12 layout-spacing">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Flight Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Flight Number</label>
                  <input type="text" class="form-control-rounded form-control" id="flightNo_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Location</label>
                  <input type="text" class="form-control-rounded form-control" id="departure_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Date</label>
                  <input type="date" class="form-control-rounded form-control" id="depDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Departure Time</label>
                  <input type="time" class="form-control-rounded form-control" id="depTime_v" readonly>
                </div>
              </div>
            </div>
          </div>

          <div class="col-lg-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="statbox widget box box-shadow">
              <div class="widget-header">                                
                <div class="row">
                  <div class="col-xl-12 col-md-12 col-sm-12 col-12 p-0">
                    <h4 class="p-0 pb-4">Rental Details</h4>
                  </div>                                                      
                </div>
              </div>
              <div class="row">
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Vehicle Type</label>
                  <input type="text" class="form-control-rounded form-control" id="vehicletype_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Vehicle Reg No</label>
                  <input type="text" class="form-control-rounded form-control" id="regNo_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Sri Lankan Driving License No</label>
                  <input type="text" class="form-control-rounded form-control" id="licenseNo_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">From</label>
                  <input type="text" class="form-control-rounded form-control" id="fromDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">To</label>
                  <input type="text" class="form-control-rounded form-control" id="toDate_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Milage Starts</label>
                  <input type="text" class="form-control-rounded form-control" id="milageStart_v" readonly>
                </div>
                <div class="form-group mb-4 col-md-3">
                  <label for="exampleFormControlInput1">Milage Ends</label>
                  <input type="text" class="form-control-rounded form-control" id="milageEnd_v" readonly>
                </div>
              </div>
            </div>
          </div>

        </div>

        <div class="modal-footer">
          <a href="" class="btn btn-outline-secondary btn-rounded mt-2" target="_blank" id="print_v">Print</a>
          <input type="reset" name="time" class="btn btn-button-6 btn-rounded mt-2" data-dismiss="modal" value="Close">
        </div>

      </form>

    </div>
  </div>
</div>
</div>



<!--  BEGIN FOOTER  -->
<?php require __DIR__.'../../includes/footer.php'; ?> 
<!--  END FOOTER  -->

<script type="text/javascript">
  $('.search_dropdown').select2();
</script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<?php require __DIR__.'../../includes/bottom_footer.php'; ?> 
<!-- END GLOBAL MANDATORY STYLES -->   

<script src="<?php echo base_url(); ?>assets/assets/js/countrySelect.js"></script>
<script src="<?php echo base_url(); ?>assets/assets/js/intlTelInput.js"></script>

<script>
  $("#country_selector").countrySelect({
    responsiveDropdown: true,
    preferredCountries: ['lk', 'us', 'gb']
  });

  var input = document.querySelector(".phone");
  var input2 = document.querySelector(".phone2");

  var iti = window.intlTelInput(input, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  var iti2 = window.intlTelInput(input2, {
    allowDropdown: true,
    autoPlaceholder:false,
    preferredCountries: ['lk', 'us', 'gb'],
    separateDialCode: true,
    utilsScript: "<?php echo base_url(); ?>assets/assets/js/utils.js",
  });

  $("#country_selector").on('change', function(){
    var code = $("#country_selector").countrySelect("getSelectedCountryData").iso2;
    iti.setCountry(code);
    iti2.setCountry(code);
  });
</script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>assets/plugins/table/datatable/datatables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<script>

  setTimeout(function() {
    $('.alert-messages').slideUp();
  }, 5000);

  $("select").on("select2:close", function (e) {  
    $(this).valid(); 
  });

  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": { "previous": "<i class='flaticon-arrow-left-1'></i>", "next": "<i class='flaticon-arrow-right'></i>" },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [ {
      targets: [ 0 ],
      orderData: [ 0, 1 ]
    }, {
      targets: [ 1 ],
      orderData: [ 1, 0 ]
    }, {
      targets: [ 4 ],
      orderData: [ 4, 0 ]
    } ]
  });

    //From and To Date selection
    var today = new Date();
    var hh = today.getHours();
    var ii = today.getMinutes();
    var dd = String(today.getDate()).padStart(2, '0');
    var mm = String(today.getMonth() + 1).padStart(2, '0');
    var yyyy = today.getFullYear();

    var from_today = yyyy + '-' + mm + '-' + dd + 'T00:00';
    $('#fromDate').attr('min', from_today);

    var dd = String(today.getDate() + 1).padStart(2, '0');
    to_date = yyyy + '-' + mm + '-' + dd + 'T00:00';
    $('#toDate').attr('min', to_date);
    $('#toDate').val(to_date);

    function changeDate() {
     from_today = new Date($('#fromDate').val());
     var dd = String(from_today.getDate() + 1).padStart(2, '0');
     var mm = String(from_today.getMonth() + 1).padStart(2, '0');
     var yyyy = from_today.getFullYear();

     from_today = yyyy + '-' + mm + '-' + dd + 'T00:00';
     $('#toDate').val(from_today);
     $('#toDate').attr('min', from_today);

     $('#vehicletype').trigger('change');
   }

   function changeToDate() {
     $('#vehicletype').trigger('change');
   }

   $('#birthday').on('change', function() {
    let dob = $(this).val();
    dob = new Date(dob);
    let today = new Date();
    let age = Math.floor((today-dob) / (365.25 * 24 * 60 * 60 * 1000));
    $('#age').val(age);
  });


   getBookingNumbers();

   function getBookingNumbers() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getBookingNumbers',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {

        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          if (result.data.length > 0) {
            $.each(result.data, function (i, item) {
              $('#bookingNo').append($('<option>', { 
                value: item.id,
                text : item.booking_no 
              }));
            });
          }
          $(".cs-overlay").css('visibility', 'hidden');
          getAgreementNo();
        }        
      }
    });
  }

  function getAgreementNo() {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementNo',
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#agreement_no').val(result.data.agreement_no);
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }


  $('#bookingNo').on('change', function () {
    let id = $(this).val();
    if (id != null) {
      $.ajax({
        type:'post',
        url:'<?php echo base_url(); ?>booking/getBookingById',
        data:{id},
        beforeSend: function(){
          $(".cs-overlay").css('visibility', 'visible');
        },
        success:function (res) {
          if (!res) {
            location.reload();  
          } else {
            let result = JSON.parse(res);

            $('#email').val(result.data.email);
            $('#fName').val(result.data.full_name);
            $('#fromDate').val(result.data.pickup.pickup_date);
            $('#toDate').val(result.data.return.return_date);
            $('#mobile1').val(result.data.mobile_no);
            

            $('#vehicletype').empty();       

            $('#vehicletype').append($('<option>', { 
              value: result.data.vehicle.vehicle_type_id,
              text : result.data.vehicle.type 
            }));

            $('#vehicletype').trigger('change');

            $(".cs-overlay").css('visibility', 'hidden');
          }        
        }
      });
    }
  });



  $('#vehicletype').on('change', function () {
    let vehicle_type_id = $(this).val();
    let from_date = $('#fromDate').val();
    let to_date = $('#toDate').val();

    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getVehicleRegistrationNumberByVehicleType',
      data:{vehicle_type_id,from_date,to_date},
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#regNo').empty();
          $('#regNo').append('<option value="" selected>Select Vehicle Reg No</option>');
          $.each(result.data, function (i, item) {
            $('#regNo').append($('<option>', { 
              value: item.vehicle_id,
              text : item.registration_no 
            }));
          });
          if ($('#vehicle_id_edit').val() != "") {
            $('#regNo').val($('#vehicle_id_edit').val());
            $('#regNo').trigger('change');
          }
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  });


  $(document).ready(function() {
    $("#agreement_form").validate({

      rules: {
        fName : {
          required: true,
        },
        birthday : {
          required: true,
        },
        passport: {
          required: true,
        },
        expiryDate: {
          required: true,
        },
        country : {
          required: true,
        },
        mobile : {
          required: true,
        },
        email : {
          required: true,
        },
        address : {
          required: true,
        },
        permenentAdd : {
          required: true,
        },
        flightNo : {
          required: true,
        },
        departure : {
          required: true,
        },
        depDate : {
          required: true,
        },
        depTime : {
          required: true,
        },
        regNo : {
          required: true,
        },
        licenseNo : {
          required: true,
        },
        fromDate : {
          required: true,
        },
        toDate : {
          required: true,
        },
        vehicleType : {
          required: true,
        },
        milageStart : {
          required: true,
        },
      },
      messages : {
        fName: {
          required: "Please enter First Name",
        },
        birthday: {
          required: "Please enter Date of Birth",
        },
        passport: {
          required: "Please enter Passport No",
        },
        expiryDate: {
          required: "Please enter Visa Expiry Date",
        },
        country: {
          required: "Please select Country",
        },
        mobile: {
          required: "Please enter Mobile No",
        },
        email: {
          required: "Please enter Email",
        },
        address: {
          required: "Please enter Address",
        },
        permenentAdd: {
          required: "Please enter Permenent Address",
        },
        flightNo: {
          required: "Please enter Flight No",
        },
        departure: {
          required: "Please enter Departure Location",
        },
        depDate: {
          required: "Please enter Departure Date",
        },
        depTime: {
          required: "Please enter Departure Time",
        },
        regNo: {
          required: "Please select Registation No",
        },
        licenseNo: {
          required: "Please enter License No",
        },
        fromDate: {
          required: "Please enter From Date",
        },
        toDate: {
          required: "Please enter To Date",
        },
        vehicleType : {
          required: "Please select Vehicle Type",
        },
        milageStart: {
          required: "Please enter Milage Start",
        },
      }
    });
  });


  $('#agreement_form').submit(function(evt) {
    evt.preventDefault();

    if( $('#agreement_form').valid() ) {

      let mobile_code = '+'+iti.selectedCountryData.dialCode;
      let mobile2_code = '+'+iti2.selectedCountryData.dialCode;
      let agreement_id = $('#agreement_id').val();
      let bookingNo = $('#bookingNo').val();
      let agreement_no = $('#agreement_no').val();
      let fName = $('#fName').val();
      let lName = $('#lName').val();
      let birthday = $('#birthday').val();
      let age = $('#age').val();
      let nationality = $('#nationality').val();
      let passport = $('#passport').val();
      let expiryDate = $('#expiryDate').val();
      let country_selector_code = $('#country_selector_code').val();
      let mobile_country_code = mobile_code;
      let mobile2_country_code = mobile2_code;
      let mobile1 = $('#mobile1').val();
      let mobile2 = $('#mobile2').val();
      let email = $('#email').val();
      let address = $('#address').val();
      let permenentAdd = $('#permenentAdd').val();
      let flightNo = $('#flightNo').val();
      let departure = $('#departure').val();
      let depDate = $('#depDate').val();
      let depTime = $('#depTime').val();
      let vehicletype = $('#vehicletype').val();
      let fromDate = $('#fromDate').val();
      let toDate = $('#toDate').val();
      let regNo = $('#regNo').val();
      let licenseNo = $('#licenseNo').val();
      let milageStart = $('#milageStart').val();
      let milageEnd = $('#milageEnd').val();

      $.ajax({
        type:'post',
        url:'<?php echo base_url(); ?>agreement/saveAgreement',
        data:{agreement_id,bookingNo,agreement_no,fName,lName,birthday,age,nationality,passport,expiryDate,country_selector_code,mobile_country_code,mobile2_country_code,mobile1,mobile2,email,address,permenentAdd,flightNo,departure,depDate,depTime,vehicletype,fromDate,toDate,regNo,licenseNo,milageStart,milageEnd},
        beforeSend: function(){
          $(".cs-overlay").css('visibility', 'visible');
        },
        success:function (res) {
          location.reload();
        }
      });
    } 
  });

  function editAgreement(id) {
    $("#submit_button").val("Update");
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          $('#bookingNo').val(result.data.booking_id);
          $('#bookingNo').trigger('change');
          $('#agreement_no').val(result.data.agreement_no); 
          $('#fName').val(result.data.first_name); 
          $('#lName').val(result.data.last_name); 
          $('#birthday').val(result.data.date_of_birth); 
          $('#age').val(result.data.age); 
          $('#nationality').val(result.data.nationality); 
          $('#passport').val(result.data.passport_no); 
          $('#expiryDate').val(result.data.visa_expiry_date);  
          $("#country_selector").countrySelect("selectCountry", result.data.country_code);
          iti.setNumber(result.data.mobile_1_country_code);
          iti2.setNumber(result.data.mobile_2_country_code);
          $('#mobile1').val(result.data.mobile_1); 
          $('#mobile2').val(result.data.mobile_2); 
          $('#email').val(result.data.email); 
          $('#address').val(result.data.address_in_srilanka); 
          $('#permenentAdd').val(result.data.permanent_address); 
          $('#flightNo').val(result.data.flight_number); 
          $('#departure').val(result.data.departure_location); 
          $('#depDate').val(result.data.departure_date); 
          $('#depTime').val(result.data.departure_time); 
          $('#vehicletype').val(result.data.vehicle_type_id); 
          $('#vehicletype').trigger('change');
          $('#fromDate').val(result.data.from_date);
          $('#fromDate').attr('min', '');
          $('#toDate').val(result.data.to_date); 
          $('#toDate').attr('min', '');
          $('#vehicle_id_edit').val(result.data.vehicle_id); 
          $('#licenseNo').val(result.data.license_no); 
          $('#milageStart').val(result.data.milage_starts); 
          $('#milageEnd').val(result.data.milage_ends); 
          $('#agreement_id').val(result.data.id);          
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }

  $('#reset_button').on('click', function() {
    let agrementid = $('#agreement_no').val();
    $("#submit_button").val("Save");
    $('#agreement_id').val("");
    $('#vehicle_id_edit').val("");
    $('#regNo').empty();
    $('#regNo').append('<option value="" disabled selected hidden>Select Vehicle Reg No</option>');
    $("form").validate().resetForm();
    $('#fromDate').attr('min', from_today);
    $('#vehicletype').empty();
    setTimeout(function() {
      $('#agreement_no').val(agrementid);
      $('#toDate').val(to_date);
      $('#toDate').attr('min', to_date);
      $("#bookingNo").val($("#bookingNo option:first").val());
      $('#bookingNo').trigger('change');
    }, 10);    
  });


  function deleteAgreement(id) {
    $.confirm({
      title: 'Confirm!',
      content: 'Are you sure, do you want to delete this?',
      type: 'red',
      buttons: {
        confirm: function(){
          location.href = '<?php echo base_url(); ?>agreement/deleteAgreement/'+id;
        },
        cancel : {

        }
      }
    });
  }

  function endAgreement(id, agreement_no, fName, lName, fromDate, toDate, milage_starts) {
    $('#agreementid').val(id);
    $('#agreementno').html(agreement_no);
    $('#client_name').val(fName+' '+lName);
    $('#from_date').val(fromDate);
    $('#to_date').val(toDate);
    $('#milage_start').val(milage_starts);
    $('#milage_end').attr('min',milage_starts);
    $('#modal_end').modal('show');
  }


  function viewAgreement(id) {
    $.ajax({
      type:'post',
      url:'<?php echo base_url(); ?>agreement/getAgreementById/'+id,
      beforeSend: function(){
        $(".cs-overlay").css('visibility', 'visible');
      },
      success:function (res) {
        if (!res) {
          location.reload();  
        } else {
          let result = JSON.parse(res);
          let print_url = '<?php echo base_url(); ?>agreement/printAgreement/'+result.data.id;
          $('#booking_no_v').val(result.data.booking.booking_no);
          $('#agreement_no_v').val(result.data.agreement_no);
          $('#agreement_no_v_label').html(result.data.agreement_no);            
          $('#vehicletype_v').val(result.data.vehicle.vehicle_type.type);
          $('#created_at_v').val(result.data.created_at);             
          $('#fName_v').val(result.data.first_name); 
          $('#lName_v').val(result.data.last_name); 
          $('#birthday_v').val(result.data.date_of_birth); 
          $('#age_v').val(result.data.age); 
          $('#nationality_v').val(result.data.nationality); 
          $('#passport_v').val(result.data.passport_no); 
          $('#expiryDate_v').val(result.data.visa_expiry_date); 
          $('#country_v').val(result.data.country.country); 
          $('#mobile1_v').val(result.data.mobile_1); 
          $('#mobile2_v').val(result.data.mobile_2); 
          $('#email_v').val(result.data.email); 
          $('#address_v').val(result.data.address_in_srilanka); 
          $('#permenentAdd_v').val(result.data.permanent_address); 
          $('#flightNo_v').val(result.data.flight_number); 
          $('#departure_v').val(result.data.departure_location); 
          $('#depDate_v').val(result.data.departure_date); 
          $('#depTime_v').val(result.data.departure_time); 
          $('#fromDate_v').val(result.data.from_date); 
          $('#toDate_v').val(result.data.to_date); 
          $('#regNo_v').val(result.data.vehicle_id); 
          $('#licenseNo_v').val(result.data.license_no); 
          $('#milageStart_v').val(result.data.milage_starts); 
          $('#milageEnd_v').val(result.data.milage_ends); 
          $('#print_v').attr('href', print_url)
          $('#modal_view').modal('show');
          $(".cs-overlay").css('visibility', 'hidden');
        }        
      }
    });
  }
</script>
<!-- END PAGE LEVEL SCRIPTS -->
</body>
</html>