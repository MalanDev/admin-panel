<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style type="text/css">
    table {
      width: 100%;
    }

    td {
      width: 25%;
      height: 60px;
    }

    .subtitle {
      padding: 10px 0;
      font-size: 18px;
      border-bottom: 1px solid #ccc;
      padding-bottom: 0;
      margin-bottom: 10px;
      color: #092766;
    }
  </style>
}
</head>
<body>

  <table>
    <tr>
      <td style="width:20%;"></td>
      <td style="width:60%;text-align: center;">
        <h2 style="text-decoration: underline;">TUK TUK RENT NEGOMBO</h2>
        <h4 style="font-style: italic;">Rental Agreement for the hire of Equipment</h4>
      </td>
      <td style="width:20%;"><img src="<?php echo base_url(); ?>assets/assets/img/logo-3.png" style="width: 120px;"></td>
    </tr>
  </table>

  <table>
    <tr>
      <td>
        <h4>Agreement No</h4>
        <p><?php echo $data->agreement_no; ?></p>
      </td>
      <td>
        <h4>Booking No</h4>
        <p><?php echo $data->booking->booking_no; ?></p>
      </td>
      <td></td>
      <td>
      </td>      
    </tr>
  </table>

  <div class="subtitle"><b>Personal Details</b></div>

  <table>
    <tr>
      <td>
        <h4>First Name</h4>
        <p><?php echo $data->first_name; ?></p>
      </td>
      <td>
        <h4>Last Name</h4>
        <p><?php echo $data->last_name; ?></p>
      </td>
      <td>
        <h4>Date of Birth</h4>
        <p><?php echo $data->date_of_birth; ?></p>
      </td>
      <td>
        <h4>Age</h4>
        <p><?php echo $data->age; ?></p>
      </td>
    </tr>
    <tr>
      <td>
        <h4>Nationality</h4>
        <p><?php echo $data->nationality; ?></p>
      </td>
      <td>
        <h4>Passpport No</h4>
        <p><?php echo $data->passport_no; ?></p>
      </td>
      <td>
        <h4>Visa Expiry Date</h4>
        <p><?php echo $data->visa_expiry_date; ?></p>
      </td>
      <td>
        <h4>Country</h4>
        <p><?php echo $data->country->country; ?></p>
      </td>
    </tr>
    <tr>
      <td>
        <h4>Mobile No 1</h4>
        <p><?php echo $data->mobile_1; ?></p>
      </td>
      <td>
        <h4>Mobile No 2</h4>
        <p><?php echo $data->mobile_2; ?></p>
      </td>
      <td colspan="2">
        <h4>Email</h4>
        <p><?php echo $data->email; ?></p>
      </td>
    </tr>
    <tr>
      <td colspan="4">
        <h4>Permanent Address</h4>
        <p><?php echo $data->permanent_address; ?></p>
      </td>
    </tr>
  </table>

  <div class="subtitle"><b>Rental Details</b></div>

  <table>
    <tr>
      <td>
        <h4>Vehicle Type</h4>
        <p><?php echo $data->vehicle->vehicle_type->type; ?></p>
      </td>
      <td>
        <h4>Vehicle Reg No</h4>
        <p><?php echo $data->vehicle->registration_no; ?></p>
      </td>
      <td>
        <h4>Sri Lankan Driving License No</h4>
        <p><?php echo $data->license_no; ?></p>
      </td>
      <td></td>
    </tr>
    <tr>
      <td>
        <h4>From</h4>
        <p><?php echo $data->from_date; ?></p>
      </td>
      <td>
        <h4>To</h4>
        <p><?php echo $data->to_date; ?></p>
      </td>
      <td>
        <h4>Milage Starts</h4>
        <p><?php echo $data->milage_starts; ?></p>
      </td>
      <td>
        <h4>Milage Ends</h4>
        <p><?php echo $data->milage_ends; ?></p>
      </td>
      
    </tr>
  </table>

  <br>
  <h4>FROM <?php echo $data->from_date; ?>  TO  <?php echo $data->to_date; ?> BEING A PERIOD OF <?php echo $data->booking->no_of_days; ?> DAYS</h4>

  <ul style="margin-left:-4%;">
    <li>ONLY HIRER MUST RIDE - DRIVE THE HIRED EQUIPMENT</li>
    <li>PLEASE DON'T RIDE - DRIVE ON THE BEACH AND FOREST</li>
    <li>RENTAL MONEY WILL NOT RETURN</li>
  </ul>

  <h5>I DO HEREBY AGREE TO THE RETURNS AND CONDITIONS STATED ABOVE.</h5>


  <table>
    <tr>
      <td>
        <br>
        <p>...........................................</p>
        <p style="text-align:center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Signature</p>
      </td>
      <td><br></td>
      <td>
        <br>
        <span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo date('Y-m-d H:i'); ?></span>
        <p>...........................................</p>
        <p style="text-align:center;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date</p>
      </td>
    </tr>
  </table>

  <div class="subtitle"><b>Flight Details</b></div>

  <table>
    <tr>
      <td>
        <h4>Flight Number</h4>
        <p><?php echo $data->flight_number; ?></p>
      </td>
      <td>
        <h4>Departure Location</h4>
        <p><?php echo $data->departure_location; ?></p>
      </td>
      <td>
        <h4>Departure Date</h4>
        <p><?php echo $data->departure_date; ?></p>
      </td>
      <td>
        <h4>Departure Time</h4>
        <p><?php echo $data->departure_time; ?></p>
      </td>
    </tr>
  </table>

  <br>
  <hr>
  <p style="text-align:center;">Powered by crud.lk</p>

</body>
</html>