<div class="statbox widget box box-shadow" id="table_div">
  <div class="widget-content widget-content-area">
    <div class="table-responsive mb-4">
      <table id="multi-column-ordering" class="table table-striped table-bordered table-hover" style="width:100%">
        <thead>
          <tr>
            <th>Agreement No</th>
            <th>Booking No</th>
            <th>Full Name</th>
            <th>Pickup Date</th>
            <th>Return Date</th>
            <th>Vehicle No</th>
            <th>Pickup Location</th>
            <th>Return Location</th>
            <th>Status Action</th>
            <th style="text-align:center;">Action</th>
          </tr>
        </thead>
        <tbody>
         <?php foreach($data as $agreement) { ?>
          <tr>
            <td><?php echo $agreement->agreement_no; ?></td>
            <td><?php echo $agreement->booking_no; ?></td>
            <td><?php echo $agreement->first_name." ".$agreement->last_name; ?></td>
            <td><?php echo $agreement->from_date; ?></td>
            <td><?php echo $agreement->to_date; ?></td>
            <td><?php echo $agreement->registration_no; ?></td>
            <td><?php echo $agreement->pickup_location; ?></td>
            <td><?php echo $agreement->return_location; ?></td>
            <td>
              <?php if($agreement->is_end == 0) { ?>
                <button class="btn btn-outline-secondary btn-rounded" onclick="endAgreement('<?php echo $agreement->id; ?>', '<?php echo $agreement->agreement_no; ?>', '<?php echo $agreement->first_name; ?>', '<?php echo $agreement->last_name; ?>', '<?php echo $agreement->from_date; ?>', '<?php echo $agreement->to_date; ?>', '<?php echo $agreement->milage_starts; ?>');">End</button>
              <?php } ?>
            </td>
            <td class="text-center" style="width: 230px;">
              <?php if($agreement->is_end == 0) { ?>
                <button class="btn btn-outline-secondary btn-rounded" onclick="editAgreement('<?php echo $agreement->id; ?>');">Edit</button>
              <?php } ?>
              <a href="<?php echo base_url(); ?>agreement/printAgreement/<?php echo $agreement->id; ?>" class="btn btn-outline-warning btn-rounded" target="_blank">Print</a>
              <button class="btn btn-outline-info btn-rounded" onclick="deleteAgreement('<?php echo $agreement->id; ?>');">Delete</button>
              <button class="btn btn-outline-success btn-rounded" onclick="viewAgreement('<?php echo $agreement->id; ?>');">View</button>
            </td>
          </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>
</div>

<script type="text/javascript">
  $('#multi-column-ordering').DataTable({
    "language": {
      "paginate": {
        "previous": "<i class='flaticon-arrow-left-1'></i>",
        "next": "<i class='flaticon-arrow-right'></i>"
      },
      "info": "Showing page _PAGE_ of _PAGES_"
    },
    columnDefs: [{
      targets: [0],
      orderData: [0, 1]
    }, {
      targets: [1],
      orderData: [1, 0]
    }, {
      targets: [4],
      orderData: [4, 0]
    }]
  });
</script>