<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';

$route['vehicle-types'] = 'master/vehicleTypes';
$route['pickup-locations'] = 'master/pickupLocations';
$route['return-locations'] = 'master/returnLocations';
$route['service-charges'] = 'master/serviceCharges';
$route['extra-services'] = 'master/extraServices';
$route['coupons'] = 'master/coupons';
$route['vehicle-owner-registration'] = 'master/vehicleOwnerRegistration';
$route['vehicle-registration'] = 'master/vehicleRegistration';

$route['check-available-vehicle'] = 'booking/checkAvailableVehicle';
$route['view-booking'] = 'booking/viewBooking';
$route['create-booking'] = 'booking/createBooking';

$route['create-agreement'] = 'agreement/createAgreement';
$route['view-agreement'] = 'agreement/viewAgreement';

$route['customer-invoice'] = 'payment/customerInvoice';
$route['vehicle-owner-invoice'] = 'payment/vehicleOwnerInvoice';

$route['vehicle-owner-details'] = 'Report/vehicleOwnerDetails';
$route['vehicle-details'] = 'Report/vehicleDetails';
$route['check-available-vehicles'] = 'Report/checkAvailableVehicles';
$route['view-bookings'] = 'Report/viewBookings';
$route['view-agreements'] = 'Report/viewAgreements';
$route['customer-invoices'] = 'Report/customerInvoices';
$route['vehicle-owner-invoices'] = 'Report/vehicleOwnerInvoices';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
